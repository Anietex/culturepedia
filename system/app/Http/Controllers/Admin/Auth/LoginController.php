<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
   use AuthenticatesUsers;

   protected $redirectTo = 'admin/dashboard';


   public function __construct()
   {
       $this->middleware('guest_admin',['except'=>['logout']]);

      Admin::create([
          'username' => "Admin",
          'email' => "admin@culturepedia.com",
          'password' => bcrypt("password")
      ]);
   }

   public function username()
   {
       return 'username';
   }

    public function showLoginForm()
   {
       return view('admin.login');
   }


//   public function login(Request $request)
//   {
//       $this->validate($request, [
//           'username'   => 'required|email',
//           'password' => 'required|min:6'
//       ]);
//   }


    public function logout(Request $request)
   {
       $this->guard()->logout();
       return redirect('/admin');
   }


   public function guard()
   {
       return Auth::guard('admin');
   }

   public function login(Request $request)
   {
       if (Auth::guard('admin')->attempt(['username' => $request->username, 'password' => $request->password], $request->remember)) {

         return redirect()->intended(url('/admin/dashboard'));
       }else{
           return redirect()->back()->withInput($request->only('username', 'remember'))->withErrors(['username'=>'Invalid username or password']);
       }
//        if unsuccessful, then redirect back to the login with the form data

   }
}
