import {ArticleLinkPopover} from "./ArticleLinkPopover";
import { ReferencePopover } from "./ReferencePopover";


(()=>{
    let viewer = new ArticleLinkPopover();
    let refPop = new ReferencePopover();

    let defTexts = $('.def-text');
    let links = $('.article-link');
    let citations = $('.cite-link');


    citations.mouseenter((event)=>{
       refPop.showPopover(event)
    });

    citations.mouseleave(()=>{
       refPop.hidePopover();
    });



    defTexts.mouseenter(function(event){
        let elm = $(event.target);
        viewer.showViewLink('',elm.text(),'',elm.data('def'),event)
    });
    defTexts.mouseleave(function () {
        viewer.hidePopover();
    });
    links.mouseenter(function (event) {
        let elm = $(event.target);
        viewer.showViewLink(elm.attr('href'),elm.text(),elm.attr('title'),elm.data('def'),event)
    });
    links.mouseleave(function () {
        viewer.hidePopover();
    })
})();
