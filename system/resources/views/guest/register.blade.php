@extends("layouts.master")
@section('title','Register')
@section('section-title','Register')
@section('content')

       <div class="container">
           <div class="form-wrapper my-4">
               <div class="registration">
                   <h3>Registration</h3>
                   <div class="form">
                       <div class="row">
                           <div class="col-sm-5">
                                <div class="form">
                                    <form method="post" action="{{ route('register') }}">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label for="username">Username</label>
                                            <input type="text" name="username" id="username" value="{{ old('username') }}" required class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" autofocus>
                                            @if ($errors->has('username'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('username') }}
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" name="email" id="email" value="{{ old('email') }}" class="form-control  {{ $errors->has('email') ? 'is-invalid' : '' }}" required>
                                            @if ($errors->has('email'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('email') }}
                                                </div>
                                            @endif

                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" name="password" id="password" value="{{ old('password') }}"  class="form-control  {{ $errors->has('password') ? 'is-invalid' : '' }}" required>
                                            @if ($errors->has('password'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('password') }}
                                                </div>
                                            @endif

                                        </div>
                                        <div class="form-group">
                                            <label for="password_confirmation">Confirm Password</label>
                                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control  {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}" required>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-block">Register</button>
                                    </form>
                                </div>
                           </div>
                           <div class="col-sm-7">
                               <div class="info ml-auto ml-sm-5 text-center">
                                   <h4 class="mb-4">Register to contribute</h4>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
@endsection
