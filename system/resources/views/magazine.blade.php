<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/animate.css">
    <meta name="theme-color" content="#000000">
    <meta name="app-url" content="{{url('/api')}}">
    <link rel="manifest" href="/manifest.json">


    @if(isset($post))
    <meta property="og:url"                content="{{url()->current()}}" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="Culture Magazine" />
    <meta property="og:description"        content="{{$post->description}}" />
    <meta property="og:image"              content="{{$media}}" />

    @endif










    <title>Culturepedia Magazine</title>
    <link href="/assets/css/main.c5d91c84.css" rel="stylesheet">
</head>
<body>
<noscript>You need to enable JavaScript to run this app.</noscript>
<div id="root">

</div>
<script type="text/javascript" src="/assets/js/main.af4873ff.js">

</script>
</body>
</html>