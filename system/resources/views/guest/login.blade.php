@extends("layouts.master")
@section('title','Login')
@section('section-title','Login')
@section('content')

    <div class="container">
        <div class="form-wrapper my-4">
            <div class="registration">
                <h3>Login</h3>
                <div class="form">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form">
                                <form method="post" action="{{ route('login') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" name="username" id="username" value="{{ old('username') }}" required class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" autofocus>
                                        @if ($errors->has('username'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('username') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" id="password" value="{{ old('password') }}"  class="form-control  {{ $errors->has('password') ? 'is-invalid' : '' }}" required>
                                        @if ($errors->has('password'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('password') }}
                                            </div>
                                        @endif

                                    </div>

                                    <button type="submit" class="btn btn-primary">Login</button>
                                    <div class="form-check mt-2">
                                        <input class="form-check-input" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}id="remember">
                                        <label class="form-check-label" for="remember">
                                           Remember me
                                        </label>
                                    </div>
                                </form>
                            </div>
                            <hr>
                            <a href="{{ route('password.request') }}">Reset Password</a>
                            <br><br>
                            <h6>Don't have an account?</h6>
                            <a href="{{ route('register') }}" class="btn btn-outline-primary btn-block">Register</a>
                        </div>
                        <div class="col-sm-5">
                            <div class="info ml-5 text-center">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
