class ArticleViewer{

    constructor(){
        this.element = this.createElement();
        this.linkView = this.getLinkView();
    }

    getLinkView(){
        let lv = document.createElement('div');
        lv.classList.add('link-view');
        lv.id = "link-view";

        let header = ()=>{
            let wrapper = document.createElement('div');
            wrapper.classList.add("header");
            let title = document.createElement('div');
            title.classList.add('title');
            let p= document.createElement('p');
            p.innerHTML=`<span class="fa fa-link"></span> <span id="pop-title"></span>`;
            title.appendChild(p);
            wrapper.appendChild(title);
            return wrapper;
        }
        let links = ()=>{
            let links = document.createElement('div');
            links.classList.add('links');
            links.innerHTML=`<div class="link-item">
                   <h4><a id="viewed-link" href=""></a></h4>
                   <p id="def-text"></p>
               </div>`;
            return links;
        }
        lv.appendChild(header());
        lv.appendChild(links());
        return lv;

    }


    createElement(){
        let wrapper = this.getWrapper();
        let inner = this.innerWrapper();
        inner.appendChild(this.getLinkView());
        wrapper.appendChild(inner);
        return wrapper;
    }



    innerWrapper(){
        let innerWrapper = document.createElement('div');
        innerWrapper.classList.add('baloon');
        return innerWrapper;
    }


    getWrapper(){
        let wrapper = document.createElement('div');
        wrapper.id = "balloon";
        wrapper.classList.add('balloon-wrapper');
        return wrapper;

    }



    attachElement(){

        document.body.appendChild(this.element);
    }

    showViewLink(link,text,title,def,event){
        if(document.body.contains(this.element)){
            document.body.removeChild(this.element)
        }
        this.attachElement();
        let defText = document.getElementById('def-text');
        let vlink = document.getElementById('viewed-link');
        let popTitle = document.getElementById('pop-title');
        if(link==='')
            popTitle.innerText = "Definition";
        else
            popTitle.innerText = "Link";

        defText.innerText = def;
        if(!def)
            defText.innerText = title;
        vlink.innerText=text;
        vlink.title =title;
        vlink.href = link;
        this.element.style.display ="block";
        document.querySelector("#link-view").style.display ="block";


        this.positionElement(this.element,event.pageX,event.pageY);
    }


    hidePopover(){
        if(document.body.contains(this.element)){
            document.body.removeChild(this.element)
        }
    }



    positionElement(element,x,y){
        element.style.left = x-100+"px";
        element.style.top = y+38+"px";
    }
}




(()=>{
    let viewer = new ArticleViewer();
    let defTexts = $('.def-text');
    let links = $('.article-link');


    defTexts.mouseenter(function(event){
        let elm = $(event.target);
        viewer.showViewLink('',elm.text(),'',elm.data('def'),event)
    });
    defTexts.mouseleave(function () {
       viewer.hidePopover();
    });
    links.mouseenter(function (event) {
        let elm = $(event.target);
        viewer.showViewLink(elm.attr('href'),elm.text(),elm.attr('title'),elm.data('def'),event)
    });
    links.mouseleave(function () {
       viewer.hidePopover();
    })
})();
