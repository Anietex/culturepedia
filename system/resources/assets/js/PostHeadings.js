
export let POST_HEADINGS  =   [
    {
        heading:"Tribe/ Community",
        description:"On this heading you will write about the tribe/community that practice this culture",
        textBoxValue:""
    },
    {
        heading:"Region/ Province/ State",
        description:"On this heading you will write about the Region/Province/State that practice this culture",
        textBoxValue:""
    },
    {
        heading:"Country",
        description:"On this heading you will write about the Country in which this culture is prevalent",
        textBoxValue:""
    },
    {
        heading:"Language(s)",
        description:"On this heading you will write about the Language(s) popular in this culture",
        textBoxValue:""
    },
    {
        heading:"Landscape",
        description:"On this heading you will on  the Landscape of this culture",
        textBoxValue:""
    },
    {
        heading:"History and Mythology",
        description:"On this heading you will write about the History and Mythology of this culture",
        textBoxValue:""
    },
    {
        heading:"Literature",
        description:"On this heading you will write about the Literature of this culture",
        textBoxValue:""
    },
    {
        heading:"Music and Arts",
        description:"On this heading you will write about the culture\'s music and art",
        textBoxValue:""
    },
    {
        heading:"Politics and Leadership",
        description:"On this heading you will write about the Politics and Leadership of this culture",
        textBoxValue:""
    },
    {
        heading:"Social Institutions",
        description:"On this heading you will write about the Social Institutions of this culture",
        textBoxValue:""
    },
    {
        heading:"Religion and Rituals",
        description:"On this heading you will write about the Religion and Ritual perform by this culture",
        textBoxValue:""
    },
    {
        heading:"Festivals",
        description:"On this heading you will write about the festivals this culture",
        textBoxValue:""
    },
    {
        heading:"Occupations",
        description:"On this heading you will write about the  Occupations of this culture",
        textBoxValue:""
    },
    {
        heading:"Tools and Technologies",
        description:"On this heading you will write about the  Tools and Technologies of this culture",
        textBoxValue:""
    },
    {
        heading:"Clothing and Cosmetics",
        description:"On this heading you will write about the  Clothing and Cosmetics of this culture",
        textBoxValue:""
    },
    {
        heading:"Food and Food Processing",
        description:"On this heading you will write about the  Food and Food Processing  of this culture",
        textBoxValue:""
    },
    {
        heading:" Tourist Attractions",
        description:"On this heading you will write about the   Tourist Attractions  of this culture",
        textBoxValue:""
    },
    {
        heading:"Other traditions",
        description:"On this heading you will write about   other traditions  of this culture",
        textBoxValue:""
    },
    {
        heading:"Pictures",
        description:"Upload Picture about their festivals, food ,dressing etc.",
        files:[]
    },
    {
        heading:"Videos",
        description:"Upload Videos about their festivals, food ,dressing etc",
        files:[]
    },

];