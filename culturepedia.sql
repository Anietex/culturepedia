-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 27, 2018 at 08:15 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `culturepedia`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@culturepedia.com', '$2y$10$V/4jkGgowfR5JKCrlI4rVuOekuVlAJm1Wzm1sDoLnv29dcdv8mYN6', 'Nefx0EbzrgfscEg5ZsXikKuRfEcbiWQ2LHH8nYw4oDOXVdudScBDplc73z4O', '2018-06-07 06:09:35', '2018-06-20 08:02:00');

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `article_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tribe` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `region` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `geo_description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `history` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `literature` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tradition` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `leadership` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_institution` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `religion` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `occupations` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `technology` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `clothing` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `food` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tourist_attraction` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`article_id`, `user_id`, `title`, `tribe`, `region`, `country`, `language`, `geo_description`, `history`, `literature`, `tradition`, `leadership`, `social_institution`, `religion`, `occupations`, `technology`, `clothing`, `food`, `tourist_attraction`, `created_at`, `updated_at`, `published`, `approved`) VALUES
(39, 5, 'The culture of Perirachi', '<div><div>round 1400 A.D, <a href=\"https://en.wikipedia.org/wiki/Periyachi\" title=\"Periyachi\" class=\"article-link\" data-def=\"\">Periyachi</a> was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that <span class=\"def-text\" data-def=\"This refers to a femail third person\">she</span> was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a<br></div></div>', 'round 1400 A.D, <a href=\"https://en.wikipedia.org/wiki/Periyachi\" title=\"Periyachi\" class=\"article-link\" data-def=\"\" style=\"background-color: rgb(255, 255, 255);\">Periyachi</a> was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that <span class=\"def-text\" data-def=\"This refers to a femail third person\">she</span> was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, <a href=\"https://en.wikipedia.org/wiki/Periyachi\" title=\"Periyachi\" class=\"article-link\" data-def=\"\" style=\"background-color: rgb(255, 255, 255);\">Periyachi</a> was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that <span class=\"def-text\" data-def=\"This refers to a femail third person\">she</span> was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, <a href=\"https://en.wikipedia.org/wiki/Periyachi\" title=\"Periyachi\" class=\"article-link\" data-def=\"\" style=\"background-color: rgb(255, 255, 255);\">Periyachi</a> was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that <span class=\"def-text\" data-def=\"This refers to a femail third person\">she</span> was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, <a href=\"https://en.wikipedia.org/wiki/Periyachi\" title=\"Periyachi\" class=\"article-link\" data-def=\"\" style=\"background-color: rgb(255, 255, 255);\">Periyachi</a> was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that <span class=\"def-text\" data-def=\"This refers to a femail third person\">she</span> was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, <a href=\"https://en.wikipedia.org/wiki/Periyachi\" title=\"Periyachi\" class=\"article-link\" data-def=\"\" style=\"background-color: rgb(255, 255, 255);\">Periyachi</a> was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that <span class=\"def-text\" data-def=\"This refers to a femail third person\">she</span> was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, <a href=\"https://en.wikipedia.org/wiki/Periyachi\" title=\"Periyachi\" class=\"article-link\" data-def=\"\" style=\"background-color: rgb(255, 255, 255);\">Periyachi</a> was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that <span class=\"def-text\" data-def=\"This refers to a femail third person\">she</span> was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, <a href=\"https://en.wikipedia.org/wiki/Periyachi\" title=\"Periyachi\" class=\"article-link\" data-def=\"\" style=\"background-color: rgb(255, 255, 255);\">Periyachi</a> was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that <span class=\"def-text\" data-def=\"This refers to a femail third person\">she</span> was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, <a href=\"https://en.wikipedia.org/wiki/Periyachi\" title=\"Periyachi\" class=\"article-link\" data-def=\"\" style=\"background-color: rgb(255, 255, 255);\">Periyachi</a> was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that <span class=\"def-text\" data-def=\"This refers to a femail third person\">she</span> was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, <a href=\"https://en.wikipedia.org/wiki/Periyachi\" title=\"Periyachi\" class=\"article-link\" data-def=\"\" style=\"background-color: rgb(255, 255, 255);\">Periyachi</a> was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that <span class=\"def-text\" data-def=\"This refers to a femail third person\">she</span> was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, <a href=\"https://en.wikipedia.org/wiki/Periyachi\" title=\"Periyachi\" class=\"article-link\" data-def=\"\" style=\"background-color: rgb(255, 255, 255);\">Periyachi</a> was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that <span class=\"def-text\" data-def=\"This refers to a femail third person\">she</span> was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, <a href=\"https://en.wikipedia.org/wiki/Periyachi\" title=\"Periyachi\" class=\"article-link\" data-def=\"\" style=\"background-color: rgb(255, 255, 255);\">Periyachi</a> was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that <span class=\"def-text\" data-def=\"This refers to a femail third person\">she</span> was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, <a href=\"https://en.wikipedia.org/wiki/Periyachi\" title=\"Periyachi\" class=\"article-link\" data-def=\"\" style=\"background-color: rgb(255, 255, 255);\">Periyachi</a> was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that <span class=\"def-text\" data-def=\"This refers to a femail third person\">she</span> was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, <a href=\"https://en.wikipedia.org/wiki/Periyachi\" title=\"Periyachi\" class=\"article-link\" data-def=\"\" style=\"background-color: rgb(255, 255, 255);\">Periyachi</a> was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that <span class=\"def-text\" data-def=\"This refers to a femail third person\">she</span> was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, <a href=\"https://en.wikipedia.org/wiki/Periyachi\" title=\"Periyachi\" class=\"article-link\" data-def=\"\" style=\"background-color: rgb(255, 255, 255);\">Periyachi</a> was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that <span class=\"def-text\" data-def=\"This refers to a femail third person\">she</span> was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, <a href=\"https://en.wikipedia.org/wiki/Periyachi\" title=\"Periyachi\" class=\"article-link\" data-def=\"\" style=\"background-color: rgb(255, 255, 255);\">Periyachi</a> was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that <span class=\"def-text\" data-def=\"This refers to a femail third person\">she</span> was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', '2018-06-26 02:51:28', '2018-06-26 03:58:31', 1, 1),
(43, 1, 'Delete video', 'round 1400 A.D, Periyachi was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that she was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, Periyachi was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that she was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, Periyachi was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that she was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, Periyachi was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that she was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, Periyachi was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that she was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, Periyachi was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that she was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, Periyachi was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that she was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, Periyachi was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that she was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, Periyachi was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that she was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, Periyachi was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that she was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, Periyachi was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that she was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'vround 1400 A.D, Periyachi was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that she was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, Periyachi was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that she was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, Periyachi was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that she was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, Periyachi was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that she was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', 'round 1400 A.D, Periyachi was a woman who was working as a maruthuvachi (doctor) in Kondithoppu village in Tamilnadu. She was well known for her proficiency in delivering babies. It is also mentioned that she was well versed in martial skills such as sword fighting, and spear throwing.In 1406 A.D, King Vallalan IV, the son of Rajanarayana Sambuvarayar III (1356 - 1375 AD) had been dethroned and was hiding in the jungle with his entourage. He had grown bitter and lost hope of recapturing the throne. He raided and plundered the nearby villages, and had become a bandit. His wife Kaarkuzhali, was about to deliver a baby.Periyachi was dragged to the forest in the middle of the night to deliver the baby of Vallalan. It was a', '2018-06-26 03:34:08', '2018-06-26 07:09:20', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `articles_media`
--

CREATE TABLE `articles_media` (
  `media_id` int(10) UNSIGNED NOT NULL,
  `article_id` int(11) NOT NULL,
  `media_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles_media`
--

INSERT INTO `articles_media` (`media_id`, `article_id`, `media_description`, `file_name`) VALUES
(4, 16, 'Yoruba Dancing', 'a4c95c77606ac95b8fc3c11174231e48.jpg'),
(5, 16, 'calabar carnival', 'f411fb2e545bb3f5e46c52a58e243333.jpg'),
(6, 16, 'Ritual', 'cc1fbea6e26e167755bccffc5e0400ff.jpg'),
(7, 17, 'yoruba', '96b88a84995a85af5c1d15693231a10e.jpg'),
(9, 18, 'book', '97a66432526504960b58321ee8ae4a3f.png'),
(10, 18, 't-shirts', '76b385d04852b624d589cc1493ef2a95.jpg'),
(11, 20, 'ytuytuy', '2f5287200fbbcb56db6279f6107a0f4c.jpg'),
(12, 17, 'design', '8b9f926df1bde6c28bc3a2e186d08583.jpg'),
(14, 21, 'Designs', '8b410ccc266face9df67a4feeff74c7f.jpg'),
(15, 21, 'Web sites', 'bdae8a09a0d2143d82d2288dbf24a322.jpg'),
(16, 14, 'Phone', '20be1c6dd7d030bf0791bef485034077.jpg'),
(17, 14, 'drugs', 'e0b53ede10e3a8f20fefa3633670e3a5.png'),
(18, 19, NULL, 'f3e27b448f53bacaa2e8222ea91f78f4.jpg'),
(19, 19, NULL, '513e9e4681c5a428edd6d4d623d4bdef.jpg'),
(20, 19, NULL, '13e44a16244e330c9043c8642f319b4f.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `featured_articles`
--

CREATE TABLE `featured_articles` (
  `id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `expire_at` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `featured_articles`
--

INSERT INTO `featured_articles` (`id`, `article_id`, `expire_at`) VALUES
(1, 39, 1530042000);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2018_06_04_113518_create_articles_table', 2),
(5, '2018_06_04_115340_create_articles_media_table', 3),
(6, '2018_06_05_072342_add_user_id_to_articles_table', 3),
(8, '2018_06_07_063912_create_admins_table', 4),
(9, '2018_06_11_063618_add_columns_to_articles_table', 5),
(10, '2018_06_13_131531_create_videos_table', 6),
(11, '2018_06_13_131812_create_pictures_table', 6),
(12, '2018_06_14_071140_alter_videos_table', 7),
(13, '2018_06_14_072011_alter_pictures_table', 7),
(14, '2018_06_20_140325_create_featured_articles_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('jomacil59@yahoo.com', '$2y$10$RkfO/g4ywqsWL.C1DyHldubc5aMstBMpYB9WHHgM6hArYn05G8xiG', '2018-06-26 02:14:21'),
('anietex@gmail.com', '$2y$10$qmD9KvuhPEFRkifxMxQXZu8sbGpNVv1eMGR01VB.Ve1LRHYr6taK.', '2018-06-26 02:15:58'),
('leobiztech@gmail.com', '$2y$10$dAsU1Haf7prgAYWKRAMVMeiusjUJpZNwjqbNEtlu.IlbvqbMQSpnO', '2018-06-26 02:19:12');

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE `pictures` (
  `picture_id` int(10) UNSIGNED NOT NULL,
  `article_id` int(11) NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pictures`
--

INSERT INTO `pictures` (`picture_id`, `article_id`, `file_name`, `description`) VALUES
(16, 43, '7789d5a060be30d0da98840a7fd171ca.jpg', 'A nice flower'),
(17, 43, '40f89cca008c848ea00b14bf17436f4e.jpg', 'Adele 25 album'),
(18, 39, 'c2a4233033261f15359857da919892ff.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(5, 'Aniefon', 'leobiztech@gmail.com', '$2y$10$bdGwEOyhtteVlb8vzMyuce4eIG49eoDwsxkkjWQskq27B/sRySwUi', '6dJj7fGeAE9SVl7bULxpgWN0G0oLgrgxuGG3FeCyxNBdZQnde2gIoEFpKbL8', '2018-06-26 01:25:57', '2018-06-26 01:25:57'),
(6, 'Anietex', 'anietex@gmail.com', '$2y$10$eOT3zG63ylnveU1TDQ6fdeBreF312jdq4dxU67VmfSi824gY94lnS', '2EShjVaN5erZ1zDO494ceFkbwUoN2KK3vDKLYHICuOY8SkyoI12gLL3mjeKX', '2018-06-26 01:37:38', '2018-06-26 01:37:38'),
(7, 'Josh', 'jomacil59@yahoo.com', '$2y$10$XSCPXFk2XgVaKhRLz27Eauy3rBWqhOl91BDn1BzIcTouw3cdEEI6O', 'CDA6ZhASqylS6e6VAa8byRQ5uZb2fIcUYeIZKD9BSasMGwJ8K3AP7o7fRTnk', '2018-06-26 02:13:56', '2018-06-26 02:13:56');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `video_id` int(10) UNSIGNED NOT NULL,
  `article_id` int(11) NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`video_id`, `article_id`, `file_name`, `description`) VALUES
(19, 43, '0695ceeddec77a611328b787c412a075.mp4', NULL),
(20, 39, 'fc8db7fcee271b246239b60a4d0973cb.mp4', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`article_id`);
ALTER TABLE `articles` ADD FULLTEXT KEY `title` (`title`);

--
-- Indexes for table `articles_media`
--
ALTER TABLE `articles_media`
  ADD PRIMARY KEY (`media_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`picture_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`video_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `article_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `articles_media`
--
ALTER TABLE `articles_media`
  MODIFY `media_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `picture_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `video_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
