<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MagazineUser extends Model
{
    protected $fillable = ['user_id', 'profile_picture', 'facebook','instagram','twitter'];
    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
}
