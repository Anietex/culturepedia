<div class="articles">
    <div class="articles">
        @if(session('delete_success'))
            <div class="alert alert-success">
                <p>{{ session('delete_success') }}</p>
            </div>
        @endif
        @if(session('approval_success'))
            <div class="alert alert-success">
                <p>{{ session('approval_success') }}</p>
            </div>
        @endif
        @if(session('approval_error'))
            <div class="alert alert-danger">
                <p>{{ session('approval_error') }}</p>
            </div>
        @endif
        @if(session('disapproval_success'))
            <div class="alert alert-success">
                <p>{{ session('disapproval_success') }}</p>
            </div>
        @endif
        @if(session('disapproval_error'))
            <div class="alert alert-danger">
                <p>{{ session('disapproval_error') }}</p>
            </div>
        @endif
        <table class="table table-responsive">
            <thead>
            <tr>
                <th width="1%">S/N</th>
                <th>Title</th>
                <th width="">Date Added</th>
                <th width="5%">Status</th>
                <th width="1%">View</th>

                <th width="1%">Edit</th>
                <th width="1%">Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($articles as $article)
                <tr>
                    <td>{{ $loop->index+1 }}</td>
                    <td>{{ $article->title }}</td>
                    <td>{{ $article->created_at }}</td>
                    <td>@if($article->approved==true){{ 'Approved' }} @else{{ 'Unapproved' }} @endif</td>
                    <td><a  href="{{ url('article/'.$article->article_id.'/'.str_slug($article->title)) }}" class="btn btn-outline-info btn-sm">View</a></td>
                    <td><a href="{{ url('user/article/'.$article->article_id.'/edit') }}" class="btn btn-outline-info btn-sm">Edit</a></td>
                    <td>
                        <form method="post" action=" {{ url('user/article',['id'=>$article->article_id]) }}">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>