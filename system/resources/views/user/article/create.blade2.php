@extends('layouts.master')

@section('styles')
    @parent
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/editor.css') }}">
@endsection
@section('title','Create Article')
@section('section-title','Create Article')

@section('content')
    <div class="container">
        <div class="create-article">
            <div class="inner-wrapper">
                <h3 class="font-weight-light">Create a New Article</h3>
                <hr>
                @if ($errors->any())
                    <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                    </div>
                @endif
                @if(session('success'))
                    <div class="alert alert-success">
                        <p>{{ session('success') }}</p>
                    </div>
                @endif
                @if(session('error'))
                    <div class="alert alert-danger">
                        <p>{{ session('error') }}</p>
                    </div>
                @endif
                <form action="{{ url('article') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                   <div class="form-group">
                       <label for="title">Article title</label>
                       <input type="text" name="title" id="title" class="form-control input-lg" placeholder="Article title" value="{{ old('title') }}">
                   </div>
                    <label for="article"><span  id="heading"></span> <small class="text-info"> (<span id="help-text"></span> ) </small></label>
                    <div class="form-group" id="text-pane">
                        <div id="editor"></div>
                        {{--<textarea class="form-control" id="article" rows="10"></textarea>--}}
                    </div>





                    <div class="upload-pictures upload-pane" id="picture-pane">
                        <label>Upload Pictures </label>
                        <small>Maximum file size of 500KB</small>
                        <div class="media-files pictures d-flex flex-wrap" id="picture-files">
                            <div class="upload-btn">
                                <div class="add-media add-media-btn">
                                    <button type="button" class="fa fa-plus fa-2x" data-input="#media-1" id="picture-btn"></button>
                                </div>
                                <input type="file"  class="form-control media-input"  id="picture-input" accept="image/*">
                            </div>
                        </div>
                    </div>




                    <div class="upload-videos upload-pane" id="video-pane">

                        <label>Upload short Videos</label>
                        <small>Maximum file size of 3.5MB</small>
                        <div class="media-files pictures d-flex flex-wrap" id="video-files">

                            {{--<div class="uploaded-media">--}}
                                {{--<span class="fa fa-close close-btn" ></span>--}}
                                {{--<video src="{{ asset('video.mp4') }}" controls class="img-fluid"></video>--}}
                                    {{--<input type="text" name="videos_description[]" class="img-description" placeholder="Video description">--}}
                                    {{--<input type="file" name="videos[]" class="form-control media-input"  id="video-${++this.vidIndex}" accept="video/*,image/*">--}}
                            {{--</div>--}}


                            <div class="upload-btn">
                                <div class="add-media add-media-btn">
                                    <button type="button" class="fa fa-plus fa-2x" id="video-btn"></button>
                                </div>
                                <input type="file"  name="" class="form-control media-input"  id="video-input" accept="video/*">
                            </div>
                        </div>
                    </div>





                    {{--<div id="upload-pane">--}}
                        {{--<div class="form-group">--}}
                            {{--<label>Upload Media files</label>--}}
                            {{--<div class="media-files d-flex flex-wrap" id="media-files">--}}
                                {{--<div class="uploaded-media">--}}
                                    {{--<span class="fa fa-close close-btn" ></span>--}}
                                    {{--<img src="{{ asset('assets/images/slide1.jpg') }}" class="img-fluid">--}}
                                    {{--<input type="text" name="description[]" class="img-description" placeholder="Media description">--}}
                                    {{--<input type="file" name="media[]" class="form-control media-input"  id="media-1" accept="video/*,image/*" >--}}
                                {{--</div>--}}

                               {{--<div class="upload-btn" >--}}
                                   {{--<div class="add-media add-media-btn">--}}
                                       {{--<button type="button" class="fa fa-plus fa-2x" data-input="#media-1"></button>--}}
                                   {{--</div>--}}
                                   {{--<input type="file"  name="upload-f" class="form-control media-input"  id="media-1" accept="video/*,image/*">--}}
                               {{--</div>--}}


                            {{--</div>--}}

                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="inputs">
                        <input type="hidden"  name="tribe" value="{{ old('tribe') }}">
                        <input type="hidden" name="region" value="{{ old('region') }}">
                        <input type="hidden" name="country" value="{{ old('country') }}">
                        <input type="hidden" name="language" value="{{ old('language') }}">
                        <input type="hidden" name="geo-description" value="{{ old('geo-description') }}">
                        <input type="hidden" name="history" value="{{ old('history') }}">
                        <input type="hidden" name="literature" value="{{ old('literature') }}">
                        <input type="hidden" name="tradition" value="{{ old('tradition') }}">
                        <input type="hidden" name="leadership" value="{{ old('leadership') }}">
                        <input type="hidden" name="social-institution" value="{{ old('social-institution') }}">
                        <input type="hidden" name="religion" value="{{ old('religion') }}">
                        <input type="hidden" name="occupation" value="{{ old('occupation') }}">
                        <input type="hidden" name="technology" value="{{ old('technology') }}">
                        <input type="hidden" name="clothing" value="{{ old('clothing') }}">
                        <input type="hidden" name="food" value="{{ old('food') }}">
                        <input type="hidden" name="tourist-attraction" value="{{ old('tourist-attraction') }}">
                    </div>

                    <div class="action-buttons d-flex justify-content-between">
                        <button type="button" class="btn btn-primary" id="prev-btn">Previous</button>
                        <button type="button" class="btn btn-primary" id="next-btn">Next</button>
                        <button type="submit" class="btn btn-primary" id="post">Publish</button>
                    </div>



                </form>

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="{{ asset('assets/js/article.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('assets/js/editor.js') }}"></script>
@endsection
