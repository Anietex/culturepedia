<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @section('styles')
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
    @show
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/admin.css') }}">

    <title>Culturepedia  | Admin | Login </title>
</head>
<body>
<header>
    <nav class="container">
        <div class="navbar">
            <div class="logo">
                <h1>Culture<span class="colored">pedia</span></h1>
            </div>

        </div>
    </nav>
</header>
<main class="main">
  <div class="container">
      <div class="form-wrapper">
          <div class="row">
              <div class="col-sm-5 col-md-5 col-12 m-auto">
                  <div class="form">
                      <form action="{{ route('admin.login') }}" method="post">
                          {{ csrf_field() }}
                          <div class="header">
                              <p>Log In</p>
                          </div>
                         <div class="body">
                             <div class="form-group">
                                 <label for="username">Username</label>
                                 <input type="text" name="username" id="username" value="{{ old('username') }}" required class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" autofocus>
                                 @if ($errors->has('username'))
                                     <div class="invalid-feedback">
                                         {{ $errors->first('username') }}
                                     </div>
                                 @endif
                             </div>
                             <div class="form-group">
                                 <label for="password">Password</label>
                                 <input type="password" name="password" id="password" value="{{ old('password') }}"  class="form-control  {{ $errors->has('password') ? 'is-invalid' : '' }}" required>
                                 @if ($errors->has('password'))
                                     <div class="invalid-feedback">
                                         {{ $errors->first('password') }}
                                     </div>
                                 @endif

                             </div>
                             <hr>
                             <button type="submit" class="btn btn-primary">Log In</button>

                             <div class="form-check mt-2">
                                 <input class="form-check-input" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}id="remember">
                                 <label class="form-check-label" for="remember">
                                     Remember me
                                 </label>
                             </div>
                         </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
</main>
    <script type="text/javascript" src="{{  asset('assets/js/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src=" {{ asset('assets/js/custom.js') }}"></script>
</body>
</html>