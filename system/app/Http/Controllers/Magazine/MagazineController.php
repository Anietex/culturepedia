<?php

namespace App\Http\Controllers\Magazine;

use App\MagazineMediaFile;
use App\MagazinePicture;
use App\MagazinePost;
use App\MagazinePostComment;
use App\MagazinePostLike;
use App\MagazineUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class MagazineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $paginator = MagazinePost::orderBy('id','desc')->paginate(6);


        $posts = collect($paginator->items());
        $parsePost = [];

        foreach ($posts as $post){
            $media = MagazinePost::find($post->id)->media->map(function ($picture){
                return asset('storage/magazine/media/'.$picture->file_name);
            });


            $comments = MagazinePost::find($post->id)->comments->count();
            $likes = MagazinePost::find($post->id)->likes->count();

            $liked = false;

            if(\auth('api')->check()){
                $like = MagazinePostLike::where('user_id',\auth('api')->user()->user_id)
                    ->where('post_id',$post->id)->first();
                if($like){
                    $liked=true;
                }
            }


            $user = MagazinePost::find($post->id)->user->username;

            $parsePost[]=['id'=>$post->id,
                'user'=>$user,
                'media'=>$media,
                'comments'=>$comments,
                'liked'=>$liked,
                'likes'=>$likes,
                'description'=>$post->description];
        }
        return response()->json(["posts"=>$parsePost,"last_page"=>$paginator->lastPage()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['post_description'=>'required','media.*'=>'required|mimes:jpg,gif,jpeg,png,avi,flv,wmv,mov,mp4,3gp']);

        $post = new MagazinePost();
        $post->description = $request->post_description;
        $post->user_id = $request->user('api')->user_id;

        if($post->save()){
            if($request->hasFile('media')){
                $this->UploadMedia($request->file('media'),$post->id);
            }
            return response()->json(['status'=>'success','message'=>'posted successfully']);
        }
        else
            return response()->json(['status'=>'error','message'=>'post not saved'],500);
    }


    public function comment(Request $request,$id){
        $comment = new MagazinePostComment();
        $comment->post_id = $id;
        $comment->user_id =  $request->user('api')->user_id;
        $comment->comment = $request->comment;

        if($comment->save()){
            return response()->json(['status'=>'success','message'=>'comment added']);
        }
        else
            return response()->json(['status'=>'error','message'=>'comment not added'],500);
    }


    public function like(Request $request,$id){
        $like = new MagazinePostLike();
        $like->user_id = $request->user('api')->user_id;
        $like->post_id = $id;
        if($like->save())
            return response()->json(['status'=>'success']);
        return response()->json(['status'=>'error'],500);

    }


    public function unlike(Request $request, $id){
        if(MagazinePostLike::where('post_id',$id)
            ->where('user_id',$request->user('api')
                ->user_id)->delete())
              return response()->json(['status'=>'success']);
        return response()->json(['status'=>'error'],500);

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(MagazinePost $post)
    {
        $media = MagazinePost::find($post->id)->media->map(function ($picture){
            return asset('storage/magazine/media/'.$picture->file_name);
        });
        $comments = MagazinePost::find($post->id)->comments->map(function ($comment){
            $user =  User::find($comment->user_id);
            return ['id'=>$comment->id,'user'=>$user->username,'comment'=>$comment->comment];
        });

        $user = MagazinePost::find($post->id)->user;

        $liked =false;
        if(\auth('api')->check()){
            $like = MagazinePostLike::where('user_id',\auth('api')->user()->user_id)
                ->where('post_id',$post->id)->first();

            if($like){
                $liked=true;
            }
        }

                    $comms = $comments->toArray();
            usort($comms,function ($com1,$com2){
                if($com1['id'] > $com2['id'])
                    return -1;
                elseif($com1['id']< $com2['id'])
                    return 1;
                else
                    return  0;
            });
           return response()->json(['id'=>$post->id,
               'user'=>$this->getUserDetails($user->user_id),
               'media'=>$media,
               'comments'=>$comms,
               'liked'=>$liked,
               'description'=>$post->description]);
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {



        MagazinePost::find($id)->media->map(function ($picture){
           if(Storage::disk('shared_public')->delete('magazine/media/'.$picture->file_name)){
               echo "Delete";
           }
        });

        MagazinePost::destroy($id);

//        return response()->json(["status"=>"success"]);



    }


    protected function uploadMedia($files,$post_id){
        for($i=0; $i<count($files); $i++){
            $picture = new MagazineMediaFile();
            $ext = $files[$i]->getClientOriginalExtension();
            $filename = md5(rand().time()).'.'.$ext;
            $picture->post_id = $post_id;
            $picture->file_name = $filename;
            $picture->save();
            $files[$i]->storeAs("magazine/media",$filename,'shared_public');
        }
    }

    private function getUserDetails($user_id){

        $otherDetails = MagazineUser::where('user_id',$user_id)->first();
        return ['username'=>User::find($user_id)->username,
            'profile_picture'=>$otherDetails->profile_picture===null?null:asset('storage/magazine/users/'.$otherDetails->profile_picture),
            'facebook'=>$otherDetails->facebook,
            'twitter'=>$otherDetails->twitter,
            'instagram'=>$otherDetails->instagram];
    }
}
