import { ModalBox } from "./ModalBox";
import { CitationModalCreator } from "./CitationModalCreator";
import { CitationParser } from "./CitationParser";
import { CitationCreator } from "./CitationCreator";



export class CitationModal extends ModalBox{

    constructor(){
        super();

        this.creator = new CitationModalCreator();

    }

    showModal(cb){
        this.callback = cb;
        super.showModal();
        this.modal.appendChild(this.creator.getModalContent());
        this.getElements();
        this.registerEvents();

    }





    hideTabs(){
        for(const tab of this.creator.tabs){
            tab.style.display = "none";
        }

    }


    addWebsiteCitation(event){
        event.preventDefault();
        let form = event.target.elements;

        let citation = {
            type:'web',
            url:form.namedItem('url').value,
            title:form.namedItem('title').value,
            firstName:form.namedItem('first-name').value,
            lastName:form.namedItem('last-name').value,
            dateRetrieved:form.namedItem('date-retrieved').value,
        };

        this.insertCitation(citation)

    }



    addBookCitation(event){
        event.preventDefault();
        let form = event.target.elements;

        let citation = {
            type:'book',
            title:form.namedItem('title').value,
            firstName:form.namedItem('first-name').value,
            lastName:form.namedItem('last-name').value,
            publisher:form.namedItem('publisher').value,
            publisherLocation:form.namedItem('publisher-location').value,
            pages:form.namedItem('pages').value
        };


        this.insertCitation(citation)
    }


    addNewsCitation(event){
        event.preventDefault();
        let form = event.target.elements;

        let citation = {
            type:'news',
            url:form.namedItem('url').value,
            title:form.namedItem('title').value,
            firstName:form.namedItem('first-name').value,
            lastName:form.namedItem('last-name').value,
            publicationName:form.namedItem('publication-name').value,
            accessDate:form.namedItem('access-date').value,
            // sourceDate:form.namedItem('source-date').value
        };


        this.insertCitation(citation);
    }


    addJournalCitation(event){
        event.preventDefault();
        let form = event.target.elements;

        let citation = {
            type:'journal',
            url:form.namedItem('url').value,
            journal:form.namedItem('journal').value,
            title:form.namedItem('title').value,
            firstName:form.namedItem('first-name').value,
            lastName:form.namedItem('last-name').value,
            sourceDate:form.namedItem('source-date').value,
            volume:form.namedItem('volume').value,
            pages:form.namedItem('pages').value
        };


        this.insertCitation(citation);
    }


    insertCitation(citation){
        let index = this.getCitations().length+1;
        let citations = document.getElementById('references');


        switch (citation.type) {
            case 'book':
                citations.appendChild(CitationCreator.createBook(citation,index));
                break;
            case 'web':
                citations.appendChild(CitationCreator.createWeb(citation,index));
                break;
            case 'journal':
                citations.appendChild(CitationCreator.createJournal(citation,index));
                break;
            case "news":
                citations.appendChild(CitationCreator.createNews(citation,index));
                break;
        }



        this.hideModal();
        this.callback(citation,index);
    }


    getCitations(){
        let citeElms = document.querySelectorAll('#references li');

        let citations = [];

        for(const elm of citeElms){
            let cite = elm.querySelector('cite');
            if(cite.classList.contains('web')){
                citations.push(CitationParser.parseWeb(elm))
            }
            else if(cite.classList.contains('book')){
                citations.push(CitationParser.parseBook(elm));

            }else if(cite.classList.contains('journal')){
                citations.push(CitationParser.parseJournal(elm));
            }else if(cite.classList.contains('news')){
                citations.push(CitationParser.parseNews(elm));
            }

        }

        return citations;

    }








    removeActiveClass(){
        for(const btn of this.creator.tabsBtn){
            btn.classList.remove('active');
        }
    }

    showManual(event){
        this.removeActiveClass();
        this.hideTabs();
        event.target.classList.add('active');

        this.manualTab.style.display = "block";


    }


    showAutomatic(event){
        this.removeActiveClass();
        this.hideTabs();
        event.target.classList.add('active');

        this.autoTab.style.display = "block";
    }

    showReuse(event){
        this.removeActiveClass();
        this.hideTabs();
        event.target.classList.add('active');


        this.reuseTab.style.display = "block"
    }

    showWebCitation(){
        this.hideManualBtn();
        this.creator.webPane.style.display = 'block';
        this.creator.setModalTitle("Website citation");
    }


    showBookCitation(){
        this.hideManualBtn();
        this.creator.bookPane.style.display = 'block';
        this.creator.setModalTitle("Book citation");
    }


    showNewsCitation(){
        this.hideManualBtn();
        this.creator.newsPane.style.display = 'block';
        this.creator.setModalTitle("News citation");
    }


    showJournalCitation(){
        this.hideManualBtn();
        this.creator.journalCitePane.style.display = 'block';
        this.creator.setModalTitle("Journal citation");
    }


    hideManualBtn(){
        this.creator.webBtnWrapper.style.display = 'none';
        this.creator.tabBar.style.display = 'none';

    }


    generateData(){
        let value = this.creator.autoInput.value;
        if(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/.test(value)) {
            this.creator.autoInput.style.borderColor = '#BDBDBD';
            this.creator.autoButton.disabled = true;
            this.creator.autoProgresBar.style.display = 'block';

            $.getJSON('/api/url-info?url=' + value)
                .done((data) => {

                    let citation = {
                        type:'web',
                        url:'',
                        title:'',
                        firstName:'',
                        lastName:'',
                        dateRetrieved:''
                    };

                    citation.url = data.url;
                    citation.title = data.title;
                    citation.dateRetrieved = data.retrieved_on;

                    let elm =    CitationCreator.createWeb(citation,'',false);
                    this.creator.automaticTab.appendChild(elm);
                    this.creator.automaticTab.appendChild(this.creator.autoSubmitBtn);
                    this.creator.autoInputPane.style.display = 'none';
                    this.creator.autoSubmitBtn.onclick = ()=>{
                        this.insertCitation(citation);
                    }



                })
                .fail(function() {
                        alert("No Data was found for URL")
                    }
                )
                .always(
                    () =>{
                        this.creator.autoButton.disabled = false;
                        this.creator.autoProgresBar.style.display = 'none';
                    }
                );

        }
        else if(/^(?:(?:\d{10})|(?:\d{13}))$/.test(value)){
            this.creator.autoButton.disabled = true;
            this.creator.autoProgresBar.style.display = 'block';
            this.creator.autoInput.style.borderColor = '#BDBDBD';
            $.getJSON('/api/isbn-info?isbn='+value)
                .done((data)=>{
                    let citation = {
                        type:'book',
                        title:'',
                        firstName:'',
                        lastName:'',
                        publisher:'',
                        publisherLocation:'',
                        publicationYear:'',
                        pages:''
                    };

                    citation.title = data.title;
                    citation.firstName = data.author.split(' ')[0];
                    citation.lastName =  data.author.split(' ')[1];
                    citation.publisher = data.publisher;
                    citation.publicationYear = data.publication_year;
                    citation.publisherLocation = data.publisher_location;

                    let elm =    CitationCreator.createBook(citation,'',false);
                    this.creator.automaticTab.appendChild(elm);
                    this.creator.automaticTab.appendChild(this.creator.autoSubmitBtn);
                    this.creator.autoInputPane.style.display = 'none';
                    this.creator.autoSubmitBtn.onclick = ()=>{
                        this.insertCitation(citation);
                    }

                })
                .fail(function() {
                    alert("No Data was found ISBN")
                })
                .always(() =>{
                    this.creator.autoButton.disabled = false;
                    this.creator.autoProgresBar.style.display = 'none';
                });
        }else {
            this.creator.autoInput.style.borderColor = '#F44336'
        }


        // this.creator.autoButton.disabled = true;
        // this.creator.autoProgresBar.style.display = 'block';
    }



    getElements(){
        this.autoTab = this.creator.automaticTab;
        this.manualTab = this.creator.manualTab;
        this.reuseTab = this.creator.reuseTab;
        this.autoBtn = this.creator.autoBtn;
        this.manualBtn = this.creator.manualBtn;
        this.reuseBtn = this.creator.reuseBtn;
        this.webBtn = this.creator.webBtn;
    }


    registerEvents(){
        this.autoBtn.onclick = e =>{ this.showAutomatic(e)};
        this.manualBtn.onclick = e => { this.showManual(e)};
        this.reuseBtn.onclick = e => {this.showReuse(e)};
        this.webBtn.onclick = ()=>{ this.showWebCitation() };

        this.creator.cancelBtn.onclick = ()=>{
            this.hideModal();
        };

        this.creator.webCiteForm.onsubmit = e => {
            this.addWebsiteCitation(e);
        };

        this.creator.bookCiteForm.onsubmit = e => { this.addBookCitation(e) };
        this.creator.newsCiteForm.onsubmit = e => { this.addNewsCitation(e) };
        this.creator.journalCiteForm.onsubmit = e => { this.addJournalCitation(e)};


        this.creator.bookBtn.onclick = () =>{this.showBookCitation()};
        this.creator.newsBtn.onclick = () => {this.showNewsCitation()};
        this.creator.jounalBtn.onclick = () => {this.showJournalCitation()};
        this.creator.autoButton.onclick = () => {this.generateData()}
    }








}