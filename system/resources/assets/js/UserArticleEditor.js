import  { Editor} from "./Editor";
import {  LinkModal } from "./LinkModal";
import { ViewLinkPopover } from "./ViewLinkPopover";
import {AdminPostHeadingNavigator} from "./AdminPostHeadingNavigator";
import {CitationModal} from "./CitationModal";
import  { ReferencePopover } from "./ReferencePopover";
import { MediaHandler } from "./MediaHandler";


window.onload = ()=>{
    let linkModal = new LinkModal();
    let popOver = new ViewLinkPopover();
    let refModal = new CitationModal();
    let refPopover = new ReferencePopover();
    let media = new MediaHandler();
    media.getElements();
    media.addEventListener();

    let edit = new Editor('#editor',linkModal,popOver,refModal,refPopover);
    edit.init();
    let nav = new AdminPostHeadingNavigator(edit);
    nav.init();

    $.get('/api/articles',function (data) {
        linkModal.setArticleJSON(data);
    });

};