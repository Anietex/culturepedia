@extends('admin.master')
@section('styles')
    @parent
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/editor.css') }}">
@endsection
@section('title','New Article')
@section('header-title')
    <h1><span class="fa fa-pencil"></span> New Article <small>Create a new article</small></h1>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
    <li class="breadcrumb-item active" aria-current="page">New Article</li>
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            New Article
        </div>
        <div class="card-body">
            <div class="article-form">
                <div class="wrapper">
                    @if ($errors->any())
                        <div class="alert alert-danger  alert-dismissible fade show" role="alert">
                            @foreach ($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @if(session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <p>{{ session('success') }}</p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @if(session('error'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <p>{{ session('error') }}</p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <form action="{{ route('admin.article.store') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="title">Article title</label>
                            <input type="text" name="title" id="title" class="form-control input-lg" placeholder="Article title" value="{{ old('title') }}">
                        </div>
                        <label for="article"><span  id="heading"></span> </label>
                        <div class="form-group" id="text-pane">
                            <div id="editor"></div>
                            {{--<textarea class="form-control" id="article" rows="10"></textarea>--}}
                        </div>

                        <div class="upload-pictures upload-pane" id="picture-pane">
                            <label>Upload Pictures </label>
                            <small>Maximum file size of 500KB</small>
                            <div class="media-files pictures d-flex flex-wrap" id="picture-files">
                                <div class="upload-btn">
                                    <div class="add-media add-media-btn">
                                        <button type="button" class="fa fa-plus fa-2x" data-input="#media-1" id="picture-btn"></button>
                                    </div>
                                    <input type="file"  class="form-control media-input"  id="picture-input" accept="image/*">
                                </div>
                            </div>
                        </div>

                        <div class="upload-videos upload-pane" id="video-pane">

                            <label>Upload short Videos</label>
                            <small>Maximum file size of 3.5MB</small>
                            <div class="media-files pictures d-flex flex-wrap" id="video-files">

                                <div class="upload-btn">
                                    <div class="add-media add-media-btn">
                                        <button type="button" class="fa fa-plus fa-2x" id="video-btn"></button>
                                    </div>
                                    <input type="file"  name="" class="form-control media-input"  id="video-input" accept="video/*">
                                </div>
                            </div>
                        </div>

                        <div class="inputs">
                            <input type="hidden" name="tribe" value="{{ old('tribe') }}">
                            <input type="hidden" name="region" value="{{ old('region') }}">
                            <input type="hidden" name="country" value="{{ old('country') }}">
                            <input type="hidden" name="language" value="{{ old('language') }}">
                            <input type="hidden" name="geo-description" value="{{ old('geo-description') }}">
                            <input type="hidden" name="history" value="{{ old('history') }}">
                            <input type="hidden" name="literature" value="{{ old('literature') }}">
                            <input type="hidden" name="tradition" value="{{ old('tradition') }}">
                            <input type="hidden" name="leadership" value="{{ old('leadership') }}">
                            <input type="hidden" name="social-institution" value="{{ old('social-institution') }}">
                            <input type="hidden" name="religion" value="{{ old('religion') }}">
                            <input type="hidden" name="festivals" value="{{ old('festivals') }}">
                            <input type="hidden" name="occupation" value="{{ old('occupation') }}">
                            <input type="hidden" name="technology" value="{{ old('technology') }}">
                            <input type="hidden" name="clothing" value="{{ old('clothing') }}">
                            <input type="hidden" name="food" value="{{ old('food') }}">
                            <input type="hidden" name="tourist-attraction" value="{{ old('tourist-attraction') }}">
                            <input type="hidden" name="other-traditions" value="{{ old('other-traditions') }}">
                            <input type="hidden" name="references" value="{{ old('references') }}">

                        </div>

                        <div class="action-buttons d-flex justify-content-between">
                            <button type="button" class="btn btn-primary" id="prev-btn">Previous</button>
                            <button type="button" class="btn btn-primary nxt-btn" id="next-btn">Next</button>
                            <button type="submit" class="btn btn-primary" id="post">Publish</button>
                        </div>

                        <div class="d-none" id="citations">
                            <ol id="references">

                            </ol>
                        </div>

                        <div class="d-none" id="fake-editor">
                            <div class="d-none" id="tribe"> {{ old('tribe') }}</div>
                            <div class="d-none" id="region"> {{ old('region') }}</div>
                            <div class="d-none" id="country"> {{ old('country') }}</div>
                            <div class="d-none" id="language"> {{ old('language') }}</div>
                            <div class="d-none" id="geo-description">{{ old('geo-description') }}</div>
                            <div class="d-none" id="history"> {{ old('history') }}</div>
                            <div class="d-none" id="literature"> {{ old('literature') }}</div>
                            <div class="d-none" id="tradition"> {{ old('tradition') }}</div>
                            <div class="d-none" id="leadership"> {{ old('leadership') }}</div>
                            <div class="d-none" id="social-institution">{{ old('social-institution') }}</div>
                            <div class="d-none" id="religion"> {{ old('religion') }}</div>
                            <div class="d-none" id="festivals"> {{ old('festivals') }}</div>
                            <div class="d-none" id="occupation">{{ old('occupation') }}</div>
                            <div class="d-none" id="technology"> {{ old('technology') }}</div>
                            <div class="d-none" id="clothing">{{ old('clothing') }}</div>
                            <div class="d-none" id="food"> {{ old('food') }}</div>
                            <div class="d-none" id="tourist-attraction">{{ old('tourist-attraction') }}</div>
                            <div class="d-none" id="other-traditions"> {{ old('other-traditions') }}</div>
                          </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


@endsection
@section('scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.26.0/polyfill.js"></script>
    {{--<script type="text/javascript" src=" {{ asset('assets/js/admin-article.js') }}" ></script>--}}
    <script type="text/javascript" src="{{ asset('assets/js/admin-editor.bundle.js') }}"></script>
@endsection