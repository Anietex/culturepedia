<?php

namespace App\Http\Controllers;

use App\ContactModel;
use App\FeaturedArticle;
use App\Message;
use DateTime;
use Illuminate\Http\Request;
use App\Article;
use Illuminate\Support\Facades\DB;
class GuestController extends Controller
{
    public function index(){
        $paginator = Article::where('published',true)
            ->where('approved',true)->orderBy('created_at','desc')->paginate(5);
        $articles = collect($paginator->items());
        $formatted = $articles->map(function ($article,$key){
            $article->tribe = substr($article->tribe.$article->region.$article->country.$article->language.$article->geo_description,0,550).'...';
            return $article;
        })->all();


        $featuredArticles = $this->getFeaturedArticle();

        return view('guest.index')->with(['articles'=>$formatted,'paginator'=>$paginator,'featuredArticles'=>$featuredArticles]);
    }

    public function search(Request $request){
        if(!$request->has('query')){
            return redirect('/');
        }else{

            $articles = collect(DB::select('select * from articles where match(title) against (? ) and published = true and approved = true',[$request->input('query')]));
            $formatted = $articles->map(function ($article,$key){
                $article->tribe = substr($article->tribe.
                        $article->region.
                        $article->country.
                        $article->language.
                        $article->geo_description,
                        0,550).'...';
                $date = new \DateTime($article->updated_at);
                $article->updated_at = $date->format('F d, Y');

                return $article;
            })->all();


            return view('guest.search')->with(['articles'=>$formatted,'query'=>$request->input('query')]);

        }
    }


    public function articles(){
        $paginator = Article::where('published',true)
            ->where('approved',true)->orderBy('created_at','desc')->paginate(5);
        $articles = collect($paginator->items());
        $formatted = $articles->map(function ($article,$key){

           $article->tribe = substr($article->tribe.$article->region.$article->country.$article->language.$article->geo_description,0,550).'...';
            return $article;
        })->all();

       return view('guest.articles')->with(['articles'=>$formatted,'paginator'=>$paginator]);
    }


    public function article(Article $article){

      $pictures = Article::find($article->article_id)->pictures;
      $videos = Article::find($article->article_id)->videos;

      return view('guest.article')->with(['article'=>$article,'pictures'=>$pictures,'videos'=>$videos]);
    }


    public function postLinks(Request $request)
    {
        $formatted = [];
        if (!$request->has('query')) {
            $articles = DB::select('select article_id,title  from articles where  published = true and approved = true');
        } else {
            $articles = DB::select('select article_id,title from articles where match(title) against (? ) and published = true and approved = true', [$request->input('query')]);
        }

       foreach ($articles as $article){
            $formatted[] = ['link'=>url("/article/$article->article_id/".str_slug($article->title,'_')),'title'=>$article->title];
       }

       //return json_encode($formatted);
        header('Access-Control-Allow-Origin:*');
        return response()->json($formatted);


    }

    public function getFeaturedArticle(){
        $featured = FeaturedArticle::find(1);

       if(is_null($featured)){
          $this->setFeaturedArticle();
       }elseif( $featured->expire_at<time()){

           $allFeatured = FeaturedArticle::all();
           foreach ($allFeatured as $article){
             FeaturedArticle::destroy($article->id);
           }
          $this->setFeaturedArticle();
       }
       $fas = FeaturedArticle::all();
       $articles = [];

       foreach ($fas as $article){
           array_push($articles,Article::find($article->article_id));
       }

       return $articles;

//        $featured = FeaturedArticle::find(1);
//
//        $article = Article::find($featured->article_id);
//        if(is_null($article))
//            return redirect(url('/'));
//
//        return redirect(url('article/'.$article->article_id.'/'.str_slug($article->title)));
    }


    public function contact(){
        return view('guest/contact');
    }


    public function sendMessage(Request $request){
        $this->validate($request,[
            'full_name'=>'required',
            'email'=>'required|email',
            'message'=>'required'
        ]);

        $message = new Message();

        $message->full_name = $request->input('full_name');
        $message->email = $request->input('email');
        $message->message =$request->input('message');

        if($message->save()){
            return redirect(url()->previous())->with('success',"Your message was sent successfully we will get back to you shortly");
        }else{
            return redirect(url()->previous())->with('error',"Your message was not sent, please try again");
        }
    }




    public function randomArticle(){
        $article = Article::where('approved',true)->where('published',true)->inRandomOrder()->get()->first();
        if(is_null($article))
            return redirect(url('/'));

        return redirect(url('article/'.$article->article_id.'/'.str_slug($article->title)));

    }

    public function privacyPolicy(){
        return view('guest.privacy_policy');
    }

    public function termsOfUse(){
        return view('guest.terms_of_use');
    }

    private function setFeaturedArticle(){
        $articles = Article::where('approved',true)->where('published',true)->inRandomOrder()->take(5)->get();
        $id= 1;
        foreach ($articles as $article){
            $featured = new FeaturedArticle();
            $featured->id = $id++;
            $featured->article_id =
                $article->article_id;
            $featured->expire_at = $this->midNight();
            $featured->save();
        }

    }

    private function midNight(){
        $secs = date('s');
        $min = date('i');
        $hrs = date('G');
        $midNight = (time()-(($min*60)+($hrs*60)+$secs))+24*60*60;
        return $midNight;
    }





}
