<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="app-url" content="{{url('/')}}">
    @section('styles')
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
    @show
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">

    <title>Culturepedia  |  @yield('title')</title>
</head>
<body>



<header>
   @include('layouts.navigation')
</header>

<section class="subheader">
    <div class="container">
        <div class="sub-wrapper d-flex flex-column-reverse flex-sm-row flex-lg-row justify-content-sm-between" >
            <div class="text">
                <p> @yield('section-title')</p>
            </div>

            <div class="search">
                <div class="seearch-wrapper">
                    <form action="{{ url('/search') }}" method="get" id="search-form">
                        <div id="article-search">
                            <input type="search" name="query" class="search" placeholder="Search articles ...">
                        </div>
                    </form>
                </div>


            </div>

        </div>
    </div>
</section>
<main class="main">
    <section class="content">
        @yield('content')
    </section>

    <div class="translate">
        <div id="google_translate_element"></div>
        <script type="text/javascript">
            function googleTranslateElementInit() {
                new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
            }
        </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

    </div>
</main>
<footer class="text-center">
    <div class="links">
        <p> <a class="nav-link" href="{{ url('/contact-us') }}">Contact Us</a>  <a href="{{ url('privacy_policy') }}">Privacy Policy</a> | <a href="{{ url('terms_of_use') }}">Terms of Use</a> </p>
    </div>
    <div class="copyright">
        <p>&copy; {{ date('Y') }} <a href="{{ url('/') }}">Culturepedia </a> | Powered by <a href="http://springlight.ng">Springlight Technology</a> </p>
    </div>
</footer>
@section('scripts')
    <script type="text/javascript" src="{{  asset('assets/js/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/typeahead.bundle.js') }}"></script>
@show
<script type="text/javascript" src=" {{ asset('assets/js/custom.js') }}"></script>
</body>
</html>