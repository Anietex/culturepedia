@extends('user.master')
@section('styles')
    @parent
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/datatables/datatables.css') }}">
@endsection
@section('title','Approved Articles')
@section('header-title')
    <h1><span class="fa fa-toggle-on"></span> Approved Articles<small> Manage your articles</small></h1>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item active" aria-current="page">Approved Articles</li>
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            Approved Articles
        </div>
        <div class="card-body">
            @include('user.articles_table')
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script type="text/javascript" src="{{  asset('assets/datatables/datatables.js') }}"></script>
    <script>
        $(document).ready( function () {
            $('.table').DataTable({
                autoFill: true
            });
        } );
    </script>
@endsection