@extends("layouts.master")
@section('title','Password Reset')
@section('section-title','Password Reset')
@section('content')

    <div class="container">
        <div class="form-wrapper my-4">
            <div class="registration">
                <h3>Password Reset</h3>
                <div class="form">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form">
                                <form method="post" action="{{ route('password.request') }}">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="token" value="{{ $token }}">
                                    <div class="form-group">
                                        <label for="email">Email Address</label>
                                        <input type="email" name="email" id="email" value="{{ old('email') }}" required class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" autofocus>
                                        @if ($errors->has('email'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('email') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" id="password" value="{{ old('password') }}"  class="form-control  {{ $errors->has('password') ? 'is-invalid' : '' }}" required>
                                        @if ($errors->has('password'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('password') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="password_confirmation">Confirm Password</label>
                                        <input type="password" name="password_confirmation" id="password_confirmation" value="{{ old('password_confirmation') }}"  class="form-control  {{ $errors->has('password') ? 'is-invalid' : '' }}" required>
                                        @if ($errors->has('password_confirmation'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('password_confirmation') }}
                                            </div>
                                        @endif
                                    </div>
                                    <button type="submit" class="btn btn-block btn-primary">Reset Password</button>
                                </form>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="info ml-5 text-center">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
