<?php

namespace App\Http\Controllers\Magazine;

use App\MagazinePost;
use App\MagazinePostLike;
use App\MagazineUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function posts($username)
    {
        $user = User::where('username',$username)->first();
        if($user){
            $posts = MagazinePost::where("user_id",$user->user_id)->orderBy('id','desc')->get();
            $parsePost = [];

            foreach ($posts as $post){
                $media = MagazinePost::find($post->id)->media->map(function ($picture){
                    return asset('storage/magazine/media/'.$picture->file_name);
                });
                $comments = MagazinePost::find($post->id)->comments->map(function ($comment){
                    $user =  User::find($comment->user_id);
                    return ['user'=>$user->username,'comment'=>$comment->comment];
                });

                $liked = false;

                if(\auth('api')->check()){
                    $like = MagazinePostLike::where('user_id',\auth('api')->user()->user_id)
                        ->where('post_id',$post->id)->first();

                    if($like){
                        $liked=true;
                    }
                }

                $author = MagazinePost::find($post->id)->user->username;
                $parsePost[]=['id'=>$post->id,
                    'user'=>$author,
                    'media'=>$media,
                    'comments'=>$comments,
                    'liked'=>$liked,
                    'description'=>$post->description];
            }
            $userPost = ["user"=>$this->getUserDetails($user->user_id),"posts" => $parsePost];

            return response()->json($userPost);
        }else{
            abort(404);
        }


    }


    public function getAccountInfo(Request $request){

        $posts = MagazinePost::where("user_id",$request->user('api')->user_id)
                ->orderBy('id','desc')
                ->get();

            $parsePost = [];

            foreach ($posts as $post){
                $media = MagazinePost::find($post->id)->media->map(function ($picture){
                    return asset('storage/magazine/media/'.$picture->file_name);
                });
                $comments = MagazinePost::find($post->id)->comments->map(function ($comment){
                    $user =  User::find($comment->user_id);
                    return ['user'=>$user->username,'comment'=>$comment->comment];
                });

                $liked = false;

                if(\auth('api')->check()){
                    $like = MagazinePostLike::where('user_id',\auth('api')->user()->user_id)
                        ->where('post_id',$post->id)->first();

                    if($like){
                        $liked=true;
                    }
                }




                $author = MagazinePost::find($post->id)->user->username;
                $parsePost[]=['id'=>$post->id,
                    'user'=>$author,
                    'media'=>$media,
                    'comments'=>$comments,
                    'liked'=>$liked,
                    'description'=>$post->description];
            }

            $userPost = ["user"=>$this->getUserDetails($request->user('api')->user_id),"posts" => $parsePost];

            return response()->json($userPost);


    }


    public function uploadProfilePhoto(Request $request){

        $this->validate($request,['profile_picture'=>'required|image']);

        $user = MagazineUser::where('user_id',$request->user('api')->user_id)->first();
        \Storage::delete('users/'.$user->profile_picture);
        $picture = $request->file('profile_picture');

        $ext = $picture->getClientOriginalExtension();
        $filename = md5(rand().time()).'.'.$ext;
        $user->profile_picture = $filename;

        if($user->save()){
            $picture->storeAs("magazine/users",$filename,'shared_public');
            return response()->json(["status"=>"success","message"=>"picture uploaded"]);
        }else{
            return response()->json(["status"=>"error","message"=>"picture not uploaded"]);
        }

    }


    public function updateSocials(Request $request){

        $user =  MagazineUser::where('user_id',$request->user('api')->user_id)->first();

        $user->facebook = $request->facebook;
        $user->twitter = $request->twitter;
        $user->instagram = $request->instagram;


       if($user->save()){
           return response()->json(["status"=>"success","message"=>"socials was updated"]);
       }else{
           return response()->json(['status'=>"error",'message'=>"social was not updated"],500);
       }
    }

    public function updatePassword(Request $request){
        $this->validate($request,['password'=>'required|confirmed']);

        $user = User::find($request->user('api')->user_id);
        $user->password = bcrypt($request->password);
        if( $user->save()){
            return response()->json(["status"=>"successful","message"=>"password was updated"]);
        }else{
            return response()->json(["status"=>"error","message"=>"password was not updated"],500);
        }
    }



    private function getUserDetails($user_id){

        $otherDetails = MagazineUser::where('user_id',$user_id)->first();
        return ['username'=>User::find($user_id)->username,
            'profile_picture'=>$otherDetails->profile_picture===null?null:asset('storage/magazine/users/'.$otherDetails->profile_picture),
            'facebook'=>$otherDetails->facebook,
            'twitter'=>$otherDetails->twitter,
            'instagram'=>$otherDetails->instagram];
    }
}
