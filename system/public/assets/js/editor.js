'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Commands = function () {
    function Commands() {
        _classCallCheck(this, Commands);
    }

    _createClass(Commands, null, [{
        key: 'bold',
        value: function bold() {
            document.execCommand('bold', false, null);
        }
    }, {
        key: 'italic',
        value: function italic() {
            document.execCommand('italic', false, null);
        }
    }, {
        key: 'orderedList',
        value: function orderedList() {
            document.execCommand('insertOrderedList', false, null);
        }
    }, {
        key: 'unorderedList',
        value: function unorderedList() {
            document.execCommand('insertUnorderedList', false, null);
        }
    }, {
        key: 'undo',
        value: function undo() {
            document.execCommand('undo', false, null);
        }
    }, {
        key: 'redo',
        value: function redo() {
            document.execCommand('redo', false, null);
        }
    }, {
        key: 'createLink',
        value: function createLink(link, text, title, def) {
            if (def === '' && link === '#') return;
            if (link === '#' && def !== '') {
                document.execCommand('insertHTML', false, '<span class="def-text" data-def="' + def + '">' + text + '</span>');
            } else {
                document.execCommand('insertHTML', false, '<a href="' + link + '" title="' + title + '" class="article-link" target="_blank" data-def="' + def + '">' + text + '</a>');
            }
        }
    }]);

    return Commands;
}();

var Editor = function () {
    function Editor(container) {
        _classCallCheck(this, Editor);

        this.container = container;
    }

    _createClass(Editor, [{
        key: 'getElements',
        value: function getElements() {
            this.undoBtn = document.getElementById('undo');
            this.redoBtn = document.getElementById('redo');
            this.boldBtn = document.getElementById("bold");
            this.italicBtn = document.getElementById("italic");
            this.linkBtn = document.getElementById('link');
            this.listUlBtn = document.getElementById('unlist');
            this.listOlBtn = document.getElementById('orlist');
            this.indentBtn = document.getElementById('in-indent');
            this.outdentBtn = document.getElementById('red-indent');
            this.balloon = document.getElementById('balloon');
            this.editor = document.getElementById("editor-text");
            this.mX = 0;
            this.mY = 0;
            this.popOver = new EditorPopover();
        }
    }, {
        key: 'init',
        value: function init() {
            this.createEditor();
            this.getElements();
            this.editor.contentEditable = true;
            this.registerEvents();
            this.addLinkEventListener();
        }
    }, {
        key: 'handleAddLink',
        value: function handleAddLink() {

            var sel = void 0;
            if (window.getSelection()) sel = window.getSelection();else {
                sel = document.selection;
            }

            var cor = getCaretPosition();
            this.mX = cor.x;
            this.mY = cor.y;
            // console.log(cor)
            var range = '';
            var textNode = void 0;
            var start = void 0;
            var end = void 0;
            var _this = this;

            if (this.mX !== 0 || this.mY !== 0) {
                if (sel.anchorNode !== null) {
                    var selected = sel.toString();
                    if (sel.toString() === '') {

                        var pos = sel.focusOffset;
                        var text = sel.focusNode.textContent;
                        textNode = sel.focusNode;
                        if (pos !== text.length) {
                            if (!/\s/.test(text.charAt(pos))) {
                                var index = text.indexOf(' ', pos);
                                pos = index === -1 ? text.length : index;
                            }
                        }

                        var preText = text.substr(0, pos);
                        range = sel.getRangeAt(0);

                        var words = preText.split(' ');
                        var lastWord = words[words.length - 1];
                        end = pos;
                        start = pos - lastWord.length;
                        range.setStart(textNode, start);
                        range.setEnd(textNode, end);
                        selected = sel.toString();
                    } else {
                        textNode = sel.focusNode;
                        start = sel.anchorOffset;
                        end = sel.toString().length + start;
                    }

                    this.popOver.showEditLink(selected, this.mX, this.mY, function (reply) {

                        selectText();
                        var title = reply.external_url_title;
                        var url = reply.external_url_link;
                        var definition = reply.definition;
                        title = title === undefined || title === null || title === '' ? '' : title;
                        url = url === undefined || url === null || url === '' ? '#' : url;
                        definition = definition === undefined || definition === null || definition === '' ? '' : definition;
                        Commands.createLink(url, selected, title, definition);
                        _this.addLinkEventListener();
                        _this.linkAddedListener();
                    });
                }
            }

            function selectText() {
                var range = document.createRange();
                sel.removeAllRanges();

                range.selectNode(_this.editor);
                sel.addRange(range);
                range.setStart(textNode, start);
                range.setEnd(textNode, end);
            }
            function getCaretPosition() {
                var x = 0;
                var y = 0;
                var sel = window.getSelection();
                if (sel.rangeCount) {
                    var range = sel.getRangeAt(0).cloneRange();
                    if (range.getClientRects()) {
                        range.collapse(true);
                        var rect = range.getClientRects()[0];
                        if (rect) {
                            y = rect.top + $(window).scrollTop();
                            x = rect.left;
                        }
                    }
                }
                return {
                    x: x,
                    y: y
                };
            }
        }
    }, {
        key: 'registerEvents',
        value: function registerEvents() {
            var _this2 = this;

            this.boldBtn.addEventListener('click', Commands.bold, false);
            this.italicBtn.addEventListener('click', Commands.italic, false);
            this.listOlBtn.addEventListener('click', Commands.orderedList);
            this.listUlBtn.addEventListener('click', Commands.unorderedList, false);
            this.undoBtn.addEventListener('click', Commands.undo, false);
            this.redoBtn.addEventListener('click', Commands.redo, false);
            this.editor.addEventListener('mousedown', function (event) {
                _this2.hideBalloon(event);
            }, true);
            this.editor.addEventListener('keyup', function () {
                _this2.hideBalloon();
            });
            this.linkBtn.addEventListener('click', function (event) {
                _this2.handleAddLink();
            });
            this.addLinkEventListener();
        }
    }, {
        key: 'showLinkInfoPopover',
        value: function showLinkInfoPopover(event) {
            var anchor = event.target;
            var link = anchor.href;
            var text = anchor.innerText;
            var title = anchor.title;

            this.popOver.showViewLink(link, text, title, anchor.dataset.def, event);
        }
    }, {
        key: 'hideBalloon',
        value: function hideBalloon(event) {
            this.popOver.hidePopover();
        }
    }, {
        key: 'addLinkEventListener',
        value: function addLinkEventListener(event) {
            var _this3 = this;

            this.editorLinks = this.editor.querySelectorAll('a');
            var defTexts = this.editor.querySelectorAll('.def-text');

            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                var _loop = function _loop() {
                    var def = _step.value;


                    def.addEventListener('mousedown', function (event) {

                        var fakeEvent = {
                            target: {
                                href: '',
                                innerText: def.innerText,
                                title: '',
                                dataset: def.dataset
                            },
                            pageX: event.pageX,
                            pageY: event.pageY
                        };
                        _this3.showLinkInfoPopover(fakeEvent);
                    });
                };

                for (var _iterator = defTexts[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    _loop();
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;

            try {
                for (var _iterator2 = this.editorLinks[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                    var link = _step2.value;

                    if (link instanceof HTMLAnchorElement) {
                        link.addEventListener('mousedown', function (event) {
                            _this3.showLinkInfoPopover(event);
                        });
                    }
                }
            } catch (err) {
                _didIteratorError2 = true;
                _iteratorError2 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion2 && _iterator2.return) {
                        _iterator2.return();
                    }
                } finally {
                    if (_didIteratorError2) {
                        throw _iteratorError2;
                    }
                }
            }
        }
    }, {
        key: 'createEditor',
        value: function createEditor() {
            var container = document.querySelector(this.container);

            function createActionBar() {
                return '\n                <div class="action-bar">\n            <button type="button" id="undo" title="Undo"><span class="fa fa-reply"></span></button>\n            <button type="button" id="redo" title="Redo"><span class="fa fa-share"></span></button>\n            <button type="button" id="bold" title="Bold Ctrl+B"> <span class="fa fa-bold" ></span></button>\n            <button type="button"  id="italic" title="Italic"><span class="fa fa-italic" ></span></button>\n            <button type="button" id="link" title="Link"><span class="fa fa-link" ></span></button>\n            <button type="button" id="unlist"><span class="fa fa-list-ul" ></span></button>\n            <button type="button"  id="orlist"><span class="fa fa-list-ol"></span></button>\n            <!--<button type="button" id="in-indent"><span class="fa fa-indent" ></span></button>-->\n            <!--<button type="button" id="red-indent"><span class="fa fa-outdent" ></span></button>-->\n        </div>';
            }

            function createEditorArea() {
                return '<div class="text-wrapper"><div class="text-area" id="editor-text"><span></span></div></div>';
            }

            var editorWrapper = document.createElement('div');
            var editor = document.createElement('div');
            editorWrapper.classList.add('editor-wrapper');
            editor.classList.add('editor');
            editor.innerHTML = createActionBar() + createEditorArea();

            editorWrapper.appendChild(editor);
            container.appendChild(editorWrapper);
        }
    }, {
        key: 'setArticleJSON',
        value: function setArticleJSON(articleJSON) {
            this.popOver.setArticleJSON(articleJSON);
        }
    }]);

    return Editor;
}();

var PopoverCreator = function () {
    function PopoverCreator() {
        _classCallCheck(this, PopoverCreator);

        this.element = this.createElement();
        this.linkView = this.getLinkView();
        this.linkEdit = this.getLinkEdit();
    }

    _createClass(PopoverCreator, [{
        key: 'createElement',
        value: function createElement() {
            var wrapper = this.getWrapper();
            var inner = this.innerWrapper();
            inner.appendChild(this.getLinkView());
            inner.appendChild(this.getLinkEdit());
            wrapper.appendChild(inner);
            return wrapper;
        }
    }, {
        key: 'innerWrapper',
        value: function innerWrapper() {
            var innerWrapper = document.createElement('div');
            innerWrapper.classList.add('baloon');
            return innerWrapper;
        }
    }, {
        key: 'getWrapper',
        value: function getWrapper() {
            var wrapper = document.createElement('div');
            wrapper.id = "balloon";
            wrapper.classList.add('balloon-wrapper');
            return wrapper;
        }
    }, {
        key: 'getLinkView',
        value: function getLinkView() {
            var lv = document.createElement('div');
            lv.classList.add('link-view');
            lv.id = "link-view";

            var header = function header() {
                var wrapper = document.createElement('div');
                wrapper.classList.add("header");
                var title = document.createElement('div');
                title.classList.add('title');
                var p = document.createElement('p');
                p.innerHTML = '<span class="fa fa-link"></span> Link';
                title.appendChild(p);
                wrapper.appendChild(title);
                // let buttons = document.createElement('div');
                // buttons.classList.add('buttons');
                // buttons.innerHTML = `<button class="" id="close-btn"><span class="fa fa-ban"></span></button><button id="edit-btn">Edit</button>`;
                // wrapper.appendChild(buttons);
                return wrapper;
            };
            var links = function links() {
                var links = document.createElement('div');
                links.classList.add('links');
                links.innerHTML = '<div class="link-item">\n                   <h4><a id="viewed-link" href="#" target="_blank"></a></h4>\n                   <p id="def-text"></p>\n               </div>';
                return links;
            };
            lv.appendChild(header());
            lv.appendChild(links());

            return lv;
        }
    }, {
        key: 'getLinkEdit',
        value: function getLinkEdit() {
            var le = document.createElement('div');
            le.classList.add('link-edit');
            le.id = "link-edit";

            var getHeader = function getHeader() {
                var header = document.createElement('div');
                header.classList.add('header');
                header.innerHTML = '<div class="left">\n                    <button id="cancel-btn">Cancel</button>\n                </div>\n                <div class="center">\n                    <p>Link</p>\n                </div>\n                <div class="right">\n                    <button class="" id="done-btn">Done</button>\n                </div>';
                return header;
            };
            var body = function body() {
                var bd = document.createElement('div');
                var tabWrap = document.createElement('div');
                bd.appendChild(tabWrap);
                tabWrap.classList.add('tabs-wrapper');
                bd.classList.add('body');
                var buttons = function buttons() {
                    var btns = document.createElement('div');
                    btns.classList.add('tabs-btn');
                    btns.innerHTML = '<button class="active" id="search-btn">Search Pages</button>\n                        <button class="" id="external-btn">External Links</button>\n                        <button class="" id="def-btn">Definition</button>';
                    return btns;
                };
                var tabs = function tabs() {
                    var tabs = document.createElement('div');
                    tabs.classList.add('tabs');

                    var searchTab = function searchTab() {
                        var st = document.createElement('div');
                        st.classList.add('tab');
                        st.id = 'search-tab';

                        st.innerHTML = '<div class="search-box">\n                                <input type="search" id="search-box">\n                            </div>\n                            <div class="search-result" id="search-result">\n                \n                               \n                            </div>\n                                 ';
                        return st;
                    };
                    var extTab = function extTab() {
                        var ext = document.createElement('div');
                        ext.classList.add('tab');
                        ext.id = "external-tab";
                        ext.innerHTML = '\n                        <div class="link-box">\n                                 <label>Title</label>\n                                <input type="url" id="ext-title">\n                                \n                                <label>Url</label>\n                                <input type="url" id="ext-url">\n                            </div>\n                    ';
                        return ext;
                    };
                    var defTab = function defTab() {
                        var def = document.createElement('div');
                        def.classList.add('tab');
                        def.id = 'definition-tab';
                        def.innerHTML = '<div class="text-area">\n                                <label>Enter the word definition</label>\n                                <textarea rows="5" id="definition"></textarea>\n                            </div>';

                        return def;
                    };
                    tabs.appendChild(searchTab());
                    tabs.appendChild(extTab());
                    tabs.appendChild(defTab());
                    return tabs;
                };
                tabWrap.appendChild(buttons());
                tabWrap.appendChild(tabs());
                return bd;
            };
            le.appendChild(getHeader());
            le.appendChild(body());

            return le;
        }
    }, {
        key: 'attachElement',
        value: function attachElement() {
            document.body.appendChild(this.element);
        }
    }, {
        key: 'showViewLink',
        value: function showViewLink(link, text, title, def) {
            if (document.body.contains(this.element)) {
                document.body.removeChild(this.element);
            }
            this.attachElement();
            var defText = document.getElementById('def-text');
            var vlink = document.getElementById('viewed-link');

            defText.innerText = def;
            if (!def) defText.innerText = title;
            vlink.innerText = text;
            vlink.title = title;
            vlink.href = link;
            this.element.style.display = "block";
            document.querySelector("#link-view").style.display = "block";
            document.querySelector("#link-edit").style.display = "none";
        }
    }, {
        key: 'showEditLink',
        value: function showEditLink() {
            if (document.body.contains(this.element)) {
                document.body.removeChild(this.element);
            }
            this.attachElement();
            this.element.style.display = "block";
            document.querySelector("#link-view").style.display = "none";
            document.querySelector("#link-edit").style.display = "block";
        }
    }, {
        key: 'hidePopover',
        value: function hidePopover() {
            if (document.body.contains(this.element)) {
                document.body.removeChild(this.element);
            }
        }
    }]);

    return PopoverCreator;
}();

var EditorPopover = function () {
    function EditorPopover() {
        _classCallCheck(this, EditorPopover);

        this.pop = new PopoverCreator();
    }

    _createClass(EditorPopover, [{
        key: 'showViewLink',
        value: function showViewLink(link, text, title, def, event) {
            this.pop.showViewLink(link, text, title, def);
            this.getElements();
            this.registerEvents();
            var x = void 0,
                y = void 0;
            x = event.pageX;
            y = event.pageY;
            this.positionElement(this.pop.element, x, y);
        }
    }, {
        key: 'showEditLink',
        value: function showEditLink(text, x, y, cb) {
            this.pop.showEditLink();
            this.getElements();
            this.registerEvents();
            this.reset();
            document.getElementById('search-box').value = text;
            this.addlinks(text, cb);
            this.positionElement(this.pop.element, x, y);
            var _this = this;
            this.doneBtn.onclick = function () {
                var external_url_title = document.querySelector('#ext-title').value;
                var external_url_link = document.querySelector("#ext-url").value;
                var definition = document.querySelector("#definition").value;
                var reply = {
                    external_url_title: external_url_title,
                    external_url_link: external_url_link,
                    definition: definition
                };
                cb(reply);
                _this.hidePopover();
            };
        }
    }, {
        key: 'addlinks',
        value: function addlinks(text, cb) {
            if (text) {
                var resultPane = document.getElementById('search-result');
                var articleJson = this.articleJSON;

                var filtered = articleJson.filter(function (article) {
                    var re = new RegExp(text, 'i');
                    return re.test(article.title);
                });
                resultPane.innerHTML = '';
                filtered.forEach(function (item) {
                    resultPane.innerHTML += '<p class="result"><span class="fa fa-link"></span> <a href="' + item.link + '" target="_blank" title="' + item.title + '" class="link-result">' + item.title + '</a> </p>';
                });
                this.addLinkSelectEvent(cb);
            }
        }
    }, {
        key: 'addLinkSelectEvent',
        value: function addLinkSelectEvent(cb) {
            var _this4 = this;

            var links = document.querySelectorAll('.link-result');
            var _iteratorNormalCompletion3 = true;
            var _didIteratorError3 = false;
            var _iteratorError3 = undefined;

            try {
                for (var _iterator3 = links[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                    var link = _step3.value;

                    link.onclick = function (event) {
                        event.preventDefault();
                        var elm = event.target;
                        var external_url_title = elm.title;
                        var external_url_link = elm.href;
                        var definition = '';
                        var reply = {
                            external_url_title: external_url_title,
                            external_url_link: external_url_link,
                            definition: definition
                        };
                        cb(reply);
                        _this4.hidePopover();
                    };
                }
            } catch (err) {
                _didIteratorError3 = true;
                _iteratorError3 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion3 && _iterator3.return) {
                        _iterator3.return();
                    }
                } finally {
                    if (_didIteratorError3) {
                        throw _iteratorError3;
                    }
                }
            }
        }
    }, {
        key: 'hidePopover',
        value: function hidePopover() {
            this.pop.hidePopover();
        }
    }, {
        key: 'getElements',
        value: function getElements() {
            this.searchBtn = document.querySelector('#search-btn');
            this.externalBtn = document.querySelector('#external-btn');
            this.defBtn = document.querySelector("#def-btn");
            this.tabs = document.querySelectorAll('.tab');
            this.tabBtns = document.querySelectorAll(".tabs-btn button");
            this.searchTab = document.querySelector('#search-tab');
            this.externalTab = document.querySelector('#external-tab');
            this.defTab = document.querySelector('#definition-tab');
            this.viewLink = document.querySelector('#link-view');
            this.editLink = document.querySelector('#link-edit');
            this.editBtn = document.querySelector("#edit-btn");
            this.cancelBtn = document.querySelector("#cancel-btn");
            this.doneBtn = document.querySelector("#done-btn");
            this.closeBtn = document.querySelector("#close-btn");
        }
    }, {
        key: 'positionElement',
        value: function positionElement(element, x, y) {

            element.style.left = x - 133 + "px";
            element.style.top = y + 40 + "px";
        }
    }, {
        key: 'registerEvents',
        value: function registerEvents() {
            var _this5 = this;

            this.externalBtn.addEventListener('click', function (event) {
                _this5.showExternal(event);
            }, false);
            this.searchBtn.addEventListener('click', function (event) {
                _this5.showSearch(event);
            }, false);
            this.defBtn.addEventListener('click', function (event) {
                _this5.showDef(event);
            }, false);
            //   this.editBtn.addEventListener('click',()=>{this.showEditPane()});
            this.cancelBtn.addEventListener('click', function () {
                _this5.hidePopover();
            });
            //   this.closeBtn.addEventListener('click',()=>{this.hidePopover()});
        }
    }, {
        key: 'showEditPane',
        value: function showEditPane() {
            var vlink = document.getElementById('viewed-link');
            var searchbox = document.getElementById('search-box');

            searchbox.value = vlink.innerText;
            this.viewLink.style.display = "none";
            this.editLink.style.display = "block";
        }
    }, {
        key: 'showExternal',
        value: function showExternal(event) {
            var target = event.target;
            this.removeActionClass();
            this.hideTabs();
            target.classList.add('active');
            this.externalTab.style.display = "block";
        }
    }, {
        key: 'showSearch',
        value: function showSearch(event) {
            var target = event.target;
            this.removeActionClass();
            this.hideTabs();
            target.classList.add('active');
            this.searchTab.style.display = "block";
        }
    }, {
        key: 'showDef',
        value: function showDef(event) {
            var target = event.target;
            this.removeActionClass();
            this.hideTabs();
            target.classList.add('active');
            this.defTab.style.display = "block";
        }
    }, {
        key: 'hideTabs',
        value: function hideTabs() {
            var _iteratorNormalCompletion4 = true;
            var _didIteratorError4 = false;
            var _iteratorError4 = undefined;

            try {
                for (var _iterator4 = this.tabs[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                    var tab = _step4.value;

                    tab.style.display = "none";
                }
            } catch (err) {
                _didIteratorError4 = true;
                _iteratorError4 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion4 && _iterator4.return) {
                        _iterator4.return();
                    }
                } finally {
                    if (_didIteratorError4) {
                        throw _iteratorError4;
                    }
                }
            }
        }
    }, {
        key: 'removeActionClass',
        value: function removeActionClass() {
            var _iteratorNormalCompletion5 = true;
            var _didIteratorError5 = false;
            var _iteratorError5 = undefined;

            try {
                for (var _iterator5 = this.tabBtns[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                    var btn = _step5.value;

                    btn.classList.remove('active');
                }
            } catch (err) {
                _didIteratorError5 = true;
                _iteratorError5 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion5 && _iterator5.return) {
                        _iterator5.return();
                    }
                } finally {
                    if (_didIteratorError5) {
                        throw _iteratorError5;
                    }
                }
            }
        }
    }, {
        key: 'setArticleJSON',
        value: function setArticleJSON(articleJSON) {
            this.articleJSON = articleJSON;
        }
    }, {
        key: 'reset',
        value: function reset() {
            this.showSearch({ target: this.searchBtn });
            document.querySelector('#ext-title').value = '';
            document.querySelector("#ext-url").value = '';
            document.querySelector("#definition").value = '';
        }
    }]);

    return EditorPopover;
}();

window.onload = function () {
    var edit = new Editor('#editor');
    edit.init();
    var nav = new PostHeadingNavigator(edit);
    nav.getElements();
    nav.extendTextarea();
    nav.registerEvents();
    nav.updateElement();
    $.get('/api/articles', function (data) {
        edit.setArticleJSON(data);
    });
};
