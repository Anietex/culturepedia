'use strict';

$('#dropdown-toggle').click(function () {
    $('#dropdown').slideToggle(200);
});

$('.excerpt a').each(function (i, e) {
    if (!$(e).hasClass('btn')) {
        $(e).removeAttr('href');
    }
});

$(window).scroll(fixedSidebar);

function fixedSidebar() {
    var sidebar = $("#sidebar");
    var article = $("#article");

    if (article.get(0)) {
        var articleTop = article.get(0).getBoundingClientRect().top;
        if (articleTop < 1) {
            sidebar.addClass('fixed');
            article.addClass('shift-left');
        } else {
            sidebar.removeClass('fixed');
            article.removeClass('shift-left');
        }
    }
}

$.get('/api/articles', function (data) {

    var articles = function articles() {
        var articles = data;

        return function findMatches(q, cb) {
            var matches = [];
            var substrRegex = new RegExp(q, 'i');

            $.each(articles, function (i, article) {
                if (substrRegex.test(article.title)) {
                    matches.push(article);
                }
            });

            cb(matches);
        };
    };

    $('#article-search .search').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        name: 'articles',
        source: articles(),
        display: function display(article) {
            return article.title;
        },
        templates: {
            suggestion: function suggestion(data) {
                return '<div class="s-item"><a href=\'' + data.link + '\'>' + data.title + '</a></div>';
            }
        }
    });
});
$('#article-search .search').keydown(function (event) {
    if (event.keyCode === 13) {
        $('#search-form').submit();
    }
});

$('.nav-link').each(function (index, elm) {
    if (document.location.href == elm.href) {
        elm.parentElement.classList.add('active');
    }
});
