@extends('admin.master')
@section('title','All Messages')
@section('page-title','All Messages')
@section('styles')
    @parent
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/datatables/datatables.css') }}">
@endsection

@section('header-title')
    <h1><span class="fa  fa-comments"></span> All Messages <small>Manage all messages</small></h1>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
    <li class="breadcrumb-item active" aria-current="page">Messages</li>
@endsection



@section('content')
    <div class="card">
        <div class="card-header">
            All Messages
        </div>
        <div class="card-body">
            <div class="admin-articles">
                <div class="articles">
                    @if(session('delete_success'))
                        <div class="alert alert-success">
                            <p>{{ session('delete_success') }}</p>
                        </div>
                    @endif
                    <table class="table">
                        <thead>
                        <tr>
                            <th width="1%">S/N</th>
                            <th>From</th>
                            <th>Message</th>
                            <th width="1%">Read</th>
                            <th width="1%">Status</th>
                            <th width="1%">Delete</th>
                        </tr>
                        </thead>

                        <tbody>
                            @foreach($messages as $message)
                                <tr>
                                    <td>{{ $loop->index+1 }}</td>
                                    <td>{{ $message->full_name }}</td>
                                    <td>{{ substr($message->message,0,40).'...' }}</td>
                                    <td><button
                                                type="button"
                                                class="btn btn-outline-info btn-sm"
                                                data-toggle="modal"
                                                data-id="{{ $message->id }}"
                                                data-target="#message-modal"
                                                title="Read message">Read more</button></td>
                                    <td>{{ $message->read==true?"Read":"Unread" }}</td>
                                    <td>
                                        <form method="post" action=" {{ url('admin/message',['id'=>$message->id]) }}">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="message-modal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="">Message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" id="msg-form" >
                    <div class="modal-body">
                        <div class="">
                            <p><strong>From: </strong> <span id="full_name"></span></p>
                            <p><strong>Email: </strong><span id="user-email"></span></p>
                            <p><strong>Sent On: </strong><span id="sent-on"></span></p>

                            <p><strong>Message:</strong></p>
                            <p><span id="message"></span> </p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script type="text/javascript" src="{{  asset('assets/datatables/datatables.js') }}"></script>
    <script>
        $(document).ready( function () {
            $('.table').DataTable({

            });
        } );

        var modal =  $('#message-modal')
      modal.on('hidden.bs.modal', function (e) {
           location.reload();
        });

        modal.on('show.bs.modal',function (e) {
            var url = $('meta[name=app-url]').attr('content');
            $.get(url+'/admin/message/'+$(e.relatedTarget).data('id'),function (message) {
              $('#message').text(message.message);
              $('#full_name').text(message.full_name);
                $('#sent-on').text(message.created_at);
                $('#user-email').text(message.email);
                $('#msg-form').attr('action',url+'/admin/message/'+message.id);
            })
        })
    </script>
@endsection