<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $primaryKey = "video_id";
    const UPDATED_AT = null;
    const CREATED_AT = null;
}
