<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @section('styles')
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/admin.css') }}">
    @show


    <title>Culturepedia | Admin  |  @yield('title')</title>
</head>
<body>
<header>
    <nav class="container-fluid">
        <div class="navbar">
            <div class="logo">
                <h1>Culture<span class="colored">pedia</span></h1>
            </div>
        </div>
    </nav>
</header>
<aside>
    <div class="sidebar">
        <div class="links">
            <ul class="list-unstyled">
                <li ><a class="active" href="{{ url('/admin/dashboard') }}"><span class="fa fa-dashboard"></span> Dashboard</a> </li>
                <li><a href="{{ url('/admin/article/create') }}"> <span class="fa fa-pencil"></span> New Article</a> </li>
                <li><a href="{{ url('/admin/articles') }}"><span class="fa fa-compass"></span> My Articles</a></li>
                <li><a href="{{ url('/admin/articles/all') }}"><span class="fa fa-indent"></span> All Articles</a></li>
                <li><a href="{{ url('/admin/articles/unapproved') }}"><span class="fa fa-toggle-off"></span> Unapproved Articles</a> </li>
                <li><a href="{{ url('/admin/articles/approved') }}"><span class="fa fa-toggle-on"></span> Approved Articles</a></li>
                <li><a href="{{ url('/admin/users/all') }}"><span class="fa fa-users"></span> Users</a> </li>
            </ul>
        </div>
    </div>
</aside>
<main class="main">

    <section class="content">
        <div class="page-header">
            <h5>@yield('page-title')</h5>
        </div>
        @yield('content')
    </section>
</main>

@section('scripts')
    <script type="text/javascript" src="{{  asset('assets/js/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
@show
{{--<script type="text/javascript" src=" {{ asset('assets/js/custom.js') }}"></script>--}}
</body>
</html>