<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::domain('mag.culturepedia.io')->group(function (){





    Route::any('/post/{magazinePost}',function (\App\MagazinePost $magazinePost){
        return view('magazine')->with(["post"=>$magazinePost,"media"=>asset("storage/magazine/media/".$magazinePost->media[0]->file_name)]);

    });

    Route::any('{query}',function (){
        return view('magazine');
    })->where('query', '.*');;


});




Route::get('/',"GuestController@index");
Route::get('/articles','GuestController@articles');
Route::get('/search','GuestController@search');
Route::get('/article/featured','GuestController@featuredArticle');
Route::get('/article/random','GuestController@randomArticle');
Route::get('/user', 'UserController@index')->name('home');
Route::get('/terms_of_use','GuestController@termsOfUse');
Route::get('/privacy_policy','GuestController@privacyPolicy');
Route::get('/contact-us','GuestController@contact');
Route::post('/contact','GuestController@sendMessage');

Route::group(['prefix'=>'user'],function (){
    Route::get('/', 'UserController@index')->name('home');
    Route::get('/article/create', 'UserController@createArticle');
    Route::get('/articles/unapproved', 'UserController@unapprovedArticles');
    Route::get('/articles/approved', 'UserController@approvedArticles');
    Route::get('/articles', 'UserController@articles');
    Route::post('/article','ArticleController@store');
    Route::delete('/article/{id}','ArticleController@destroy');
    Route::get('/article/{article}/edit','UserController@editArticle');
    Route::put('/article/{id}','ArticleController@update');
    Route::put('account/update','UserController@updateAccount')->name('user.update');
    Route::put('account/password','UserController@updatePassword')->name('user.password');
});



Route::middleware(['auth'])->group(function (){
    Route::get('/article/create','ArticleController@create');
    Route::post('/article','ArticleController@store');
    Route::get('/my_articles','ArticleController@index');
    Route::delete('/article/{id}','ArticleController@destroy');
    Route::get('article/{article}/edit','ArticleController@edit');
    Route::put('article/{id}','ArticleController@update');
});


Route::group(['prefix'=>'admin','namespace'=>'Admin'],function (){
    Route::get('/','Auth\LoginController@showLoginForm');
    Route::get('/login','Auth\LoginController@showLoginForm');
    Route::post('login', 'Auth\LoginController@login')->name('admin.login');
    Route::post('logout', 'Auth\LoginController@logout')->name('admin.logout');
    Route::get('register', 'Auth\RegisterController@showRegistrationForm');
    Route::post('register', 'Auth\RegisterController@register')->name('admin.register');
});

Route::group(['prefix'=>'admin','namespace'=>'Admin','middleware'=>'auth:admin'],function (){
    Route::get('dashboard','AdminController@dashboard');
    Route::get('article/create','AdminController@createArticle');
    Route::get('articles/all','AdminController@allArticles');
    Route::get('articles/unapproved','AdminController@unapprovedArticles');
    Route::get('articles/approved','AdminController@approvedArticles');
    Route::get('articles','AdminController@articles');
    Route::get('users/all','AdminController@users');
    Route::get('article/unapproved','AdminController@unapprovedArticles');
    Route::post('article/store','ArticleController@store')->name('admin.article.store');
    Route::delete('article/{id}','ArticleController@destroy');
    Route::get('article/{article}/edit','AdminController@editArticle');
    Route::put('article/{id}','ArticleController@update');
    Route::put('article/approve/{id}','ArticleController@approve');
    Route::put('article/disapprove/{id}','ArticleController@disapprove');
    Route::put('account/update','AdminController@updateAccount')->name('admin.update');
    Route::put('account/password','AdminController@updatePassword')->name('admin.password');
    Route::get('messages','AdminController@messages');
    Route::get('message/{message}','AdminController@getMessage');
    Route::delete('message/{id}','AdminController@deleteMessage');
});

Route::get('article/{article}/{slug}',"GuestController@article");