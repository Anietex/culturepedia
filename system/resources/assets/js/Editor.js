import { Commands } from "./components/Commands";
import {CitationCreator} from "./CitationCreator";


export class Editor {

    constructor(container,linkModal,linkPopover,refModal,refPopover){
        this.container = container;
        this.linkModal = linkModal;
        this.linkPopover = linkPopover;
        this.refModal = refModal;
        this.refPopover = refPopover;


    }


    init(){
        this.createEditor();
        this.getElements();
        this.editor.contentEditable = true;
        this.registerEvents();
        this.addLinkEventListener();
        this.registerCiteLinkListener();

        let ob = new MutationObserver(()=>{
            this.registerCiteLinkListener();
            this.addLinkEventListener();
           // ob.disconnect();
        });

        ob.observe(this.editor,{ childList: true,subtree: true})

    }



    getElements(){
        this.undoBtn = document.getElementById('undo');
        this.redoBtn = document.getElementById('redo');
        this.boldBtn = document.getElementById("bold");
        this.italicBtn = document.getElementById("italic");
        this.linkBtn = document.getElementById('link');
        this.listUlBtn = document.getElementById('unlist');
        this.listOlBtn = document.getElementById('orlist');
        this.citeBtn = document.getElementById('cite');
        this.outdentBtn = document.getElementById('red-indent');
        this.balloon = document.getElementById('balloon');
        this.editor = document.getElementById("editor-text");
        // this.mX=0;
        // this.mY =0;
        // this.popOver = new EditorPopover();
    }



    registerEvents(){
        this.boldBtn.addEventListener('click',Commands.bold,false);
        this.italicBtn.addEventListener('click',Commands.italic,false);
        this.listOlBtn.addEventListener('click',Commands.orderedList);
        this.listUlBtn.addEventListener('click',Commands.unorderedList,false);
        this.undoBtn.addEventListener('click',Commands.undo,false);
        this.redoBtn.addEventListener('click',Commands.redo,false);
        this.citeBtn.addEventListener('click',()=>{this.showReferenceModal()});
        this.editor.addEventListener('mousedown',(event)=>
        {
            this.linkPopover.hidePopover();
            this.refPopover.hidePopover()
        },true);

        this.editor.addEventListener('keydown',(e)=>{this.editorKeyDown(e)});
        this.editor.addEventListener('keyup',()=>{this.hidePopovers()});
        this.linkBtn.addEventListener('click',(event)=>{this.showLinkModal()});
        this.addLinkEventListener();
    }

    hidePopovers(){
        this.linkPopover.hidePopover();
        this.refPopover.hidePopover();
    }




    createEditor(){
        let container = document.querySelector(this.container);
        function createActionBar() {
            return `
                <div class="action-bar">
            <button type="button" id="undo" title="Undo"><span class="fa fa-reply"></span></button>
            <button type="button" id="redo" title="Redo"><span class="fa fa-share"></span></button>
            <button type="button" id="bold" title="Bold Ctrl+B"> <span class="fa fa-bold" ></span></button>
            <button type="button"  id="italic" title="Italic"><span class="fa fa-italic" ></span></button>
            <button type="button" id="link" title="Link"><span class="fa fa-link" ></span></button>
            <button type="button" id="unlist"><span class="fa fa-list-ul" ></span></button>
            <button type="button"  id="orlist"><span class="fa fa-list-ol"></span></button>
            <button type="button" id="cite" title="Citation"><span class="fa  fa-quote-left" ></span> <small>Cite </small></button>
            <!--<button type="button" id="red-indent"><span class="fa fa-outdent" ></span></button>-->
        </div>`;
        }

        function createEditorArea() {
            return `<div class="text-wrapper"><div class="text-area" id="editor-text"><span></span></div></div>`
        }

        let editorWrapper = document.createElement('div');
        let editor = document.createElement('div');
        editorWrapper.classList.add('editor-wrapper');
        editor.classList.add('editor');
        editor.innerHTML=createActionBar()+createEditorArea();

        editorWrapper.appendChild(editor);
        container.appendChild(editorWrapper);
    }



    onLinkAdded(reply,selected){
        // selectText();
        let title = reply.external_url_title;
        let url = reply.external_url_link;
        let definition = reply.definition;
        title = title===undefined||title===null || title === ''?'':title;
        url   = url===undefined||url===null || url === ''?'#':url;
        definition = definition===undefined||definition===null || definition === ''?'':definition;
        Commands.createLink(url,selected,title,definition);
        this.addLinkEventListener();
        this.linkAddedListener();
    }




    showLinkModal(){
        let sel;
        if(window.getSelection())
            sel = window.getSelection();
        else{
            sel = document.selection
        }
        let textNode;
        let start;
        let end;
        let range = '';



        let selectText = ()=>{
            let range = document.createRange();
            sel.removeAllRanges();

            range.selectNode(this.editor);
            sel.addRange(range);
            range.setStart(textNode,start);
            range.setEnd(textNode,end);
        };



        if(sel.anchorNode!==null){
            let selected = sel.toString();
            if(selected ===''){
                let pos = sel.focusOffset;
                let text = sel.focusNode.textContent;
                textNode  = sel.focusNode;
                if(pos!==text.length) {
                    if (!/\s/.test(text.charAt(pos))) {
                        let index = text.indexOf(' ', pos);
                        pos = index === -1 ? text.length : index;
                    }
                }

                let preText = text.substr(0, pos);
                range = sel.getRangeAt(0);

                let words = preText.split(' ');
                let lastWord = words[words.length - 1];
                end = pos;
                start = pos-lastWord.length;
                range.setStart(textNode,start);
                range.setEnd(textNode,end);
                selected = sel.toString();

                if(selected){
                    this.linkModal.showModal((reply)=>{
                        selectText();
                        this.onLinkAdded(reply,selected);
                    },selected);
                }

            }else{
                textNode  = sel.focusNode;
                start = sel.anchorOffset;
                end = sel.toString().length+start;
            }


        }



    }


    addLinkEventListener(){
        this.editorLinks = this.editor.querySelectorAll('a');
        let defTexts = this.editor.querySelectorAll('.def-text');

        for (const def of defTexts){
            def.addEventListener('mousedown',(event)=>{

                let fakeEvent = {
                    target:{
                        href:'',
                        innerText:def.innerText,
                        title:'',
                        dataset:def.dataset,
                    },
                    pageX: event.pageX,
                    pageY: event.pageY
                };
                this.showLinkInfoPopover(fakeEvent)});
        }

        for(const link of  this.editorLinks){
            if(link instanceof HTMLAnchorElement && !link.classList.contains('cite-link')){

                link.addEventListener('mousedown',event=>{this.showLinkInfoPopover(event)})
            }
        }
    }


    registerCiteLinkListener(){
        let citeLinks = this.editor.querySelectorAll('.cite-link');

        for(const link of citeLinks){
           link.addEventListener('mousedown',e=>{this.onCiteLinkSelected(e)});
        }

    }


    showReferenceModal(event){

        let sel;
        if(window.getSelection())
            sel = window.getSelection();
        else{
            sel = document.selection
        }

        if(sel.anchorNode){
            let node = sel.focusNode;
            let range = sel.getRangeAt(0);
            let start = sel.anchorOffset;

            this.refModal.showModal((citation,index)=>{
                this.selectText(sel,node,start,start);
                Commands.addRefLink(citation,index);

                this.registerCiteLinkListener();

                this.linkAddedListener();



            })

        }





    }



    selectText(sel,textNode,start,end){
        let range = document.createRange();
        if(textNode.length>=start){
            sel.removeAllRanges();
            sel.addRange(range);
            range.selectNode(this.editor);
            range.setStart(textNode,start);
            range.setEnd(textNode,end);

            setTimeout(function() {
                this.editor.focus();
            }, 0);

        }




    }

    onCiteLinkSelected(event){


        this.refPopover.showPopover(event);
    }



    editorKeyDown(event){
        let sel;
        if(window.getSelection())
            sel = window.getSelection();
        else{
            sel = document.selection
        }
        let node = sel.focusNode;



        this.node = node;
        if(sel.rangeCount>0){
            this.sel = sel;
            this.range = sel.getRangeAt(0);
            this.start = sel.anchorOffset;
        }


        if(node && /^\[\d*\]/gi.test(node.data)){
                event.preventDefault();
           if(event.key==="Backspace"){

               let ob = new MutationObserver(()=>{
                   this.linkAddedListener();
                   ob.disconnect();
               });
               ob.observe(this.editor,{ childList: true,subtree: true});
               event.target.removeChild(node.parentElement.parentElement);

           }
        }else {

            if(event.key==="Backspace" && sel.anchorOffset !==sel.focusOffset){
                let ob = new MutationObserver(()=>{
                    this.linkAddedListener();
                    ob.disconnect();
                });
                ob.observe(this.editor,{ childList: true,subtree: true});

            }
        }


    }


    renameRefIDs(){
        this.editor.querySelectorAll('.cite-link').forEach(elm=>{
             elm.id = elm.id.split('-').slice(1,3).join('-');
        });

        this.registerCiteLinkListener();
        if(this.range)
             this.selectText(this.sel,this.node,this.start,this.start);

    }



    onCiteLinkDeleted() {
        let citeLinks = this.editor.querySelectorAll('.cite-link');
        CitationCreator.onCitationUpdated(this.editor);
    }

    showLinkInfoPopover(event){
        this.linkPopover.showPopover(event)
    }


    linkAddedListener(){
        // a callback function for editor users
    }

    getPostPanels(){

    };


    contentUpdated(){

    }





}