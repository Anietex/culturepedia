import {CitationParser} from "./CitationParser";

export class CitationCreator {

    static getCitations(){
        let citeElms = document.querySelectorAll('#references li');

        let citations = [];

        for(const elm of citeElms){
            let cite = elm.querySelector('cite');
            if(cite.classList.contains('web')){
                citations.push(CitationParser.parseWeb(elm))
            }
            else if(cite.classList.contains('book')){
                citations.push(CitationParser.parseBook(elm));

            }else if(cite.classList.contains('journal')){
                citations.push(CitationParser.parseJournal(elm));
            }else if(cite.classList.contains('news')){
                citations.push(CitationParser.parseNews(elm));
            }

        }

        return citations;

    }

    static createBook(citation,index,useList=true){
        let author = '',
            publisherLocation = '',
            publisher ='',
            publicationYear = '',
            pages=''
        ;


        if(citation.firstName!=='' || citation.lastName !==''){
            author = `
                <span class="author-name">
                         ${citation.lastName+', '+citation.firstName},
                </span>
            `
        }


        if(citation.publisherLocation!==''){
             publisherLocation = `
                <span class="publisher-location">
                         ${ citation.publisherLocation } :
                </span>
             `
        }


        if(citation.publisher){
            publisher = `
                 <span class="publisher">
                         ${ citation.publisher },
                 </span>
            `
        }


        if(citation.publicationYear){
            publicationYear = `
                <span class="publication-year">
                         ${citation.publicationYear}.
                </span>
            `;
        }


        if(citation.pages){
            pages = ` <span class="pages">
                          pp. <span class="page-nums">${citation.pages}.</span>
                      </span>`
        }

        let citeWrapper;
        if(useList){
             citeWrapper = document.createElement('li');
        }else {
             citeWrapper = document.createElement('div');
        }



        citeWrapper.id = 'cite-note-'+index;

        citeWrapper.innerHTML = `
             <span class="back-link"><a href="${'#ref-'+index}"><b>^</b></a></span>
             <span class="ref-text">
                <cite class="citation  book">
        `;

        citeWrapper.innerHTML+= author;

        citeWrapper.innerHTML += `
                      <span class="title">
                         <i>${ citation.title },</i>
                      </span>`;


        citeWrapper.innerHTML += publisherLocation + publisher+ publicationYear + pages;

        citeWrapper.innerHTML+= `</cite></span>`;


        return citeWrapper;

    }


    static createJournal(citation,index,useList=true){
        let url = '',
            title='',
            author = '',
            journal ='',
            sourceDate = '',
            volume ='',
            pages = '';



        if(citation.url){
            title  = `
                <span class="title">
                 <a href="${citation.url}" target="_blank"> "${ citation.title},"</a>
                </span>
            `;
        }else {
            title =    `
                <span class="title">
                 "${citation.title},"
               </span>
            `
        }




        if(citation.firstName!=='' || citation.lastName !==''){
            author = `
                <span class="author-name">
                         ${citation.lastName+', '+citation.firstName},
                </span>
            `
        }


        if(citation.pages){
            pages = ` <span class="pages">
                          pp. <span class="page-nums">${citation.pages},</span>
                      </span>`
        }


        if(citation.sourceDate){
            sourceDate = `
            <span class="source-date">
                  ${citation.sourceDate}
            </span>
            
            `
        }


        if(citation.journal){
            journal = `
                <span class="journal-title">
                      <i>${citation.journal}, </i>
                </span>
            `;
        }



        if(citation.volume){
            volume = `
                <span class="volume">
                     ${citation.volume},
                </span>
            `
        }


        let citeWrapper;
        if(useList){
            citeWrapper = document.createElement('li');
        }else {
            citeWrapper = document.createElement('div');
        }
        citeWrapper.id = 'cite-note-'+index;

        citeWrapper.innerHTML = `
             <span class="back-link"><a href="${'#ref-'+index}"><b>^</b></a></span>
             <span class="ref-text">
                <cite class="citation  journal">
        ` + author+title+journal+volume+pages+sourceDate;


        citeWrapper.innerHTML += `
              </cite></span>
        `;


        return citeWrapper;
    }


    static createWeb(citation,index,useList=true){
        let author =  '',
        dateRetrieved = '';


        if(citation.firstName!=='' || citation.lastName !==''){
            author = `
                <span class="author-name">
                         ${citation.lastName+', '+citation.firstName}.
                </span>
            `
        }


        if(citation.dateRetrieved){
            dateRetrieved  = `
            <span class="date-retrieved">
              Retrieved ${citation.dateRetrieved}.
            </span>
            
            
            `;
        }


        let citeWrapper;
        if(useList){
            citeWrapper = document.createElement('li');
        }else {
            citeWrapper = document.createElement('div');
        }
        citeWrapper.id = 'cite-note-'+index;

        citeWrapper.innerHTML = `
             <span class="back-link"><a href="${'#ref-'+index}"><b>^</b></a></span>
             <span class="ref-text">
                <cite class="citation  web">
        `+author
        +` <span class="title">
              <i><a href="${citation.url}"  target="_blank">${citation.title}.</a></i>
           </span>`
        +dateRetrieved
        +`</cite></span>`;


        return citeWrapper;

    }






















    static createNews(citation,index,useList=true){




            let title = '',
                publisher = '',
                author='',
                dateRetrieved='';

        if(citation.url!==''){

            title  = `
                <span class="title">
                  <i><a href="${citation.url}" target="_blank">${ citation.title}</a></i>
                </span>
            `;
        }else {
            title = `
                <span class="title">
                  <i>${citation.title}</i>
               </span>
            `;
        }



        if(citation.publicationName){
            publisher = `
                 <span class="publisher">
                         ${ citation.publicationName }
                 </span>
            `
        }



        if(citation.firstName!=='' || citation.lastName !==''){
            author = `
                <span class="author-name">
                         ${citation.lastName+', '+citation.firstName}
                </span>
            `
        }



        if(citation.accessDate){
            dateRetrieved  = `
            <span class="date-retrieved">
             Retrieved  ${citation.accessDate}
            </span>
            
            
            `;
        }



        let citeWrapper;
        if(useList){
            citeWrapper = document.createElement('li');
        }else {
            citeWrapper = document.createElement('div');
        }
        citeWrapper.id = 'cite-note-'+index;

        citeWrapper.innerHTML = `
             <span class="back-link"><a href="${'#ref-'+index}"><b>^</b></a></span>
             <span class="ref-text">
                <cite class="citation  news">
        `+author+publisher
            +title
            +dateRetrieved
            +`</cite></span>`;





    return citeWrapper;

    }













    static onCitationUpdated(editor){



        let retainCitations = [];
        let citeElms = Array.from(document.querySelectorAll('#references li'));




        let ref = document.getElementById('references');



        let citations   = citeElms.map((elm)=>{
            // console.log(elm)
            let citeData;
            let cite = elm.querySelector('cite');
            if(cite.classList.contains('web')){
               citeData = CitationParser.parseWeb(elm)
            }
            else if(cite.classList.contains('book')){
                citeData = CitationParser.parseBook(elm)

            }else if(cite.classList.contains('journal')){
                citeData = CitationParser.parseJournal(elm)
            }else if(cite.classList.contains('news')){
                citeData = CitationParser.parseNews(elm)
            }
           return {
               index:elm.id.split('-')[2],
               citeData
           }
        });

        // console.log(citeElms);


        let citeLink = Array.from(editor.querySelectorAll('.cite-link'));

        // console.log(citeLink);

       let linkObj = citeLink.map((link)=>{

           let index;

            if(!link.id.includes('fake'))
                index = link.id.split('-')[1];
           else
                index = link.id.split('-')[2];
            let type  = link.dataset.reftype;

            if(index)
             return {index,type}
        });



        for(let i=0; i<linkObj.length; i++){
            for(let k=0; k<citations.length; k++){

                if(citations[k].index === linkObj[i].index){
                    retainCitations.push(citations[k]);
                    break;
                }
            }
        }


        citeElms.map(el=>{
            ref.removeChild(el);
        });

        for(let l=0; l<citeLink.length; l++){

            for(let i=0; i<retainCitations.length; i++){

                // console.log(citeLink[l].id , 'fake-ref-'+retainCitations[i].index)
                if(citeLink[l].id === 'fake-ref-'+retainCitations[i].index ||
                    citeLink[l].id === 'ref-'+retainCitations[i].index

                ){

                    switch (retainCitations[i].citeData.type) {
                        case "book":
                            ref.appendChild(CitationCreator.createBook(retainCitations[i].citeData,l+1));
                            break;
                        case "journal":
                            ref.appendChild(CitationCreator.createJournal(retainCitations[i].citeData,l+1));
                            break;
                        case "web":
                            ref.appendChild(CitationCreator.createWeb(retainCitations[i].citeData,l+1));
                            break;
                        case "news":
                            ref.appendChild(CitationCreator.createNews(retainCitations[i].citeData,l+1));
                            break;

                    }


                    citeLink[l].innerText = `[${l+1}]`;
                    citeLink[l].id = 'fake-ref-'+(l+1);
                    citeLink[l].href = '#cite-note-'+(l+1);

                    break;
                }
            }

        }


    }
}