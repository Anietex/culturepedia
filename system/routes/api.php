<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::domain('mag.culturepedia.io')->group(function (){
    Route::get('/',function (){
        return "Hello Mag";
    });


    Route::get('/articles','GuestController@postLinks');
    Route::get('/posts','Magazine\MagazineController@index');
    Route::get('post/{post}','Magazine\MagazineController@show');
    Route::get('posts/{username}','Magazine\UserController@posts');

    Route::post('/login',"Magazine\Auth\LoginController@login");

    Route::post('/register',"Magazine\Auth\LoginController@register");

    Route::middleware('auth:api')->group(function (){
        Route::post('/post','Magazine\MagazineController@store');
        Route::post('/post/{id}/comment','Magazine\MagazineController@comment');
        Route::post('/post/{id}/like','Magazine\MagazineController@like');
        Route::post('/post/{id}/unlike','Magazine\MagazineController@unlike');
        Route::post('/logout',"Magazine\Auth\LoginController@logout");
        Route::get('/account/info','Magazine\UserController@getAccountInfo');
        Route::post('/account/update/socials','Magazine\UserController@updateSocials');
        Route::post('/account/update/profile_picture','Magazine\UserController@uploadProfilePhoto');
        Route::post('/account/update/change_password','Magazine\UserController@updatePassword');

    });
});




Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




Route::get('/articles','GuestController@postLinks');
Route::get('/url-info','CitationDataCreateController@urlData');
Route::get('/isbn-info','CitationDataCreateController@isbnData');

Route::get('/posts','Magazine\MagazineController@index');
Route::get('post/{post}','Magazine\MagazineController@show');
Route::get('posts/{username}','Magazine\UserController@posts');

Route::post('/login',"Magazine\Auth\LoginController@login");

Route::post('/register',"Magazine\Auth\LoginController@register");


Route::middleware('auth:api')->group(function (){
    Route::post('/post','Magazine\MagazineController@store');
    Route::post('/post/{id}/comment','Magazine\MagazineController@comment');
    Route::post('/post/{id}/like','Magazine\MagazineController@like');
    Route::post('/post/{id}/unlike','Magazine\MagazineController@unlike');
    Route::post('/logout',"Magazine\Auth\LoginController@logout");
    Route::get('/account/info','Magazine\UserController@getAccountInfo');
    Route::post('/account/update/socials','Magazine\UserController@updateSocials');
    Route::post('/account/update/profile_picture','Magazine\UserController@uploadProfilePhoto');
    Route::post('/account/update/change_password','Magazine\UserController@updatePassword');
    Route::delete('/post/{id}','Magazine\MagazineController@destroy');
});