export  class ViewLinkPopover {


    constructor(){
        this.popover = null
    }

    showPopover(event){
        this.hidePopover();
        this.popover = this.createElement();
        let t = event.target;


        document.body.appendChild(this.popover);
        this.popover.style.left =event.pageX-133+"px";
        this.popover.style.top = event.pageY+40+"px";




        let defText = document.getElementById('def-text');
        let vlink = document.getElementById('viewed-link');


        defText.innerText = t.dataset.def;
        if(!t.dataset.def)
            defText.innerText = t.title;
        vlink.innerText=t.innerText;
        vlink.title =t.title;
        vlink.href = t.href;
        document.querySelector("#link-view").style.display ="block";

    }


    hidePopover(){


        if(this.popover){
            if(document.body.contains(this.popover)){
                document.body.removeChild(this.popover);
            }
        }
    }


    createElement(){
        let wrapper = this.getWrapper();
        let inner = this.innerWrapper();
        inner.appendChild(this.getLinkView());
        wrapper.appendChild(inner);
        return wrapper;
    }



    innerWrapper(){
        let innerWrapper = document.createElement('div');
        innerWrapper.classList.add('baloon');
        return innerWrapper;
    }


    getWrapper(){
        let wrapper = document.createElement('div');
        wrapper.id = "balloon";
        wrapper.classList.add('balloon-wrapper');
        return wrapper;

    }


    getLinkView(){
        let lv = document.createElement('div');
        lv.classList.add('link-view');
        lv.id = "link-view";

        let header = ()=>{
            let wrapper = document.createElement('div');
            wrapper.classList.add("header");
            let title = document.createElement('div');
            title.classList.add('title');
            let p= document.createElement('p');
            p.innerHTML=`<span class="fa fa-link"></span> Link`;
            title.appendChild(p);
            wrapper.appendChild(title);
            // let buttons = document.createElement('div');
            // buttons.classList.add('buttons');
            // buttons.innerHTML = `<button class="" id="close-btn"><span class="fa fa-ban"></span></button><button id="edit-btn">Edit</button>`;
            // wrapper.appendChild(buttons);
            return wrapper;
        }
        let links = ()=>{
            let links = document.createElement('div');
            links.classList.add('links');
            links.innerHTML=`<div class="link-item">
                   <h4><a id="viewed-link" href="#" target="_blank"></a></h4>
                   <p id="def-text"></p>
               </div>`;
            return links;
        }
        lv.appendChild(header());
        lv.appendChild(links());

        return lv;

    }

}