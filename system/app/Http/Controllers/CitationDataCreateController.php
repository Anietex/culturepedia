<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CitationDataCreateController extends Controller
{


    public function urlData(Request $request){

        if($request->input('url')){
            $handle = curl_init();

            $url = $request->input('url');

            curl_setopt($handle, CURLOPT_URL, $url);

            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);

            $output = curl_exec($handle);

            curl_close($handle);

            if(preg_match('/<title>(.+)<\/title>/im',$output,$matches)){

                return response()->json(['url'=>$url,'title'=>$matches[1],'retrieved_on'=>date('F j, Y')]);
            }else{
                return response()->json(['error'=>'no data was found'],500);
            }


        }else {
            return response()->json([]);
        }

    }



    public function isbnData(Request $request){
        $handle = curl_init();

        if($request->input('isbn')) {
            $isbn = $request->input('isbn');
            $url = 'https://openlibrary.org/api/books?bibkeys=ISBN:'.$isbn.'&format=json&jscmd=data';
            curl_setopt($handle, CURLOPT_URL, $url);

            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);

            $output = curl_exec($handle);

            curl_close($handle);
            if(count(json_decode($output,true))){


                $key = 'ISBN:'.$isbn;
                $book = json_decode($output)->$key;
                $data['title'] = $book->title.(property_exists($book,'subtitle')?' : '.$book->subtitle:'');
                $data['author'] = $book->authors[0]->name;
                $data['publisher'] = $book->publishers[0]->name;
                $data['publication_year'] = $book->publish_date;
                $data['publisher_location'] = property_exists($book,'publish_places')?$book->publish_places[0]->name:'';


                return response()->json($data);

            }else{
                return response()->json(["error"=>"no data"],500);
            }

        }else{

            return '';
        }

    }
}
