@extends('layouts.master')
@section('title',"Contact Us")
@section('styles')
    @parent
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/magnific-popup.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/article.css')}}">
@endsection
@section('section-title')
    <p> Contact Us</p>
@endsection

@section('content')
    <div class="mt-3">
        <div class="container">
            <div class="contact">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="">
                            @if(session('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <p>{{ session('success') }}</p>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                                @if(session('error'))
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <p>{{ session('success') }}</p>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif



                            <form method="post" action="{{ url('contact') }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label>Full name</label>
                                    <input type="text" class="form-control  {{ $errors->has('full_name') ? 'is-invalid' : '' }}" name="full_name" required>
                                    @if ($errors->has('full_name'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('full_name') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control  {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" required>
                                    @if ($errors->has('email'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('email') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea  class="form-control  {{ $errors->has('message') ? 'is-invalid' : '' }}" name="message" rows="6" required></textarea>
                                    @if ($errors->has('email'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('email') }}
                                        </div>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-primary"> Send</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent


@endsection