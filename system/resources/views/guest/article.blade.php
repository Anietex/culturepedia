@extends('layouts.master')
@section('title',$article->title)
@section('styles')
    @parent
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/magnific-popup.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/article.css')}}">
@endsection
@section('section-title')
   <p> <a href='{{ url('articles') }}'>All Articles</a> / {{ $article->title }}</p>
@endsection

@section('content')
    <section class="article-title">
        <div class="container">
            <h3>{{ $article->title }}</h3>
        </div>
    </section>

    <div class="main-content">
        <div class="container">
            <aside id="sidebar">
                <div class="sidebar">
                    <h5 class="px-3">Table of Contents</h5>
                    <ul class="list-unstyled" >
                        <li><a href="#tribe">Clan Tribe</a> </li>
                        <li><a href="#region">Region/ Province/ State</a> </li>
                        <li><a href="#country">Country</a> </li>
                        <li><a href="#language">Dialect Language</a> </li>
                        <li><a href="#geo">Landscape Description</a> </li>
                        <li><a href="#history">History and Mythology</a> </li>
                        <li><a href="#social">Social Organization</a> </li>
                        <li><a href="#politics">Politics and Leadership</a> </li>
                        <li><a href="#literature">Literature</a> </li>
                        <li><a href="#tradition">Music and Arts</a> </li>
                        <li><a href="#religion">Religion and Rituals</a> </li>
                        <li><a href="#festivals">Festivals</a> </li>
                        <li><a href="#occupation">Occupations</a> </li>
                        <li><a href="#tools">Tools and Technologies</a> </li>
                        <li><a href="#clothing"> Clothing and Cosmetics</a> </li>
                        <li><a href="#food">Food and Food Processing</a> </li>
                        <li><a href="#tourist">Tourist Attractions</a> </li>
                        <li><a href="#other-traditions">Other traditions</a> </li>

                        @if(count($pictures)>0 || count($videos)>0)
                            <li><a href="#picture">Pictures and/or Videos</a> </li>
                        @endif
                        <li><a href="#references">References</a> </li>






                    </ul>
                </div>
            </aside>

            <article class="py-4 text-justify" id="article">
                <div id="tribe">
                    <h3>Clan Tribe @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <p>{!! $article->tribe !!} </p>
                </div>
                <div id="region">
                    <h3>Region/ Province/ State  @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <p>{!! $article->region !!} </p>
                </div>
                <div id="country">
                    <h3>Country  @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <p>{!! $article->country !!} </p>
                </div>
                <div id="language">
                    <h3>Dialect Language  @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <p>{!!  $article->language !!} </p>
                </div>
                <div id="geo">
                    <h3>Landscape Description  @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <p>{!! $article->geo_description !!} </p>
                </div>
                <div id="history">
                    <h3>History and Mythology  @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <p>{!! $article->history !!} </p>
                </div>

                <div id="social">
                    <h3>Social Organization  @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <p>{!! $article->social_institution !!} </p>
                </div>

                <div id="politics">
                    <h3>Politics and Leadership  @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <p>{!!  $article->leadership !!}</p>
                </div>

                <div id="literature">
                    <h3>Literature  @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <p>{!! $article->literature !!} </p>
                </div>
                <div id="tradition">
                    <h3>Music and Arts  @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <p>{!! $article->tradition !!}</p>
                </div>


                <div id="religion">
                    <h3>Religion and Ritual  @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <p>{!! $article->religion !!}</p>
                </div>

                <div id="festivals">
                    <h3>Festivals  @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <p>{!! $article->festivals !!}</p>
                </div>
                <div id="occupation">
                    <h3>Occupations  @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <p>{!! $article->occupations !!}</p>
                </div>
                <div id="tools">
                    <h3>Tools and Technologies  @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <p>{!! $article->technology !!} </p>
                </div>
                <div id="clothing">
                    <h3>Clothing and Cosmetics  @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <p>{!!$article->clothing  !!} </p>
                </div>
                <div id="food">
                    <h3>Food and Food Processing  @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <p>{!! $article->food !!}</p>
                </div>
                <div id="tourist">
                    <h3>Tourist Attractions  @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <p>{!! $article->tourist_attraction !!}</p>
                </div>

                <div id="other-traditions">
                    <h3>Other Traditions  @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <p>{!! $article->other_traditions !!}</p>
                </div>
                @if(count($pictures)>0 || count($videos)>0)
                <div id="picture">
                    <h3>Pictures and/or Videos @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                    <div class="medias pt-3">
                        <div class="row">
                            @foreach($pictures as $picture)
                            <div class="col-sm-6">
                               <a href="{{ asset('storage/media/pictures/'.$picture->file_name)}}" class="media">
                                   <img src="{{ asset('storage/media/pictures/'.$picture->file_name)}}" class="img-fluid img-thumbnail">
                               </a>
                                <p>{{ $picture->description }}</p>
                            </div>
                            @endforeach
                                @foreach($videos as $video)
                                    <div class="col-sm-6">
                                        <video src="{{ asset('storage/media/videos/'.$video->file_name)}}" controls class="img-fluid img-thumbnail" ></video>
                                        <p>{{ $video->description }}</p>
                                    </div>
                                @endforeach
                        </div>
                    </div>
                </div>
               @endif

                <div id="references">
                    <h3>References  @if(auth()->check()) <small> <a href="{{ url('/user/article/'.$article->article_id.'/edit') }}">edit</a> </small> @endif</h3>
                    <hr class="m-1">
                   <ol>
                       {!! $article->article_references !!}
                   </ol>
                </div>
            </article>
        </div>
        <div class="socials bg-light" >
            <ul>
                <li class="text-center">Share <br/>this</li>
                <li class="facebook"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{urlencode(url("/article/$article->article_id/".str_slug($article->title,'_')))}}"><span class="fa fa-facebook-official"></span></a> </li>
                <li class="twitter"><a target="_blank" href="https://twitter.com/intent/tweet?text={{$article->title}}&url={{ urlencode(url("/article/$article->article_id/".str_slug($article->title,'_'))) }}&short_url_length=10"><span class="fa fa-twitter-square"></span></a> </li>
                <li class="gplus"><a target="_blank" href="https://plus.google.com/share?url={{urlencode(url("/article/$article->article_id/".str_slug($article->title,'_')))}}"><span class="fa fa-google-plus-square"></span></a> </li>
                <li class="twitter"><a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url={{urlencode(url("/article/$article->article_id/".str_slug($article->title,'_')))}}"><span class="fa fa-linkedin-square"></span></a> </li>

            </ul>
        </div>
        <style>
            .translate{
                display: none;
            }
        </style>
    </div>
@endsection
@section('scripts')
    @parent
    <script type="text/javascript"  src="{{ asset('assets/js/article.bundle.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.smooth-scroll.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
    <script type="text/javascript">
        $('a').smoothScroll({
            easing: 'swing',
            speed: 600,
        });

        $('.media').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-img-mobile',
            image: {
                verticalFit: true
            }

        });

    </script>
@endsection