let path = require('path');
let webpack = require('webpack');



module.exports = {

    entry:{
        'admin-editor':path.resolve('resources/assets/js/AdminArticleEditor.js'),
        'user-editor':path.resolve('resources/assets/js/UserArticleEditor.js'),
        article:  path.resolve('resources/assets/js/ArticleView.js'),

    },

    output: {
        path: path.resolve('../assets/js'),
        filename: '[name].bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                options: {
                    presets: ['env']
                }
            }
        ]
    },
    stats: {
        colors: true
    },
    devtool: 'source-map',
    mode:'production',
    watch:true,
};