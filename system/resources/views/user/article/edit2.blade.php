@extends('layouts.master')
@section('title','Edit Article')
@section('section-title','Edit Article')

@section('styles')
    @parent
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/editor.css') }}">
@endsection
@section('content')
    <div class="container">
        <div class="create-article">
            <div class="inner-wrapper">
                <h3 class="font-weight-light">Edit Article</h3>
                <hr>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
                @if(session('success'))
                    <div class="alert alert-success">
                        <p>{{ session('success') }}</p>
                    </div>
                @endif
                @if(session('error'))
                    <div class="alert alert-danger">
                        <p>{{ session('error') }}</p>
                    </div>
                @endif
                <form action="{{ url('article/'.$article->article_id) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label for="title">Article title</label>
                        <input type="text" name="title" id="title" class="form-control input-lg" placeholder="Article title" value="{{ $article->title }}">
                    </div>
                    <label for="article"><span  id="heading"></span> <small class="text-info"> (<span id="help-text"></span> ) </small></label>
                    <div class="form-group" id="text-pane">
                        <div id="editor"></div>
                        {{--<textarea class="form-control" id="article" rows="10"></textarea>--}}
                    </div>

                    <div class="upload-pictures upload-pane" id="picture-pane">
                        <label>Upload Pictures </label>
                        <small>Maximum file size of 500KB</small>
                        <div class="media-files pictures d-flex flex-wrap" id="picture-files">

                            @foreach($pictures as $picture)
                                <div class="uploaded-media">
                                    <span class="fa fa-close close-btn" ></span>
                                    <img src="{{ asset('storage/media/pictures/'.$picture->file_name)}}" class="img-fluid">
                                    <input type="text" name="old_pic_description[]" class="img-description" placeholder="Media description" value="{{ $picture->description }}">
                                    <input type="hidden" name="old_pictures[]" class="form-control media-input"   value="{{ $picture->file_name }}" >
                                </div>
                            @endforeach
                            <div class="upload-btn">
                                <div class="add-media add-media-btn">
                                    <button type="button" class="fa fa-plus fa-2x" data-input="#media-1" id="picture-btn"></button>
                                </div>
                                <input type="file"  class="form-control media-input"  id="picture-input" accept="image/*">
                            </div>
                        </div>
                    </div>


                    <div class="upload-videos upload-pane" id="video-pane">

                        <label>Upload short Videos</label>
                        <small>Maximum file size of 3.5MB</small>
                        <div class="media-files pictures d-flex flex-wrap" id="video-files">

                           @foreach($videos as $video)
                                <div class="uploaded-media">
                                    <span class="fa fa-close close-btn" ></span>
                                    <video src="{{  asset('storage/media/videos/'.$video->file_name)}}" controls class="img-fluid"></video>
                                    <input type="text" name="old_vid_description[]" class="img-description" placeholder="Video description">
                                    <input type="hidden" name="old_videos[]" class="form-control media-input" value="{{ $video->file_name }}">
                                </div>
                            @endforeach

                            <div class="upload-btn">
                                <div class="add-media add-media-btn">
                                    <button type="button" class="fa fa-plus fa-2x" id="video-btn"></button>
                                </div>
                                <input type="file"  name="" class="form-control media-input"  id="video-input" accept="video/*">
                            </div>
                        </div>
                    </div>

















                    {{--<div id="upload-pane">--}}
                        {{--<div class="form-group">--}}
                            {{--<label>Upload Media files</label>--}}
                            {{--<div class="media-files d-flex flex-wrap" id="media-files">--}}
                                {{--@foreach($medias as $media)--}}
                                    {{--<div class="uploaded-media">--}}
                                    {{--<span class="fa fa-close close-btn" ></span>--}}
                                    {{--<img src="{{ asset('storage/media/'.$media->file_name)}}" class="img-fluid">--}}
                                    {{--<input type="text" name="old_description[]" class="img-description" placeholder="Media description" value="{{ $media->media_description }}">--}}
                                    {{--<input type="hidden" name="old_files[]" class="form-control media-input"   value="{{ $media->file_name }}" >--}}
                                    {{--</div>--}}
                                {{--@endforeach--}}

                                {{--<div class="uploaded-media">--}}
                                {{--<span class="fa fa-close close-btn" ></span>--}}
                                {{--<img src="{{ asset('assets/images/slide1.jpg') }}" class="img-fluid">--}}
                                {{--<input type="text" name="description[]" class="img-description" placeholder="Media description">--}}
                                {{--<input type="file" name="media[]" class="form-control media-input"  id="media-1" accept="video/*,image/*" >--}}
                                {{--</div>--}}


                                {{--<div class="upload-btn" >--}}
                                    {{--<div class="add-media add-media-btn">--}}
                                        {{--<button type="button" class="fa fa-plus fa-2x" data-input="#media-1"></button>--}}
                                    {{--</div>--}}
                                    {{--<input type="file"  name="upload-f" class="form-control media-input"  id="media-1" accept="video/*,image/*">--}}
                                {{--</div>--}}


                            {{--</div>--}}

                        {{--</div>--}}
                    {{--</div>--}}
                    
                    

                    <div class="inputs">
                        <input type="hidden"  name="tribe" value="{{ $article->tribe }}">
                        <input type="hidden" name="region" value="{{ $article->region }}">
                        <input type="hidden" name="country" value="{{ $article->country }}">
                        <input type="hidden" name="language" value="{{ $article->language }}">
                        <input type="hidden" name="geo-description" value="{{ $article->geo_description }}">
                        <input type="hidden" name="history" value="{{ $article->history }}">
                        <input type="hidden" name="literature" value="{{ $article->literature }}">
                        <input type="hidden" name="tradition" value="{{ $article->tradition }}">
                        <input type="hidden" name="leadership" value="{{ $article->leadership }}">
                        <input type="hidden" name="social-institution" value="{{ $article->social_institution }}">
                        <input type="hidden" name="religion" value="{{ $article->religion }}">
                        <input type="hidden" name="occupation" value="{{ $article->occupations }}">
                        <input type="hidden" name="technology" value="{{ $article->technology }}">
                        <input type="hidden" name="clothing" value="{{ $article->clothing }}">
                        <input type="hidden" name="food" value="{{ $article->food }}">
                        <input type="hidden" name="tourist-attraction" value="{{ $article->tourist_attraction}}">
                    </div>

                    <div class="action-buttons d-flex justify-content-between">
                        <button type="button" class="btn btn-primary" id="prev-btn">Previous</button>
                        <button type="button" class="btn btn-primary" id="next-btn">Next</button>
                        <button type="submit" class="btn btn-primary" id="post">Publish</button>
                    </div>



                </form>

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="{{ asset('assets/js/article.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('assets/js/editor.js') }}"></script>
@endsection
