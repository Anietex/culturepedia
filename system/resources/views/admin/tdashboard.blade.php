<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/admin.css') }}">
    <title>Dashboard | </title>
</head>
<body>
<header>
    <div class="navigation">
        <nav class="navbar navbar-expand-lg navbar-default bg-light container navbar-fixed">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <a class="navbar-brand" href="#">Culturepedia</a>
                <ul class="navbar-nav  mr-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">All Articles <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Approved Articles</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Unapproved Articles</a>
                    </li>
                </ul>
                <div class="user">
                    <ul class="list-unstyled list-inline navbar-nav">
                        <li><a  class="nav-link" href="#">Welcome, Admin</a></li>
                        <li><a class="nav-link" href="#">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <div class="big-header py-3">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <h1><span class="fa fa-dashboard" aria-hidden="true"></span> Dashboard <small>Manage Your Site</small></h1>
                </div>
                <div class="col-md-2">
                  <div class="action mt-3">
                      <a href="#" class="btn btn-primary">New Article</a>
                  </div>
                </div>
            </div>
        </div>
    </div>
</header>
<section id="breadcrumb">
    <div class="container">
        <ol class="breadcrumb">
            <li class="active">Dashboard</li>
        </ol>
    </div>
</section>
<section class="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-md-3">
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action active">
                        <span class=" fa fa-dashboard"></span> Dashboard
                    </a>
                    <a href="#" class="list-group-item list-group-item-action"> <span class=" fa fa-list"></span> My Articles</a>
                    <a href="#" class="list-group-item list-group-item-action"><span class=" fa fa-align-justify"></span> All Articles</a>
                    <a href="#" class="list-group-item list-group-item-action"><span class=" fa fa-toggle-of"></span> Unapproved Articles</a>
                    <a href="#" class="list-group-item list-group-item-action disabled"><span class=" fa fa-toggle-on"></span> Approved Articles</a>
                    <a href="#" class="list-group-item list-group-item-action disabled"><span class=" fa fa-users"></span> Users</a>
                </div>
            </div>
            <div class="col-sm-9 col-md-9">
                <div class="card overview">
                    <div class="card-header">
                       Overview
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="item">
                                    <h2><span class="fa fa-pencil"></span> 50,000</h2>
                                    <h4>Total Articles</h4>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="item">
                                    <h2><span class="fa fa-pencil"></span> 50,000</h2>
                                    <h4>Published Articles</h4>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="item">
                                    <h2><span class="fa fa-users"></span> 50,000</h2>
                                    <h4>Total Users</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mt-3">
                    <div class="card-header">
                       Latest Articles
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Title</th>
                                <th scope="col">Date added</th>
                                <th scope="col">Status</th>
                                <th scope="col">Approval</th>
                                <th scope="col">Edit</th>
                                <th scope="col">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>An article about no body culture</td>
                                <td>12/12/2018</td>
                                <td>Unapproved</td>
                                <td><a href="#" class="btn btn-sm btn-success">Approve</a></td>
                                <td><a href="#" class="btn btn-primary btn-sm">Edit</a></td>
                                <td><a href="#" class="btn btn-sm btn-danger">Delete</a></td>
                            </tr>


                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card mt-3">
                    <div class="card-header">
                      Newest Users
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</body>
</html>