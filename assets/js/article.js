"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MediaHandler = function () {
    function MediaHandler() {
        _classCallCheck(this, MediaHandler);

        this.index = 1;
        this.vidIndex = 0;
        this.picIndex = 0;
        this.addRemoveListener();
    }

    _createClass(MediaHandler, [{
        key: "addEventListener",
        value: function addEventListener() {
            var _this2 = this;

            this.pictureBtn.click(function (event) {
                _this2.showPictureDialog(event);
            });

            this.pictureInput.change(function (event) {
                _this2.handlePictures(event);
            });

            this.videoBtn.click(function (event) {
                _this2.showVideoDialog(event);
            });

            this.videoInput.change(function (event) {
                _this2.handleVideos(event);
            });
        }
    }, {
        key: "getElements",
        value: function getElements() {
            this.pictureBtn = $("#picture-btn");
            this.pictureInput = $("#picture-input");
            this.videoBtn = $("#video-btn");
            this.videoInput = $("#video-input");

            this.uploadBtn = $(".upload-btn");
            this.uploadInput = $('#media-1');
        }
    }, {
        key: "showPictureDialog",
        value: function showPictureDialog() {
            this.pictureInput.get(0).click();
        }
    }, {
        key: "showVideoDialog",
        value: function showVideoDialog() {
            this.videoInput.get(0).click();
        }
    }, {
        key: "showFileDialog",
        value: function showFileDialog() {
            this.uploadInput.get(0).click();
        }
    }, {
        key: "addRemoveListener",
        value: function addRemoveListener() {
            $(".close-btn").click(function (e) {
                $(this).parent().remove();
            });
        }
    }, {
        key: "handlePictures",
        value: function handlePictures(event) {
            var _this3 = this;

            var files = event.target.files;
            var _this = this;
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = files[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var file = _step.value;

                    var reader = new FileReader();
                    reader.onload = function (event) {
                        var element = _this3.createImageThumbnail(event.target.result);
                        $("#picture-files").prepend(element);
                        _this3.addRemoveListener();
                        _this3.attachPicToInput(files);
                        // this.attachFileToInput(files)
                    };
                    reader.readAsDataURL(file);
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        }
    }, {
        key: "handleVideos",
        value: function handleVideos(event) {
            var _this4 = this;

            var files = event.target.files;
            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;

            try {
                for (var _iterator2 = files[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                    var file = _step2.value;

                    var reader = new FileReader();
                    reader.onload = function (event) {
                        var element = _this4.createVideoThumbnail(event.target.result);
                        $("#video-files").prepend(element);
                        _this4.addRemoveListener();
                        _this4.attachVideoToInput(files);
                        //this.attachFileToInput(files)
                    };
                    reader.readAsDataURL(file);
                }
            } catch (err) {
                _didIteratorError2 = true;
                _iteratorError2 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion2 && _iterator2.return) {
                        _iterator2.return();
                    }
                } finally {
                    if (_didIteratorError2) {
                        throw _iteratorError2;
                    }
                }
            }
        }
    }, {
        key: "handleFiles",
        value: function handleFiles(event) {
            var _this5 = this;

            var files = event.target.files;
            var _this = this;
            var _iteratorNormalCompletion3 = true;
            var _didIteratorError3 = false;
            var _iteratorError3 = undefined;

            try {
                for (var _iterator3 = files[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                    var file = _step3.value;

                    var reader = new FileReader();
                    reader.onload = function (event) {
                        var element = _this5.createImageThumbnail(event.target.result);
                        $("#media-files").prepend(element);
                        _this5.addRemoveListener();
                        _this5.attachFileToInput(files);
                    };
                    reader.readAsDataURL(file);
                }
            } catch (err) {
                _didIteratorError3 = true;
                _iteratorError3 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion3 && _iterator3.return) {
                        _iterator3.return();
                    }
                } finally {
                    if (_didIteratorError3) {
                        throw _iteratorError3;
                    }
                }
            }
        }
    }, {
        key: "createImageThumbnail",
        value: function createImageThumbnail(src) {
            return "<div class=\"uploaded-media\">\n                   <span class=\"fa fa-close close-btn\" ></span>\n                    <img src=\"" + src + "\" class=\"img-fluid\">\n                     <input type=\"text\" name=\"pictures_description[]\" class=\"img-description\" placeholder=\"Picture description\">\n                    <input type=\"file\" name=\"pictures[]\" class=\"form-control media-input\"  id=\"picture-" + ++this.picIndex + "\" accept=\"video/*,image/*\">\n                </div>";
        }
    }, {
        key: "createVideoThumbnail",
        value: function createVideoThumbnail(src) {
            return "<div class=\"uploaded-media\">\n                   <span class=\"fa fa-close close-btn\" ></span>\n                    <video src=\"" + src + "\" class=\"img-fluid\"></video>\n                     <input type=\"text\" name=\"videos_description[]\" class=\"img-description\" placeholder=\"Video description\">\n                    <input type=\"file\" name=\"videos[]\" class=\"form-control media-input\"  id=\"video-" + ++this.vidIndex + "\" accept=\"video/*,image/*\">\n                </div>";
        }
    }, {
        key: "attachVideoToInput",
        value: function attachVideoToInput(files) {
            $("#video-" + this.vidIndex).get(0).files = files;
        }
    }, {
        key: "attachPicToInput",
        value: function attachPicToInput(files) {
            $("#picture-" + this.picIndex).get(0).files = files;
        }
    }, {
        key: "attachFileToInput",
        value: function attachFileToInput(files) {
            $("#media-" + this.index).get(0).files = files;
        }
    }]);

    return MediaHandler;
}();

var media = new MediaHandler();

media.getElements();
media.addEventListener();
media.addRemoveListener();

var PostHeadingNavigator = function () {
    function PostHeadingNavigator(editor) {
        var _this6 = this;

        _classCallCheck(this, PostHeadingNavigator);

        this.editor = editor;
        this.headings = this.getPostHeadings();
        this.active = 0;
        this.nextBtn = null;
        this.prevBtn = null;
        this.textArea = null;
        this.title = null;
        this.helpTex = null;
        this.updateObject();
        this.editor.linkAddedListener = function () {

            _this6.headings[_this6.active].textBoxValue = _this6.textArea.val();
            _this6.updateInputs();
        };
    }

    _createClass(PostHeadingNavigator, [{
        key: "getElements",
        value: function getElements() {
            this.nextBtn = $("#next-btn");
            this.prevBtn = $("#prev-btn");
            this.textArea = $("#editor-text");
            this.title = $("#heading");
            this.helpTex = $("#help-text");
            this.picturePane = $('#picture-pane');
            this.videoPane = $('#video-pane');
            this.uploadPane = $("#upload-pane");
            this.textPane = $("#text-pane");
            this.mediaIput = $(".media-input");
            this.postBtn = $("#post");
        }
    }, {
        key: "extendTextarea",
        value: function extendTextarea() {
            var _this7 = this;

            this.textArea.val = function (val) {
                if (val === undefined) return _this7.textArea.get(0).innerHTML;
                _this7.textArea.get(0).innerHTML = val;
            };
        }
    }, {
        key: "registerEvents",
        value: function registerEvents() {
            var _this8 = this;

            this.nextBtn.click(function (event) {
                _this8.nextHeading();
            });

            this.prevBtn.click(function (event) {
                _this8.previousHeading();
            });

            this.textArea.keyup(function (event) {
                _this8.headings[_this8.active].textBoxValue = _this8.textArea.val();
                _this8.updateInputs();
            });

            this.mediaIput.change(function (event) {
                _this8.handleChoseFile();
            });
        }
    }, {
        key: "handleChoseFile",
        value: function handleChoseFile() {
            if (this.mediaIput.val() != '') {
                this.uploadPane.append("");
            }
        }
    }, {
        key: "nextHeading",
        value: function nextHeading() {
            if (this.active < this.headings.length) {
                this.active += 1;
                this.updateElement();
                this.editor.hideBalloon();
            }
        }
    }, {
        key: "previousHeading",
        value: function previousHeading() {
            if (this.active > 0) {
                this.active -= 1;
                this.updateElement();
                this.editor.hideBalloon();
            }
        }
    }, {
        key: "updateObject",
        value: function updateObject() {

            this.headings[0].textBoxValue = $('input[name="tribe"]').val();
            this.headings[1].textBoxValue = $('input[name="region"]').val();
            this.headings[2].textBoxValue = $('input[name="country"]').val();
            this.headings[3].textBoxValue = $('input[name="language"]').val();
            this.headings[4].textBoxValue = $('input[name="geo-description"]').val();
            this.headings[5].textBoxValue = $('input[name="history"]').val();
            this.headings[6].textBoxValue = $('input[name="literature"]').val();
            this.headings[7].textBoxValue = $('input[name="tradition"]').val();
            this.headings[8].textBoxValue = $('input[name="leadership"]').val();
            this.headings[9].textBoxValue = $('input[name="social-institution"]').val();
            this.headings[10].textBoxValue = $('input[name="religion"]').val();
            this.headings[11].textBoxValue = $('input[name="festivals"]').val();
            this.headings[12].textBoxValue = $('input[name="occupation"]').val();
            this.headings[13].textBoxValue = $('input[name="technology"]').val();
            this.headings[14].textBoxValue = $('input[name="clothing"]').val();
            this.headings[15].textBoxValue = $('input[name="food"]').val();
            this.headings[16].textBoxValue = $('input[name="tourist-attraction"]').val();
            this.headings[17].textBoxValue = $('input[name="other-traditions"]').val();
        }
    }, {
        key: "updateInputs",
        value: function updateInputs() {

            switch (this.active) {
                case 0:
                    $('input[name="tribe"]').val(this.headings[0].textBoxValue);
                    break;
                case 1:
                    $('input[name="region"]').val(this.headings[1].textBoxValue);
                    break;
                case 2:
                    $('input[name="country"]').val(this.headings[2].textBoxValue);
                    break;
                case 3:
                    $('input[name="language"]').val(this.headings[3].textBoxValue);
                    break;
                case 4:
                    $('input[name="geo-description"]').val(this.headings[4].textBoxValue);
                    break;
                case 5:
                    $('input[name="history"]').val(this.headings[5].textBoxValue);
                    break;
                case 6:
                    $('input[name="literature"]').val(this.headings[6].textBoxValue);
                    break;
                case 7:
                    $('input[name="tradition"]').val(this.headings[7].textBoxValue);
                    break;
                case 8:
                    $('input[name="leadership"]').val(this.headings[8].textBoxValue);
                    break;
                case 9:
                    $('input[name="social-institution"]').val(this.headings[9].textBoxValue);
                    break;
                case 10:
                    $('input[name="religion"]').val(this.headings[10].textBoxValue);
                    break;
                case 11:
                    $('input[name="festivals"]').val(this.headings[11].textBoxValue);
                    break;
                case 12:
                    $('input[name="occupation"]').val(this.headings[12].textBoxValue);
                    break;
                case 13:
                    $('input[name="technology"]').val(this.headings[13].textBoxValue);
                    break;
                case 14:
                    $('input[name="clothing"]').val(this.headings[14].textBoxValue);
                    break;
                case 15:
                    $('input[name="food"]').val(this.headings[15].textBoxValue);
                    break;
                case 16:
                    $('input[name="tourist-attraction"]').val(this.headings[16].textBoxValue);
                    break;
                case 17:
                    $('input[name="other-traditions"]').val(this.headings[17].textBoxValue);

            }
        }
    }, {
        key: "updateElement",
        value: function updateElement() {
            if (this.active + 1 >= this.headings.length) {
                this.nextBtn.hide();
            } else {
                this.nextBtn.show();
            }

            if (this.active <= 0) {
                this.prevBtn.hide();
            } else {
                this.prevBtn.show();
            }

            if (this.active === this.headings.length - 2) {

                this.title.text(this.headings[this.active].heading);
                this.helpTex.text(this.headings[this.active].description);
                this.textPane.hide();
                this.picturePane.show();
                this.videoPane.hide();
                this.postBtn.hide();
            } else if (this.active === this.headings.length - 1) {
                this.title.text(this.headings[this.active].heading);
                this.helpTex.text(this.headings[this.active].description);
                this.textPane.hide();
                this.picturePane.hide();
                this.videoPane.show();
                this.postBtn.show();
            } else {
                this.textPane.show();
                this.videoPane.hide();
                this.picturePane.hide();
                this.title.text(this.headings[this.active].heading);
                this.helpTex.text(this.headings[this.active].description);
                this.textArea.val(this.headings[this.active].textBoxValue);
                this.postBtn.hide();
                this.updateInputs();
            }

            this.editor.addLinkEventListener();
        }
    }, {
        key: "getPostHeadings",
        value: function getPostHeadings() {
            return [{
                heading: "Tribe/ Community",
                description: "On this heading you will write about the tribe/community that practice this culture",
                textBoxValue: ""
            }, {
                heading: "Region/ Province/ State",
                description: "On this heading you will write about the Region/Province/State that practice this culture",
                textBoxValue: ""
            }, {
                heading: "Country",
                description: "On this heading you will write about the Country in which this culture is prevalent",
                textBoxValue: ""
            }, {
                heading: "Language(s)",
                description: "On this heading you will write about the Language(s) popular in this culture",
                textBoxValue: ""
            }, {
                heading: "Landscape",
                description: "On this heading you will on  the Landscape of this culture",
                textBoxValue: ""
            }, {
                heading: "History and Mythology",
                description: "On this heading you will write about the History and Mythology of this culture",
                textBoxValue: ""
            }, {
                heading: "Literature",
                description: "On this heading you will write about the Literature of this culture",
                textBoxValue: ""
            }, {
                heading: "Music and Arts",
                description: "On this heading you will write about the culture\'s music and art",
                textBoxValue: ""
            }, {
                heading: "Politics and Leadership",
                description: "On this heading you will write about the Politics and Leadership of this culture",
                textBoxValue: ""
            }, {
                heading: "Social Institutions",
                description: "On this heading you will write about the Social Institutions of this culture",
                textBoxValue: ""
            }, {
                heading: "Religion and Rituals",
                description: "On this heading you will write about the Religion and Ritual perform by this culture",
                textBoxValue: ""
            }, {
                heading: "Festivals",
                description: "On this heading you will write about the festivals this culture",
                textBoxValue: ""
            }, {
                heading: "Occupations",
                description: "On this heading you will write about the  Occupations of this culture",
                textBoxValue: ""
            }, {
                heading: "Tools and Technologies",
                description: "On this heading you will write about the  Tools and Technologies of this culture",
                textBoxValue: ""
            }, {
                heading: "Clothing and Cosmetics",
                description: "On this heading you will write about the  Clothing and Cosmetics of this culture",
                textBoxValue: ""
            }, {
                heading: "Food and Food Processing",
                description: "On this heading you will write about the  Food and Food Processing  of this culture",
                textBoxValue: ""
            }, {
                heading: " Tourist Attractions",
                description: "On this heading you will write about the   Tourist Attractions  of this culture",
                textBoxValue: ""
            }, {
                heading: "Other traditions",
                description: "On this heading you will write about   other traditions  of this culture",
                textBoxValue: ""
            }, {
                heading: "Pictures",
                description: "Upload Picture about their festivals, food ,dressing etc.",
                files: []
            }, {
                heading: "Videos",
                description: "Upload Videos about their festivals, food ,dressing etc",
                files: []
            }];
        }
    }]);

    return PostHeadingNavigator;
}();