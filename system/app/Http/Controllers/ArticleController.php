<?php

namespace App\Http\Controllers;

use App\ArticleMedia;
use App\Http\Requests\StoreArticle;
use App\Picture;
use App\Video;
use Illuminate\Http\Request;
use App\Article;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {

        $articles =  Article::where('user_id',auth()->user()->user_id)->orderBy('created_at','desc')->get();

        return view('user.article.articles')->with(['articles'=>$articles]);


    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \View::make('user.article.create');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArticle $request)
    {


        $this->validate($request,[
           'title'=>'required|unique:articles',
            'tribe'=>'required',
            'region'=>'required',
            'country'=>'required',
            'language'=>'required',
            'geo-description'=>'required',
            'history'=>'required',
            'literature'=>'required',
            'tradition'=>'required',
            'leadership'=>'required',
            'social-institution'=>'required',
            'religion'=>'required',
            'festivals'=>'required',
            'occupation'=>'required',
            'technology'=>'required',
            'clothing'=>'required',
            'food'=>'required',
            'tourist-attraction'=>'required',
            'other-traditions'=>'required',
            'videos.*'=> 'mimetypes:video/*|max:3584',
            'pictures.*'=>'image|max:500',
        ]);



        print_r($_POST);
        exit();


        $article = new Article();
        $article->user_id = $request->user()->user_id;
        $article->title = $request->input('title');
        $article->tribe  = $request->input('tribe');
        $article->region =$request->input('region');
        $article->country = $request->input('country');
        $article->language = $request->input('language');
        $article->geo_description = $request->input('geo-description');
        $article->history = $request->input('history');
        $article->literature = $request->input('literature');
        $article->tradition = $request->input('tradition');
        $article->leadership = $request->input('leadership');
        $article->social_institution = $request->input('social-institution');
        $article->religion = $request->input('religion');
        $article->festivals = $request->input('festivals');
        $article->occupations = $request->input('occupation');
        $article->technology = $request->input('technology');
        $article->clothing = $request->input('clothing');
        $article->food = $request->input('food');
        $article->tourist_attraction = $request->input('tourist-attraction');
        $article->other_traditions = $request->input('other-traditions');
        $article->article_references = $request->input('references');
        $article->published = true;




        if($article->save()){
            if($request->hasFile('pictures'))
                $this->uploadPictures($request->file('pictures'),$request->input('pictures_description'),$article->article_id);
            if($request->hasFile('videos'))
                $this->uploadVideos($request->file('videos') ,$request->input('videos_description'),$article->article_id);
            return redirect(url()->previous())->with('success',"Article was successfully published");
        }else{
            return redirect(url()->previous())->with('error',"Article was not   published due to some system error.");
        }

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $pictures = Article::find($article->article_id)->pictures;
        $videos  = Article::find($article->article_id)->videos;
        return \View::make('user.article.edit',['article'=>$article,'videos'=>$videos,'pictures'=>$pictures]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required',
            'tribe'=>'required',
            'region'=>'required',
            'country'=>'required',
            'language'=>'required',
            'geo-description'=>'required',
            'history'=>'required',
            'literature'=>'required',
            'tradition'=>'required',
            'leadership'=>'required',
            'social-institution'=>'required',
            'religion'=>'required',
            'festivals'=>'required',
            'occupation'=>'required',
            'technology'=>'required',
            'clothing'=>'required',
            'food'=>'required',
            'tourist-attraction'=>'required',
            'other-traditions'=>'required',
            'videos.*'=> 'mimetypes:video/*|max:3584',
            'pictures.*'=>'image|max:500',
        ],[
            'title.required'=>'An article title is required',
            'tribe.required' =>'Write something about the tribe/community',
            'region.required' => 'Write something about the Region/ Province/ State',
            'country.required' => 'Write something about the country',
            'language.required' =>'Write something about their languages',
            'geo-location.required'=>'Write something about the Geographical Description of their area',
            'history.required' =>'Write something about their History and Mythology',
            'literature.required' =>'Write something about their literature',
            'tradition.required' => 'Write something about their tradition',
            'leadership.required' =>'Write something about their Politics and Leadership',
            'social-institution.required'=>'Write something about their Social Institutions',
            'religion.required'=>'Write something about their Religion and Ritual ',
            'festivals.required'=>'Write something about their festivals',
            'occupation.required' => 'Write something about their occupation',
            'technology.required' => 'Write something about their tools and technology',
            'clothing.required' => 'Write something about their clothing and cosmetics',
            'food.required' => 'Write something about their Food and Food Processing',
            'tourist-attraction.required' => 'Write something about their Tourist Attractions',
            'other-traditions.required' => 'Write something about their other traditions',
            'videos.mimetypes' =>'only video files are allowed',
            'videos.*.max' =>'Videos file size must not be greater than 3.5MB',
            'pictures.*.max' => 'Picture file must not be greater than 500kb'
        ]);


        $article =  Article::find($id);
        $article->title = $request->input('title');
        $article->tribe  = $request->input('tribe');
        $article->region =$request->input('region');
        $article->country = $request->input('country');
        $article->language = $request->input('language');
        $article->geo_description = $request->input('geo-description');
        $article->history = $request->input('history');
        $article->literature = $request->input('literature');
        $article->tradition = $request->input('tradition');
        $article->leadership = $request->input('leadership');
        $article->social_institution = $request->input('social-institution');
        $article->religion = $request->input('religion');
        $article->festivals = $request->input('festivals');
        $article->occupations = $request->input('occupation');
        $article->technology = $request->input('technology');
        $article->clothing = $request->input('clothing');
        $article->food = $request->input('food');
        $article->tourist_attraction = $request->input('tourist-attraction');
        $article->other_traditions = $request->input('other-traditions');
        $article->article_references = $request->input('references');

        if($article->save()){
            $picRecs =  Article::find($id)->pictures;
            $vidRecs =  Article::find($id)->videos;
            $picFiles =  $request->input('old_pictures');
            $vidFiles =  $request->input('old_videos');
            $vidOdes = $request->input('old_vid_description');
            $picOdes = $request->input('old_pic_description');

            $this->deleteRemovedPictures($picRecs,$picFiles);
            $this->deleteRemovedVideos($vidRecs,$vidFiles);
            $this->updatePictureDescription($picFiles,$picOdes);
            $this->updateVideoDescription($vidFiles,$vidOdes);

            if($request->hasFile('pictures')){
                $newPics = $request->file('pictures');
                $picDes = $request->input('pictures_description');
                $this->uploadPictures($newPics,$picDes,$id);
            }

            if($request->hasFile('videos')){
                $newVids = $request->file('videos');
                $vidDes  = $request->input('videos_description');
                $this->uploadVideos($newVids,$vidDes,$id);
            }


            return redirect(url()->previous())->with('success','Article was updated successfully');
        }else{
            return redirect(url()->previous())->with('error','Article was not updated due to system error');
        }



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $videos = Article::find($id)->videos;
        $pictures =Article::find($id)->pictures;


        foreach ($pictures as $picture){
            $this->deleteFile('pictures/'.$picture->file_name);
            Picture::destroy($picture->picture_id);
        }

        foreach ($videos as $video){
            $this->deleteFile('videos/'.$video->file_name);
            Video::destroy($video->video_id);
        }

        Article::destroy($id);
        return redirect(url()->previous())->with('delete_success','The article was successfully deleted');
    }


    protected function uploadFile($files,$des,$id){

        for($i=0; $i<count($files); $i++){
            $media = new ArticleMedia();
            $ext = $files[$i]->getClientOriginalExtension();
            $filename = md5(rand()).'.'.$ext;
            $media->article_id = $id;
            $media->file_name =$filename;
            $media->media_description = $des[$i];
            $media->save();
            $files[$i]->storeAs("media",$filename,'public');
        }

    }

    protected function updatePictureDescription($ids,$des){
        if(is_array($ids)){
            for($i=0; $i<count($ids); $i++){
                $pic = Picture::find($ids[$i]);
                $pic->description=$des[$i];
                $pic->save();
            }
        }

    }

    protected function updateVideoDescription($ids,$des){
        if(is_array($ids)){
            for($i=0; $i<count($ids); $i++){
                print $des[$i];
                $vid = Video::find($ids[$i]);
                $vid->description=$des[$i];
                $vid->save();
            }
        }

    }
    protected  function uploadVideos($files,$des,$article_id){

        for($i=0; $i<count($files); $i++){
            $video = new Video();
            $ext = $files[$i]->getClientOriginalExtension();
            $filename = md5(rand().time()).'.'.$ext;
            $video->article_id = $article_id;
            $video->file_name = $filename;
            $video->description = $des[$i];
            $video->save();
            $files[$i]->storeAs("media/videos",$filename,'shared_public');
        }
    }


    protected function uploadPictures($files,$des,$article_id){

        for($i=0; $i<count($files); $i++){
            $picture = new Picture();
            $ext = $files[$i]->getClientOriginalExtension();
            $filename = md5(rand().time()).'.'.$ext;
            $picture->article_id = $article_id;
            $picture->file_name = $filename;
            $picture->description = $des[$i];
            $picture->save();
            $files[$i]->storeAs("media/pictures",$filename,'shared_public');
        }
    }

    protected function deleteRemovedVideos($records,$files){
       $files = is_null($files)?[]:$files;
        if(is_array($files)){
            foreach ($records as $record){
                if(!in_array($record->video_id,$files)){
                    $this->deleteFile('videos/'.$record->file_name);
                    Video::destroy($record->video_id);
                }
            }
        }

    }

    protected function deleteRemovedPictures($records,$files){
        $files = is_null($files)?[]:$files;
        if(is_array($files)){
            foreach ($records as $record){
                if(!in_array($record->picture_id,$files)){
                    $this->deleteFile('pictures/'.$record->file_name);
                    Picture::destroy($record->picture_id);
                }
            }
        }

    }





    protected function deleteFile($file){
        \Storage::delete('media/'.$file);
    }


}
