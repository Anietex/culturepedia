@extends('admin.master')
@section('styles')
    @parent
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/datatables/datatables.css') }}">
@endsection
@section('title','My Articles')
@section('page-title','My Articles')

@section('header-title')
    <h1><span class="fa fa-list"></span> My Article <small>Manage your articles</small></h1>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
    <li class="breadcrumb-item active" aria-current="page"> My Articles</li>
@endsection
@section('content')
 <div class="card">
     <div class="card-header">
         My Articles
     </div>
     <div class="card-body">
         <div class="admin-articles">
             <div class="articles">
                 @if(session('delete_success'))
                     <div class="alert alert-success">
                         <p>{{ session('delete_success') }}</p>
                     </div>
                 @endif
                 <table class="table">
                     <thead>
                     <tr>
                         <th width="1%">S/N</th>
                         <th>Title</th>
                         <th width="1%">View</th>
                         <th width="1%">Edit</th>
                         <th width="1%">Delete</th>
                     </tr>
                     </thead>
                     <tbody>
                     @foreach($articles as $article)
                         <tr>
                             <td>{{ $loop->index+1 }}</td>
                             <td>{{ $article->title }}</td>
                             <td><a  href="{{ url('article/'.$article->article_id.'/'.str_slug($article->title)) }}" class="btn btn-outline-info btn-sm">View</a></td>
                             <td><a href="{{ url('admin/article/'.$article->article_id.'/edit') }}" class="btn btn-outline-info btn-sm">Edit</a></td>
                             <td>
                                 <form method="post" action=" {{ url('admin/article',['id'=>$article->article_id]) }}">
                                     {{ method_field('DELETE') }}
                                     {{ csrf_field() }}
                                     <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                 </form>
                             </td>
                         </tr>
                     @endforeach
                     </tbody>
                 </table>
             </div>
         </div>
     </div>
 </div>
@endsection
@section('scripts')
    @parent
    <script type="text/javascript" src="{{  asset('assets/datatables/datatables.js') }}"></script>
    <script>
        $(document).ready( function () {
            $('.table').DataTable({
                autoFill: true
            });
        } );
    </script>
@endsection