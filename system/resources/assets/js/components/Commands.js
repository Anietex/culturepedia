export class Commands {

    static bold() {
        document.execCommand('bold', false, null)
    }

    static italic() {
        document.execCommand('italic', false, null)
    }

    static orderedList() {
        document.execCommand('insertOrderedList', false, null);
    }

    static unorderedList() {
        document.execCommand('insertUnorderedList', false, null)
    }

    static undo() {
        document.execCommand('undo', false, null);
    }

    static redo() {
        document.execCommand('redo', false, null)
    }

    static createLink(link, text, title, def) {
        if (def === '' && link === '#')
            return;
        if (link === '#' && def !== '') {
            document.execCommand('insertHTML', false, `<span class="def-text" data-def="${ def }">${text}</span>`)
        } else {
            document.execCommand('insertHTML', false, `<a href="${link}" title="${title}" class="article-link" target="_blank" data-def="${ def }">${text}</a>`)
        }
    }

    static addRefLink(cite,refNo){
        document.execCommand('insertHTML', false, `<sup><a href="${'#cite-note-'+refNo}" id="${'ref-'+refNo}"   class="cite-link" data-reftype="${cite.type}"  >[${refNo}]</a></sup>&ensp; `);
    }

}