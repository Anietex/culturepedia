@extends('admin.master')
@section('title','Dashboard')
@section('header-title')
    <h1><span class="fa fa-dashboard"></span> Dashboard<small> Manage your site</small></h1>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item active" aria-current="page"> Dashboard</li>
@endsection
@section('content')
    <div class="card overview">
        <div class="card-header">
            Overview
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-4">
                    <div class="item">
                        <h2><span class="fa fa-pencil"></span> {{ number_format($totalArticles) }}</h2>
                        <h4>Total Articles</h4>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="item">
                        <h2><span class="fa fa-pencil"></span> {{ number_format($published) }}</h2>
                        <h4>Published Articles</h4>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="item">
                        <h2><span class="fa fa-users"></span> {{ number_format($totalUsers) }}</h2>
                        <h4>Total Users</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card mt-3">
        <div class="card-header">
            Latest Articles
        </div>
        <div class="card-body">
            <div class="admin-articles">
                <div class="articles">
                    @if(session('delete_success'))
                        <div class="alert alert-success">
                            <p>{{ session('delete_success') }}</p>
                        </div>
                    @endif
                    @if(session('approval_success'))
                        <div class="alert alert-success">
                            <p>{{ session('approval_success') }}</p>
                        </div>
                    @endif
                    @if(session('approval_error'))
                        <div class="alert alert-danger">
                            <p>{{ session('approval_error') }}</p>
                        </div>
                    @endif
                    @if(session('disapproval_success'))
                        <div class="alert alert-success">
                            <p>{{ session('disapproval_success') }}</p>
                        </div>
                    @endif
                    @if(session('disapproval_error'))
                        <div class="alert alert-danger">
                            <p>{{ session('disapproval_error') }}</p>
                        </div>
                    @endif
                    <table class="table table-sm">
                        <thead>
                        <tr>
                            <th width="1%">S/N</th>
                            <th>Title</th>
                            <th>Date Added</th>
                            <th width="5%">Status</th>
                            <th width="1%">View</th>
                            <th width="1%">Approval</th>
                            <th width="1%">Edit</th>
                            <th width="1%">Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($articles as $article)
                            <tr>
                                <td>{{ $loop->index+1 }}</td>
                                <td>{{ $article->title }}</td>
                                <td>{{ $article->updated_at }}</td>
                                <td>@if($article->approved==true){{ 'Approved' }} @else{{ 'Unapproved' }} @endif</td>
                                <td><a  href="#" class="btn btn-outline-info btn-sm">View</a></td>

                                <td>
                                    @if($article->approved ==true)
                                        <form method="post" action=" {{ url('admin/article/disapprove',['id'=>$article->article_id]) }}">
                                            {{ method_field('PUT') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-outline-info btn-sm" title="Disapprove article">Disapprove</button>
                                        </form>
                                    @else
                                        <form method="post" action=" {{ url('admin/article/approve',['id'=>$article->article_id]) }}">
                                            {{ method_field('PUT') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-outline-info btn-sm">Approve</button>
                                        </form>
                                    @endif
                                </td>
                                <td><a href="{{ url('admin/article/'.$article->article_id.'/edit') }}" class="btn btn-outline-info btn-sm">Edit</a></td>
                                <td>
                                    <form method="post" action=" {{ url('admin/article',['id'=>$article->article_id]) }}">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    <div class="card mt-3">
        <div class="card-header">
            Newest Users
        </div>
        <div class="card-body">
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Joined On</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $loop->index+1 }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection