export class CitationModalCreator {


    constructor(){
        this.tabs = [];
        this.tabsBtn = [];
    }


    getModalContent(){
        this.createContainer();
        this.createWrapper();
        this.createHeader();
        this.createTabBar();
        this.createBody();
        this.createFooter();
        this.createAutomaticTab();
        this.createManualTab();
        this.createReuseTab();



        this.body.appendChild(this.automaticTab);
        this.body.appendChild(this.manualTab);
        // this.body.appendChild(this.reuseTab);

        this.wrapper.appendChild(this.container);
        this.container.appendChild(this.header);
        this.container.appendChild(this.tabBar);
        this.container.appendChild(this.body);
        this.container.appendChild(this.footer);

        return this.wrapper;
    }


    createContainer(){
        this.container = this.createElm('div');
        this.container.classList.add('c-modal-content');
        this.container.onclick = e =>{
            e.stopPropagation();
        }
    }


    createWrapper(){
        this.wrapper = this.createElm('div');
        this.wrapper.classList.add('citation-wrapper');


    }


    createHeader(){

        this.header = this.createElm('div');
        this.header.classList.add('header');
        this.ModalTitle = this.createElm('p');
        this.ModalTitle.innerText = "Add Citation";
        this.header.appendChild( this.ModalTitle);

    }


    setModalTitle(title){
        this.ModalTitle.innerText = title;
    }


    createTabBar(){
        this.tabBar = this.createElm('div');
        this.tabBar.classList.add('tabs-btn');

        this.manualBtn = this.createElm('button');
        this.manualBtn.innerText = "Manual";
        this.manualBtn.classList.add('active');
        this.manualBtn.id = "manual";

        this.autoBtn = this.createElm('button');
        this.autoBtn.innerText = "Automatic"
        this.autoBtn.id = "automatic";

        this.reuseBtn = this.createElm('button');
        this.reuseBtn.innerText="Reuse citation";
        this.reuseBtn.id = "reuse";



        this.tabBar.appendChild(this.autoBtn);
        this.tabBar.appendChild(this.manualBtn);
        // this.tabBar.appendChild(this.reuseBtn);

        this.tabsBtn.push(this.manualBtn,this.autoBtn,this.reuseBtn);


    }

    createBody(){
        this.body = this.createElm('div');
        this.body.classList.add('body');
    }

    createAutomaticTab(){
        this.automaticTab = this.createElm('div');
        this.automaticTab.id= "automatic-tab";
        this.automaticTab.classList.add('tab');

        this.autoInputPane = this.createElm('div');
        this.autoSubmitBtn = this.createElm('button');


        this.autoSubmitBtn.innerText= "Add";
        this.autoSubmitBtn.classList.add('btn','btn-primary','btn-sm','btn-block');

        let inputWrapper = this.createElm('div');
        inputWrapper.classList.add('auto-tab-input');





        this.autoProgresBar = this.createElm('div');
        this.autoProgresBar.innerHTML = `
            <div class="progress">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div>
            </div>
        `;

        let label = this.createElm('label');
        label.innerText = "Enter URL or ISBN";

        this.autoInput = this.createElm('input');
        this.autoInput.id = "auto-input";
        this.autoInput.placeholder = "e.g http://example.com";

        this.autoButton = this.createElm('button');

        this.autoButton.innerText = "Generate";
        this.autoButton.id = "gen-btn";


        inputWrapper.appendChild(this.autoInput);
        inputWrapper.appendChild(this.autoButton);

        this.autoInputPane.appendChild(this.autoProgresBar);
        this.autoInputPane.appendChild(label);
        this.autoInputPane.appendChild(inputWrapper);

        this.autoProgresBar.style.display = 'none';



        this.automaticTab.appendChild(this.autoInputPane);
        // this.automaticTab.appendChild(inputWrapper);

        this.tabs.push(this.automaticTab);


    }


    createManualTab(){
        this.manualTab = this.createElm('div');

        this.webBtnWrapper = this.createElm('div');




        this.manualTab.classList.add('manual-tab-wrapper');
        this.manualTab.id= "manual-tab";


        let row1 = this.createElm('div');
        row1.classList.add('mt-row');

        let row2 = this.createElm('div');
        row2.classList.add('mt-row');

        let web = this.createElm('div');
        web.classList.add('col');

        this.webBtn = this.createElm('button');
        this.webBtn.innerHTML = `<span class="fa fa-bookmark"></span> Website`;
        this.webBtn.id = 'web-btn';

        web.appendChild(this.webBtn);

        row1.appendChild(web);

        let book = this.createElm('div');
        book.classList.add('col');

        this.bookBtn = this.createElm('button');
        this.bookBtn.innerHTML = `<span class="fa fa-book"></span> Book`;

        book.appendChild(this.bookBtn);

        row1.appendChild(book);

        let news = this.createElm('div');
        news.classList.add('col');

        this.newsBtn = this.createElm('button');
        this.newsBtn.innerHTML = `<span class="fa fa-newspaper-o"></span> News`;

        news.appendChild(this.newsBtn);
        row2.appendChild(news);

        let journal = this.createElm('div');
        journal.classList.add('col');

        this.jounalBtn = this.createElm('button');
        this.jounalBtn.innerHTML = `<span class="fa fa-object-group"></span> Journal`;

        journal.appendChild(this.jounalBtn);

        row2.appendChild(journal);




        this.webBtnWrapper.appendChild(row1);
        this.webBtnWrapper.appendChild(row2);

        this.manualTab.appendChild(this.webBtnWrapper);

        this.createWebPane();
        this.createBookPane();
        this.createNewsPane();
        this.createJournalPane();

        this.manualTab.appendChild(this.webPane);
        this.manualTab.appendChild(this.bookPane);
        this.manualTab.appendChild(this.newsPane);
        this.manualTab.appendChild(this.journalCitePane);

        this.tabs.push(this.manualTab);


    }


    createWebPane(){
        this.webPane = this.createElm('div');
        this.webPane.classList.add('web-pane');

        this.closeWebPaneBtn = this.createElm('button');
        this.closeWebPaneBtn.innerText = "Close";

        this.webCiteForm = this.createElm('form');


        let urlWrapper = this.createElm('div');
        urlWrapper.classList.add('form-group');

        urlWrapper.innerHTML = `
            <label>URL *</label>
            <input type="url" name="url"  placeholder="e.g. http//:example.com"  class="form-control form-control-sm" required>
       
        `

        let titleDiv = this.createElm('div');
        titleDiv.classList.add('form-group');

        titleDiv.innerHTML = `
            <label>Title *</label>
            <input type="text" name="title" placeholder="Title of the website" class="form-control form-control-sm" required>
        `;

        let fnDiv = this.createElm('div');
        fnDiv.classList.add('form-group');
        fnDiv.innerHTML = `
            <label>First Name</label>
            <input type="text" class="form-control form-control-sm" placeholder="First name of the content author" name="first-name" />
        
        `;


        let lnDiv = this.createElm('div');
        lnDiv.classList.add('form-group');

        lnDiv.innerHTML = `
                 <label>Last Name</label>
                 <input type="text" class="form-control form-control-sm" placeholder="Last name of the content author" name="last-name" />    
        `;

        let dateRetrieved = this.createElm('div');
        dateRetrieved.classList.add('form-group');
        dateRetrieved.innerHTML = `
            <label>Date Retrieved</label>
                 <input type="text" class="form-control form-control-sm" placeholder="eg June 12, 2018" name="date-retrieved"/>    
        
        `;


        this.webCiteForm.appendChild(urlWrapper);
        this.webCiteForm.appendChild(titleDiv);
        this.webCiteForm.appendChild(lnDiv);
        this.webCiteForm.appendChild(fnDiv);
        this.webCiteForm.appendChild(dateRetrieved);
        this.webPane.appendChild(this.webCiteForm);


        let webCiteFooter = this.createElm('div');
        webCiteFooter.classList.add('footer');

        let webSubmitBtn = this.createElm('button');
        webSubmitBtn.type = "submit";
        webSubmitBtn.innerText= "Add";
        webSubmitBtn.classList.add('btn','btn-primary','btn-sm','btn-block');

        this.webCiteForm.appendChild(webSubmitBtn);


    }


    createBookPane(){
        this.bookPane = this.createElm('div');
        this.bookPane.classList.add('book-pane');

        this.bookCiteForm = this.createElm('form');

        let titleDiv = this.createElm('div');





         titleDiv.innerHTML = `
            <label>Title *</label>
            <input type="text" name="title" placeholder="Title of the book" class="form-control form-control-sm" required>
        `;

        let fnDiv = this.createElm('div');
        fnDiv.classList.add('form-group');
        fnDiv.innerHTML = `
            <label>First Name</label>
            <input type="text" class="form-control form-control-sm" placeholder="First name of the  author" name="first-name" />
        
        `;


        let lnDiv = this.createElm('div');
        lnDiv.classList.add('form-group');

        lnDiv.innerHTML = `
                 <label>Last Name</label>
                 <input type="text" class="form-control form-control-sm" placeholder="Last name of the  author" name="last-name" />    
        `;


        let pubYear = this.createElm('div');
        pubYear.classList.add('form-group');

        pubYear.innerHTML = `
                <label>Year of Publication</label>
                 <input type="text" class="form-control form-control-sm" placeholder="e.g 1994" name="publication-year"/>    
        
        `;

        let publisher = this.createElm('div');

        publisher.classList.add('form-group');
        publisher.innerHTML = `
            <label>Publisher</label>
                 <input type="text" class="form-control form-control-sm" placeholder="" name="publisher"/>    
        `;


        let pubLocation = this.createElm('div');
        pubLocation.classList.add('form-group');

        pubLocation.innerHTML = `
        <label>Location Of Publisher</label>
                 <input type="text" class="form-control form-control-sm" placeholder=""   name="publisher-location"/>    
        `;


        let pages =this.createElm('div');

        pages.classList.add('form-group');

        pages.innerHTML = `
            
            <label>Page(s)</label>
                 <input type="text" class="form-control form-control-sm" placeholder="e.g 1 or 23-50 or 23,24,25 " name="pages"/>    
        
        `;

        let submitBtn = this.createElm('button');

        submitBtn.classList.add('btn','btn-primary','btn-sm','btn-block');

        submitBtn.type = 'submit';

        submitBtn.innerHTML = "Add";


        this.bookCiteForm.appendChild(titleDiv);
        this.bookCiteForm.appendChild(lnDiv);
        this.bookCiteForm.appendChild(fnDiv);
        this.bookCiteForm.appendChild(publisher);
        this.bookCiteForm.appendChild(pubYear);
        this.bookCiteForm.appendChild(pubLocation);
        this.bookCiteForm.appendChild(pages);
        this.bookCiteForm.appendChild(submitBtn);
        this.bookPane.appendChild(this.bookCiteForm);

    }
    
    
    createNewsPane(){
        this.newsPane  = this.createElm('div');
        this.newsPane.classList.add('news-pane');
        
        this.newsCiteForm = this.createElm('form');

        let urlWrapper = this.createElm('div');

        urlWrapper.innerHTML = `
            <label>URL</label>
            <input type="url" name="url"  placeholder="URL to the news article if any"  class="form-control form-control-sm" >
       
        `;



        let titleDiv = this.createElm('div');
            titleDiv.classList.add('form-group');
        titleDiv.innerHTML = `
            <label>Source Title *</label>
            <input type="text" name="title" placeholder="Title of the book" class="form-control form-control-sm" required>
        `;

        let fnDiv = this.createElm('div');
        fnDiv.classList.add('form-group');
        fnDiv.innerHTML = `
            <label>First Name</label>
            <input type="text" class="form-control form-control-sm" placeholder="First name of the  author" name="first-name" />
        
        `;


        let lnDiv = this.createElm('div');
        lnDiv.classList.add('form-group');

        lnDiv.innerHTML = `
                 <label>Last Name</label>
                 <input type="text" class="form-control form-control-sm" placeholder="Last name of the  author" name="last-name" />    
        `;

        let sourceDate = this.createElm('div');
        sourceDate.classList.add('form-group');

        sourceDate.innerHTML = `
                <label>Source Date</label>
                 <input type="text" class="form-control form-control-sm" placeholder="e.g 1994" name="source-date"/>    
        
        `;


        let pubName = this.createElm('div');

        pubName.classList.add('form-group');
        pubName.innerHTML = `
            <label>Name of publication</label>
                 <input type="text" class="form-control form-control-sm" placeholder="" name="publication-name"/>    
        `;


        let accessDate = this.createElm('div');

        accessDate.classList.add('form-group');

        accessDate.innerHTML = `
                <label>URL access date</label>
                 <input type="text" class="form-control form-control-sm" placeholder="The date the URL was accessed" name="access-date"/>    
        `;






        let submitBtn = this.createElm('button');

        submitBtn.classList.add('btn','btn-primary','btn-sm','btn-block');

        submitBtn.type = 'submit';

        submitBtn.innerHTML = "Add";

        this.newsCiteForm.appendChild(urlWrapper);
        this.newsCiteForm.appendChild(titleDiv);
        this.newsCiteForm.appendChild(lnDiv);
        this.newsCiteForm.appendChild(fnDiv);
        // this.newsCiteForm.appendChild(sourceDate);
        this.newsCiteForm.appendChild(pubName);
        this.newsCiteForm.appendChild(accessDate);
        this.newsCiteForm.appendChild(submitBtn);

        this.newsPane.appendChild(this.newsCiteForm);

    }





    createJournalPane(){
        this.journalCitePane = this.createElm('div');
        this.journalCitePane.classList.add('journal-pane');
        this.journalCiteForm = this.createElm('form');

        let fnDiv = this.createElm('div');
        fnDiv.classList.add('form-group');
        fnDiv.innerHTML = `
            <label>First Name</label>
            <input type="text" class="form-control form-control-sm" placeholder="First name of the  author" name="first-name" />
        
        `;


        let lnDiv = this.createElm('div');
        lnDiv.classList.add('form-group');

        lnDiv.innerHTML = `
                 <label>Last Name</label>
                 <input type="text" class="form-control form-control-sm" placeholder="Last name of the  author" name="last-name" />    
        `;


        let sourceDate = this.createElm('div');
        sourceDate.classList.add('form-group');

        sourceDate.innerHTML = `
                <label>Source Date</label>
                 <input type="text" class="form-control form-control-sm" placeholder="e.g. May 1994" name="source-date"/>    
        
        `;


        let titleDiv = this.createElm('div');
        titleDiv.classList.add('form-group');
        titleDiv.innerHTML = `
            <label>Title *</label>
            <input type="text" name="title" placeholder="Title of the book" class="form-control form-control-sm" required>
        `;


        let journalDiv = this.createElm('div');

        journalDiv.classList.add('form-group');

        journalDiv.innerHTML = `
             <label>Journal *</label>
            <input type="text" name="journal" placeholder="" class="form-control form-control-sm" required>
      
        `;


        let volumeDiv = this.createElm('div');

        volumeDiv.classList.add('form-group');

        volumeDiv.innerHTML = `
             <label>Volume </label>
            <input type="text" name="volume" placeholder="" class="form-control form-control-sm">
      
        `;


        let pages =this.createElm('div');

        pages.classList.add('form-group');

        pages.innerHTML = `
            
            <label>Page(s)</label>
                 <input type="text" class="form-control form-control-sm" placeholder="e.g. 1 or 23-50 or 23,24,25 " name="pages"/>    
        
        `;

        let urlWrapper = this.createElm('div');

        urlWrapper.innerHTML = `
            <label>URL </label>
            <input type="url" name="url"  placeholder="URL to the news article if any"  class="form-control form-control-sm" >
       
        `;

        let submitBtn = this.createElm('button');

        submitBtn.classList.add('btn','btn-primary','btn-sm','btn-block');

        submitBtn.type = 'submit';

        submitBtn.innerHTML = "Add";



        this.journalCiteForm.appendChild(journalDiv);
        this.journalCiteForm.appendChild(titleDiv);
        this.journalCiteForm.appendChild(urlWrapper);
        this.journalCiteForm.appendChild(lnDiv);
        this.journalCiteForm.appendChild(fnDiv);
        this.journalCiteForm.appendChild(sourceDate);
        this.journalCiteForm.appendChild(volumeDiv);
        this.journalCiteForm.appendChild(pages);
        this.journalCiteForm.appendChild(submitBtn);

        this.journalCitePane.appendChild(this.journalCiteForm);


    }







    createReuseTab(){
        this.reuseTab = this.createElm('div');
        this.reuseTab.id='reuse-tab';
        this.reuseTab.classList.add('tab');
        this.tabs.push(this.reuseTab);
        this.reuseTab.innerHTML="reuse";


    }


    createFooter(){
        this.footer = this.createElm('div');
        this.footer.classList.add('footer');

        this.cancelBtn =  this.createElm('button');

        this.cancelBtn.innerText = "Cancel";
        this.cancelBtn.classList.add('btn','btn-block','btn-sm');
        this.footer.appendChild(this.cancelBtn);

    }



    createElm(tag){
        return document.createElement(tag);
    }








}