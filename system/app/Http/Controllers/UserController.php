<?php

namespace App\Http\Controllers;

use App\Article;
use App\User;
use Illuminate\Http\Request;
use Illuminate\View\View;

class UserController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth');

    }

    public function index(){
        $data['tarticles'] = Article::where('user_id',auth()->user()->user_id)->count();
        $data['tapproved'] = Article::where('user_id',auth()->user()->user_id)->where('approved',true)->count();
        $data['tunapproved'] = Article::where('user_id',auth()->user()->user_id)->where('approved',false)->count();
        $this->shareVars();
        $data['articles'] = Article::where('user_id',auth()->user()->user_id)->orderBy('created_at','desc')->take(7)->get();
        return view('user.home')->with($data);
    }
    public function createArticle(){
        $this->shareVars();
        return \view('user.article.create');
    }

    public function editArticle(Article $article){
        $this->shareVars();
        $pictures = Article::find($article->article_id)->pictures;
        $videos  = Article::find($article->article_id)->videos;
        return \View::make('user.article.edit',['article'=>$article,'videos'=>$videos,'pictures'=>$pictures]);

    }

    public function articles(){
        $this->shareVars();
        $data['articles'] = Article::where('user_id',auth()->user()->user_id)->orderBy('created_at','desc')->get();
        return view('user.article.articles')->with($data);
    }

    public function approvedArticles(){
        $this->shareVars();
        $data['articles'] = Article::where('user_id',auth()->user()->user_id)->where('approved',true)->orderBy('created_at','desc')->get();
        return view('user.article.approved')->with($data);
    }

    public function unapprovedArticles(){
        $this->shareVars();
        $data['articles'] = Article::where('user_id',auth()->user()->user_id)->where('approved',false)->orderBy('created_at','desc')->get();
        return view('user.article.unapproved')->with($data);
    }


    public function updateAccount(Request $request){
        $this->validate($request,['username'=>'required',
            'email'=>'required|email']);
        $admin = User::find(auth()->user()->user_id);
        $admin->username = $request->input('username');
        $admin->email  = $request->input('email');
        $admin->save();
        auth()->login($admin);
        return redirect(url()->previous())->with('au_success','Account was successfully updated');
    }

    public function updatePassword(Request $request){
        $this->validate($request,['password'=>'required|confirmed',
            'old_password'=>'required']);
        $admin = User::find(auth()->user()->user_id);

        if(auth()->attempt(['username'=>$admin->username,'password'=>$request->old_password])){
            $admin->password = bcrypt($request->password);
            $admin->save();
            return redirect(url()->previous())->with('au_success','Password changed successfully');
        }else{
            return redirect(url()->previous())->with('password_error','Invalid old Password');
        }
    }

    private function shareVars(){
        $articles = Article::where('user_id',auth()->user()->user_id)->count();
        $approved = Article::where('user_id',auth()->user()->user_id)->where('approved',true)->count();
        $unapproved = Article::where('user_id',auth()->user()->user_id)->where('approved',false)->count();

        \View::share('t_articles',$articles);
        \View::share('t_approved',$approved);
        \View::share('t_unapproved',$unapproved);
    }

}
