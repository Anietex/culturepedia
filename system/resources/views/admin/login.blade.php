<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/admin.css') }}">
    <title>Admin | Login</title>
</head>
<body>
<header>
    <div class="navigation">
        <nav class="navbar navbar-expand-lg navbar-default bg-light container navbar-fixed">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <a class="navbar-brand" href="#">Culturepedia</a>
            </div>
        </nav>
    </div>
</header>
<section class="main">
    <div class="container">
        <div class="form-wrapper mt-5">
            <div class="row">
                <div class="col-sm-5 col-md-5 col-12 m-auto">
                    <div class="form">

                        <form action="{{ route('admin.login') }}" method="post">
                            {{ csrf_field() }}
                            <div class="header">
                                <p>Log In</p>
                            </div>
                            <div class="body">
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input type="text" name="username" id="username" value="{{ old('username') }}" required class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" autofocus>
                                    @if ($errors->has('username'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('username') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" id="password" value="{{ old('password') }}"  class="form-control  {{ $errors->has('password') ? 'is-invalid' : '' }}" required>
                                    @if ($errors->has('password'))
                                        <div class="invalid-feedback">
                                            {{ $errors->first('password') }}
                                        </div>
                                    @endif

                                </div>
                                <hr>
                                <button type="submit" class="btn btn-primary">Log In</button>

                                <div class="form-check mt-2">
                                    <input class="form-check-input" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}id="remember">
                                    <label class="form-check-label" for="remember">
                                        Remember me
                                    </label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</body>
</html>