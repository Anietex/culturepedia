@extends('layouts.master')
@section('title','My Articles')
@section('section-title','My Articles')

@section('content')
    <div class="container">
        <div class="user-articles">
            <div class="wrapper">
                <h3 class="font-weight-light">My Articles</h3>
                @if(session('delete_success'))
                    <div class="alert alert-success">
                        <p>{{ session('delete_success') }}</p>
                    </div>
                @endif
                <div class="articles">
                    @foreach($articles as $article)
                    <div class="article">
                        <div class="title">
                            <p>{{ $article->title }}</p>
                        </div>
                        <div class="actions">


                            <form action="{{ url('article',['article_id'=>$article->article_id]) }}" method="post">
                                <a href="{{ url('article/'.$article->article_id.'/edit') }}" class="btn btn-default btn-sm">Edit</a>
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit"  class="btn btn-link btn-sm ">Delete</button>
                                <a href="#" class="btn btn-default btn-sm">Unpublished</a>
                            </form>


                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script type="text/javascript">
       // $('')
    </script>
@endsection