@extends('layouts.master')
@section('section-title','All Articles')
@section('title','All Articles')

@section('content')
    <div class="main-content">
        <div class="container">
            <div class="articles">
                <div class="inner-wrapper">
                  <div class="articles-wrapper">
                      @foreach($articles as $article)

                          <div class="article">
                              <div class="heading">
                                  <h1 class="text-capitalize"><a href="{{ url("/article/$article->article_id/".str_slug($article->title,'_')) }}">{{ $article->title }}</a></h1>
                                  <div class="meta">
                                      <p  class="text-uppercase font-weight-bold">Last Updated on {{ $article->updated_at }}</p>
                                  </div>
                              </div>
                              <div class="excerpt text-justify">
                                  <p>{!! $article->tribe !!} </p>
                              </div>
                              <div class="read-btn mt-4">
                                  <a href="{{ url("/article/$article->article_id/".str_slug($article->title,'_')) }}" class="btn btn-outline-primary">Read More</a>
                              </div>
                          </div>
                      @endforeach
                  </div>
                    <hr>
                    <div>
                        {{$paginator->links('vendor.pagination.bootstrap-4')}}
                    </div>

                </div>

            </div>
            <style>
                .translate{
                    display: none;
                }
            </style>

        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        $('sup').hide();
    </script>

@endsection