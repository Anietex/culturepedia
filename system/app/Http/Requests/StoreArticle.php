<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreArticle extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|unique:articles',
            'tribe'=>'required',
            'region'=>'required',
            'country'=>'required',
            'language'=>'required',
            'geo-description'=>'required',
            'history'=>'required',
            'literature'=>'required',
            'tradition'=>'required',
            'leadership'=>'required',
            'social-institution'=>'required',
            'religion'=>'required',
            'occupation'=>'required',
            'technology'=>'required',
            'clothing'=>'required',
            'food'=>'required',
            'tourist-attraction'=>'required',
            'videos.*'=> 'mimetypes:video/*|max:3584',
            'pictures.*'=>'image|max:500',
        ];
    }


    public function messages()
    {
        return [
            'title.required'=>'An article title is required',
            'title.unique' =>'There is already an article with the same title',
            'tribe.required' =>'Write something about the tribe/community',
            'region.required' => 'Write something about the Region/ Province/ State',
            'country.required' => 'Write something about the country',
            'language.required' =>'Write something about their languages',
            'geo-location.required'=>'Write something about the Geographical Description of their area',
            'history.required' =>'Write something about their History and Mythology',
            'literature.required' =>'Write something about their literature',
            'tradition.required' => 'Write something about their tradition',
            'leadership.required' =>'Write something about their Politics and Leadership',
            'social-institution.required'=>'Write something about their Social Institutions',
            'religion.required'=>'Write something about their Religion and Ritual ',
            'occupation.required' => 'Write something about their occupation',
            'technology.required' => 'Write something about their tools and technology',
            'clothing.required' => 'Write something about their clothing and cosmetics',
            'food.required' => 'Write something about their Food and Food Processing',
            'tourist-attraction.required' => 'Write something about their Tourist Attractions',
            'video.mimetypes' =>'only video files are allowed',
            'video.max' =>'Videos file size must not be greater than 3.5MB',
            'picture.max' => 'Picture file must not be greater than 500kb'
        ];
    }
}
