<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MagazinePost extends Model
{

    public function media(){
        return $this->hasMany('App\MagazineMediaFile','post_id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function comments(){

        return $this->hasMany('App\MagazinePostComment','post_id');
    }

    public function likes(){
        return $this->hasMany('App\MagazinePostLike','post_id');
    }
}
