@extends("layouts.master")
@section('title','Reset Password')
@section('section-title','Reset Password')
@section('content')

    <div class="container">
        <div class="form-wrapper my-4">
            <div class="registration">
                <h3>Reset Password</h3>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="form">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form">
                                <form method="post" action="{{ route('password.email') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="email">Email Address</label>
                                        <input type="email" name="email" id="email" value="{{ old('username') }}" required class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" autofocus>
                                        @if ($errors->has('username'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('email') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-check">

                                    </div>
                                    <button type="submit" class="btn btn-primary">Reset Password</button>

                                </form>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="info ml-5 text-center">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
