<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'culturepedia');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^&j<xGJ&8M(_E^ajK2,D5t3{#YF.k:Ae=?,8U4aargC5N_=?PfKjv68pK5c_c<-.');
define('SECURE_AUTH_KEY',  ',/4^eK0iL36c+g}snu:Mg|e=^R^9.~<dgo]MP+Kv}OMClvpxMR&O3@)n`}*7>0Jx');
define('LOGGED_IN_KEY',    'BexU,}}?b=auv[} ?OQbptLpf7O6AF1w*RF:pUeIowOX@aRB,Sh&`8Jl&ubOwS[}');
define('NONCE_KEY',        'ggQ| z74>L!X./s 91SwsfmT1EI-Z2#94{}XOKXE*I({WEntl:`y[v0-9wj}pcnE');
define('AUTH_SALT',        'mSxO8}ji,cp,lXmfPO/BP)~uu6,?]- fih!<I$mPW-oG{z;PMIlPtZ-u4Rzptqi)');
define('SECURE_AUTH_SALT', 'LFAE4>xnc>U5]V<v,!zg<g7b/R-q*/Z]b_io?HoP5q9rk6%4wj(xS7u6@;NbsnxR');
define('LOGGED_IN_SALT',   '1PwTc0)}VIy I0g[~N<*KEnqI$,PDUD;X=]$jh:3jP_{}D#L{q-< >DRd}CGIo>f');
define('NONCE_SALT',       '$3U1PtYEQNVG8VrAy]x{!WUTh0X}OeGfOY,;6PtH)G,:h2oE^3jQJ|w_:Oe5{gTB');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
