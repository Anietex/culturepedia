@extends('user.master')

@section('title','Dashboard')
@section('header-title')
    <h1><span class="fa fa-dashboard"></span> Account<small> Manage your account</small></h1>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item active" aria-current="page"> Account</li>
@endsection
@section('content')
    <div class="card overview">
        <div class="card-header">
            Articles Overview
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-4">
                    <div class="item">
                        <h2><span class="fa fa-pencil"></span> {{ number_format($tarticles) }}</h2>
                        <h4>Total Articles</h4>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="item">
                        <h2><span class="fa fa-toggle-on"></span> {{ number_format($tapproved) }}</h2>
                        <h4>Approved Articles</h4>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="item">
                        <h2><span class="fa fa-toggle-off"></span> {{ number_format($tunapproved) }}</h2>
                        <h4>Unapproved</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card mt-3">
        <div class="card-header">
            Recent Articles
        </div>
        <div class="card-body">
            @include('user.articles_table')
        </div>
    </div>
@endsection