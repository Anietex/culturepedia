<?php

namespace App\Http\Controllers\Magazine\Auth;

use App\MagazineUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
class LoginController extends Controller
{
    public function __construct()
    {

    }

    public function login(Request $request){

        $this->validate($request,
            [
                "username"=>"required",
                "password"=>"required",
        ],[
            'username.required'=>"Enter your username",
            'password.required'=>"Enter your password"
            ]);

        if(Auth::attempt(['username'=>$request->input('username'),"password"=>$request->input('password')])){
            $user = Auth::user();
            $token = $user->createToken("Culture")->accessToken;
            MagazineUser::firstOrCreate(["user_id"=>$user->user_id]);

            return response()->json(['status' => "success","token"=>$token],200);
        }else{
            return response()->json(['status'=>'error','message'=>"Unauthenticated"], 401);
        }
    }


    public function register(Request $request){
        $this->validate($request,
            [
                "username"=>"required|unique:users",
                "password"=>"required|confirmed|min:6",
                "email"=>"required|unique:users",
                "password_confirmation"=>"required"
            ],[
                'username.required'=>"Enter your username",
                'password.required'=>"Enter your password",
                'username.unique'=>"Username already in used",
                "email.unique"=>"Email already in use",
                "password.confirmed"=>"Password did not match",
                "password.min"=>"Password must be at least 6 characters "
            ]);

        $user  = new User();
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = \Hash::make($request->password);

        if($user->save()){
            return response()->json(["status"=>"success","message"=>"registration was successful"]);
        }else{
            return response()->json(["status"=>"error","message"=>"registration was not successful"]);
        }
    }


    public function logout(Request $request){
        $user = $request->user('api');
        $tokens = $user->tokens;
        foreach ($tokens as $token)
                $token->revoke();
        return response()->json(["status"=>"success"]);
    }
}

