import { CitationModal } from "./CitationModal";
import { CitationCreator } from "./CitationCreator";
import { CitationParser } from "./CitationParser";

export class ReferencePopover{

    constructor(){
        this.popover = null;

        this.modal = new CitationModal();
        this.linkIcon = '';
        this.linkText = '';
    }

    showPopover(event){
        this.hidePopover();

        let t = event.target;

        // parsing the url to get the citation index;
        let index = t.href.split('#')[1].split('-')[2];

        let elm = document.getElementById('cite-note-'+index);
        let view = null;

        let cite = elm.querySelector('cite');
        if(cite.classList.contains('web')){
           view = this.getWebCite(CitationParser.parseWeb(elm));
        }
        else if(cite.classList.contains('book')){
            view = this.getBookCite(CitationParser.parseBook(elm));

        }else if(cite.classList.contains('journal')){
            view = this.getJournalCite(CitationParser.parseJournal(elm));
        }else if(cite.classList.contains('news')){
            view = this.getNewsCite(CitationParser.parseNews(elm));
        }


        this.popover = this.createElement();

        this.linkView.appendChild(view);
        document.body.appendChild(this.popover);
        this.popover.style.left =event.pageX-133+"px";
        this.popover.style.top = event.pageY+40+"px";

    }


    hidePopover(){


        if(this.popover){
            if(document.body.contains(this.popover)){
                document.body.removeChild(this.popover);
            }
        }
    }


    createElement(){
        let wrapper = this.getWrapper();
        let inner = this.innerWrapper();
        inner.appendChild(this.getLinkView());
        wrapper.appendChild(inner);
        return wrapper;
    }



    innerWrapper(){
        let innerWrapper = document.createElement('div');
        innerWrapper.classList.add('baloon');
        return innerWrapper;
    }


    getWrapper(){
        let wrapper = document.createElement('div');
        wrapper.id = "balloon";
        wrapper.classList.add('balloon-wrapper');
        return wrapper;

    }

    getLinkView(){
        this.linkView = document.createElement('div');


        this.linkView.classList.add('link-view');
        this.linkView.id = "link-view";

        let header = ()=>{
            let wrapper = document.createElement('div');
            wrapper.classList.add("header");
            let title = document.createElement('div');
            title.classList.add('title');
            let p= document.createElement('p');
            p.innerHTML=`<span class="fa ${this.linkIcon}"></span> ${this.linkText}`;
            title.appendChild(p);
            wrapper.appendChild(title);
            let buttons = document.createElement('div');
            buttons.classList.add('buttons');
            buttons.innerHTML = `<button class="" id="close-btn"><span class="fa fa-ban"></span></button><button id="edit-btn">Edit</button>`;
            // wrapper.appendChild(buttons);
            return wrapper;
        };

        this.linkView.appendChild(header());
        return this.linkView;

    }



   getWebCite(citation){
        let wrapper = CitationCreator.createWeb(citation,null,false);
        this.linkText = "Web Reference";
        this.linkIcon = "fa-bookmark";
        return wrapper;
    }


    getBookCite(citation){
        let wrapper = CitationCreator.createBook(citation,null,false);
        this.linkText = "Book Reference";
        this.linkIcon = "fa-book";

        return wrapper;
    }


    getJournalCite(citation){
        let wrapper = CitationCreator.createJournal(citation,null,false);
        this.linkText = "Web Reference";
        this.linkIcon = "fa-object-group";

        return wrapper;
    }


    getNewsCite(citation){
        let wrapper = CitationCreator.createNews(citation,null,false);

        this.linkText = "News Reference";
        this.linkIcon = "fa-newspaper-o";
        return wrapper;
    }
}