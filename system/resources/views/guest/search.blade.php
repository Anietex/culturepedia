@extends('layouts.master')
@section('section-title','Search Result for '.$query)
@section('title','Search | '.$query)

@section('content')
    <div class="main-content">
        <div class="container">
            <div class="articles my-3">
                @if(count($articles)<1)
                    <div class="text-center">
                        <h4 class="text-center">Nothing was found for your query '{{ $query }}' </h4>
                    </div>
                @endif
                <div class="inner-wrapper">
                    @foreach($articles as $article)
                        <div class="article">
                            <div class="heading">
                                <h1 class="text-capitalize"><a href="{{ url("/article/$article->article_id/".str_slug($article->title,'_')) }}">{{ $article->title }}</a></h1>
                                <div class="meta">
                                    <p  class="text-uppercase font-weight-bold">Last Updated on {{ $article->updated_at }}</p>
                                </div>
                            </div>
                            <div class="excerpt">
                                <p>{{ $article->tribe }}</p>
                            </div>
                            <div class="read-btn mt-4">
                                <a href="{{ url("/article/$article->article_id/".str_slug($article->title,'_')) }}" class="btn btn-outline-primary">Read More</a>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection
