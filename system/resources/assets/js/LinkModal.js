import {ModalBox} from "./ModalBox";

export class LinkModal extends ModalBox {


    constructor(){
        super();

    }

    createOverlay(){
        super.createOverlay();
        let mutObserver = new MutationObserver((change)=>{
            this.getElements();
            this.registerEvents();
            this.showSearchText();
            this.addLinks();
            mutObserver.disconnect();


        });
        mutObserver.observe(this.modal,{childList: true, subtree: true});
        this.modal.appendChild(this.getLinkEdit())

    }










    showModal(cb,searchText){
       super.showModal();
       this.doneCallback = cb;
       this.searchText = searchText;

    }

    setArticleJSON(articleJSON){
        this.articleJSON = articleJSON;
    }

    showSearchText(){

        this.searchBox.value =this.searchText;
    }


    onDoneClick(){
        // this.doneBtn.onclick = ()=>{
            let external_url_title = document.querySelector('#ext-title').value;
            let external_url_link = document.querySelector("#ext-url").value;
            let definition =  document.querySelector("#definition").value;
            let reply = {
                external_url_title,
                external_url_link,
                definition
            };
            if(this.doneCallback)
                 this.doneCallback(reply);
            this.hideModal();

        // }
    }



    addLinks(){
        let resultPane = document.getElementById('search-result');
        if(this.searchText) {

            const articleJson = this.articleJSON;

            let filtered = articleJson.filter(article => {
                let re = new RegExp(this.searchText, 'i');
                return re.test(article.title)
            });
            resultPane.innerHTML = '';
            filtered.forEach(item => {
                resultPane.innerHTML += `<p class="result"><span class="fa fa-link"></span> <a href="${item.link}" target="_blank" title="${item.title}" class="link-result">${item.title}</a> </p>`
            });
            this.addLinkSelectEvent();
        }else {
            resultPane.innerHTML = ''
        }

    }


    addLinkSelectEvent(){
        let links =  document.querySelectorAll('.link-result');
        for(const link of links){
            link.onclick = event =>{
                event.preventDefault();
                let elm = event.target;
                let external_url_title = elm.title;
                let external_url_link = elm.href;
                let definition =  '';
                let reply = {
                    external_url_title,
                    external_url_link,
                    definition
                };

                this.doneCallback(reply);
                this.hideModal();

            }
        }
    }







    getLinkEdit(){
        let le =  document.createElement('div');

        le.onclick = event =>{
            event.stopPropagation();
        };

        let wrapper = document.createElement('div');
        wrapper.classList.add('add-link-wrapper');
        le.classList.add('c-modal-content');
        le.id="link-edit";

        let getHeader =()=>{
            let header =document.createElement('div');
            header.classList.add('header');
            header.innerHTML = "<p>Add Link</p>";



            return header;
        };

        let footer = document.createElement('div');
        footer.classList.add('footer');

        footer.innerHTML = `
                    <button id="cancel-btn">Cancel</button>
                
                
                
                    <button class="" id="done-btn">Done</button>
                `;





        let body = ()=>{
            let bd = document.createElement('div');
            let tabWrap =  document.createElement('div');
            bd.appendChild(tabWrap);
            tabWrap.classList.add('tabs-wrapper');
            bd.classList.add('body');
            let buttons = ()=>{
                let btns = document.createElement('div');
                btns.classList.add('tabs-btn');
                btns.innerHTML = `<button class="active" id="search-btn">Search Pages</button>
                        <button class=""  id="external-btn">External Links</button>
                        <button class=""  id="def-btn">Definition</button>`;
                return btns;
            }
            let tabs = ()=>{
                let tabs = document.createElement('div');
                tabs.classList.add('tabs');

                let searchTab = ()=>{
                    let st = document.createElement('div');
                    st.classList.add('tab');
                    st.id='search-tab';

                    st.innerHTML = `<div class="search-box">
                                <input type="search" id="search-box">
                            </div>
                            <div class="search-result" id="search-result">
                
                               
                            </div>
                                 `;
                    return st;
                }
                let extTab = ()=>{
                    let ext = document.createElement('div');
                    ext.classList.add('tab');
                    ext.id = "external-tab";
                    ext.innerHTML = `
                        <div class="link-box">
                                 <label>Title</label>
                                <input type="url" id="ext-title">
                                
                                <label>URL</label>
                                <input type="url"  placeholder="http://example.com" id="ext-url">
                            </div>
                    `;
                    return ext;
                }
                let defTab = ()=>{
                    let def = document.createElement('div');
                    def.classList.add('tab');
                    def.id='definition-tab';
                    def.innerHTML = `<div class="text-area">
                                <label>Enter the word definition</label>
                                <textarea rows="3" id="definition"></textarea>
                            </div>`;

                    return def;

                }
                tabs.appendChild(searchTab());
                tabs.appendChild(extTab());
                tabs.appendChild(defTab());
                return tabs;

            }
            tabWrap.appendChild(buttons());
            tabWrap.appendChild(tabs());
            return bd;

        };
        le.appendChild(getHeader());
        le.appendChild(body());
        le.appendChild(footer);
        wrapper.appendChild(le);

        return wrapper;

    }




    getElements(){
        this.searchBtn = document.querySelector('#search-btn');
        this.externalBtn = document.querySelector('#external-btn');
        this.defBtn = document.querySelector("#def-btn");
        this.tabs = document.querySelectorAll('.tab');
        this.tabBtns = document.querySelectorAll(".tabs-btn button");
        this.searchTab = document.querySelector('#search-tab');
        this.externalTab = document.querySelector('#external-tab');
        this.defTab = document.querySelector('#definition-tab');
        this.viewLink =document.querySelector('#link-view');
        this.editLink = document.querySelector('#link-edit');
        this.editBtn = document.querySelector("#edit-btn");
        this.cancelBtn = document.querySelector("#cancel-btn");
        this.doneBtn = document.querySelector("#done-btn");
        this.closeBtn = document.querySelector("#close-btn");
        this.searchBox = document.getElementById('search-box');

    }



    registerEvents(){
        this.externalBtn.addEventListener('click',(event)=>{this.showExternal(event)},false);
        this.searchBtn.addEventListener('click',(event)=>{this.showSearch(event)},false);
        this.defBtn.addEventListener('click',(event)=>{this.showDef(event)},false);
        //   this.editBtn.addEventListener('click',()=>{this.showEditPane()});
        this.cancelBtn.addEventListener('click',()=>{this.hideModal()});
        this.doneBtn.addEventListener('click',()=>{this.onDoneClick()});
        this.searchBox.addEventListener('keyup',()=>{
            this.searchText = this.searchBox.value;
            this.addLinks();
        });
        // this.closeBtn.addEventListener('click',()=>{this.hideModal()});
    }






    showEditPane(){
        let vlink = document.getElementById('viewed-link');
        let searchbox = document.getElementById('search-box');

        searchbox.value = vlink.innerText;
        this.viewLink.style.display="none";
        this.editLink.style.display ="block";
    }

    showExternal(event){

        let target = event.target;
        this.removeActionClass();
        this.hideTabs();
        target.classList.add('active');
        this.externalTab.style.display = "block";
    }

    showSearch(event){
        let target = event.target;
        this.removeActionClass();
        this.hideTabs();
        target.classList.add('active');
        this.searchTab.style.display = "block";
    }

    showDef(event){
        let target = event.target;
        this.removeActionClass();
        this.hideTabs();
        target.classList.add('active');
        this.defTab.style.display = "block";
    }

    hideTabs(){
        for(const tab of this.tabs){
            tab.style.display = "none"
        }
    }


    removeActionClass(){
        for(const btn of this.tabBtns){
            btn.classList.remove('active');
        }
    }




}