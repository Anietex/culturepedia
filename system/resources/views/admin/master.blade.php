<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="app-url" content="{{url('/')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @section('styles')
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
    @show
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/admin.css') }}">
    <title>Admin  |  @yield('title')</title>
</head>
<body>
<header>


    <div class="navigation">
        <nav class="navbar navbar-expand-lg navbar-light navbar-default navbar-dark text-light container">
            <a class="navbar-brand" href="{{ url('admin') }}">Culturepedia</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav  mr-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('/admin/articles/all') }}">All Articles <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/admin/articles/approved') }}">Approved Articles</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/admin/articles/unapproved') }}">Unapproved Articles</a>
                    </li>
                </ul>
                <div class="user">
                    <ul class="list-unstyled list-inline navbar-nav">
                        <li><a  class="nav-link" href="#">Welcome, {{ auth()->user()->username }}</a></li>
                        <li class="nav-item dropdown">
                            <a class=" dropdown-toggle nav-link" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Account
                            </a>
                            <div class="dropdown-menu bg-primary">
                                <a class="dropdown-item" href="#"  data-toggle="modal" data-target="#account-modal">Edit Profile</a>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#password-modal">Change Password</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ url('admin/logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">Logout</a>
                            </div>
                        </li>
                    </ul>
                    <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </nav>
    </div>



    <div class="big-header py-3">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <h1>@yield('header-title')</h1>
                </div>
                <div class="col-md-2">
                    <div class="action mt-2">
                        <a href="{{ url('/admin/article/create') }}" class="btn btn-primary">New Article</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
{{--Modal Box--}}

<div class="modal fade" id="account-modal" tabindex="-1" role="dialog" aria-labelledby="account-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Account Update</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ route('admin.update') }}">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" id="username" name="username" value="{{auth()->user()->username}}" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" value="{{ auth()->user()->email }}" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{--modal ends--}}

{{--Password Modal--}}

<div class="modal fade" id="password-modal" tabindex="-1" role="dialog" aria-labelledby="password-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ route('admin.password') }}">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="new-password">New Password</label>
                        <input type="password" class="form-control" id="new-password" name="password" required>
                    </div>
                    <div class="form-group">
                        <label for="confirm_password">Confirm new password</label>
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
                    </div>
                    <div class="form-group">
                        <label for="old_password">Old Password</label>
                        <input type="password" class="form-control" id="old_password" name="old_password" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container  mt-3">
    @if(session('au_success'))
        <p class="alert alert-success alert-dismissable">{{ session('au_success') }}</p>
    @endif

        @if ($errors->has('password'))
            <div class="alert alert-danger alert-dismissable">
                <p>{{ $errors->first('password') }}</p>
            </div>
        @endif
        @if ($errors->has('old_password'))
            <div class="alert alert-danger alert-dismissable">
                <p>{{ $errors->first('password') }}</p>
            </div>
        @endif
        @if (session('password_error'))
            <div class="alert alert-danger alert-dismissable">
                <p>{{ session('password_error') }}</p>
            </div>
        @endif

        @if ($errors->has('password_confirmation'))
            <div class="alert alert-danger alert-dismissable">
                <p>{{ $errors->first('password_confirmation') }}</p>
            </div>
        @endif


</div>
<section id="breadcrumb ">
    <div class="container">
        <ol class="breadcrumb mt-3">
           @section('breadcrumb')
            @show
        </ol>
    </div>
</section>
<section class="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-md-3 mb-4">
                <div class="list-group">
                    <a href="{{ url('/admin/dashboard') }}" class="list-group-item list-group-item-action active">
                        <span class=" fa fa-dashboard"></span> Dashboard
                    </a>
                    <a href="{{ url('/admin/articles') }}" class="list-group-item list-group-item-action"> <span class=" fa fa-list"></span> My Articles  <span class="badge badge-info">{{ $adminArticles }}</span> </a>
                    <a href="{{ url('/admin/articles/all') }}" class="list-group-item list-group-item-action"><span class=" fa fa-align-justify"></span> All Articles <span class="badge badge-info">{{ $allArticles }}</span></a>
                    <a href="{{ url('/admin/articles/unapproved') }}" class="list-group-item list-group-item-action"><span class=" fa fa-toggle-off"></span> Unapproved Articles <span class="badge badge-info">{{ $unapproved }}</span></a>
                    <a href="{{ url('/admin/articles/approved') }}" class="list-group-item list-group-item-action disabled"><span class=" fa fa-toggle-on"></span> Approved Articles <span class="badge badge-info">{{ $approved }}</span></a>
                    <a href="{{ url('/admin/users/all') }}" class="list-group-item list-group-item-action disabled"><span class=" fa fa-users"></span> Users <span class="badge badge-info">{{ $usersCount }}</span></a>
                    <a href="{{ url('/admin/messages') }}" class="list-group-item list-group-item-action disabled"><span class=" fa fa-comments"></span> Messages <span class="badge badge-info">{{ $unreadMsg }} New</span></a>

                </div>
            </div>
            <div class="col-sm-9 col-md-9">
                @section('content')

                @show
            </div>
        </div>
    </div>
</section>
<footer class="text-center">
    <div class="links">
        <p> <a href="{{ url('privacy_policy') }}">Privacy Policy</a> | <a href="{{ url('terms_of_use') }}">Terms of Use</a> </p>
    </div>
    <div class="copyright">
        <p>&copy; {{ date('Y') }} <a href="{{ url('/') }}">Culturepedia </a> | Powered by <a href="http://springlight.ng">Springlight Technology</a> </p>
    </div>
</footer>
@section('scripts')
    <script type="text/javascript" src="{{  asset('assets/js/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
@show
</body>
</html>