
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Culturepedia</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="app-url" content="{{url('/')}}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
</head>
<body>
<header>
   @include('layouts.navigation')
</header>
<main>


    {{--<div class="jumbotron text-center">--}}
        {{--<h1 class="big-text my-3">Welcome to CULTUREPEDIA</h1>--}}
        {{--<div class="form mx-auto mt-5">--}}
            {{--<form action="{{ url('/search') }}" method="get">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-12 col-sm-11">--}}
                        {{--<div class="input-group search" id="article-search">--}}
                            {{--<input autofocus  name="query" placeholder="Search  Cultural facts" type="search" class="form-control search form-control-lg">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-12 col-sm-1 mt-4 mt-sm-0">--}}
                        {{--<button class="btn btn-primary btn-lg">Search</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</form>--}}
        {{--</div>--}}
    {{--</div>--}}


    <section class="home-banner">
        <div class="overlay">
            <div class="container">
                <div class="wrapper text-center">
                    <h1>Welcome to <span class="text-uppercase title">Culturepedia</span></h1>
                    {{--<div class="brief">--}}
                        {{--<p>Thought happens when you hear peace so authoratively that whatsoever you are easing is your attraction.--}}
                            {{--Things, yogis, and true suns will always protect them. Lobster combines greatly with crusted blueberries.--}}
                        {{--</p>--}}
                    {{--</div>--}}
                    <div class="search mt-4">
                        <form action="{{ url('/search') }}" method="get">

                            <div id="article-search">
                                <input type="search" name="query" class="search-box search" placeholder="Search for different cultures facts ..." required>
                            </div>
                          <div class="search-btn mt-5">
                              <button>Search</button>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="slider">
            <div class="owl-carousel">
                <div class="slide1 slide"></div>
                <div class="slide2 slide"></div>
                <div class="slide3 slide"></div>
                <div class="slide4 slide"></div>
                <div class="slide5 slide"></div>
            </div>
        </div>
    </section>





    <section class="contents mt-5">
        <div class="wrapper">
            <div class="row">
                <div class="col-sm-4">
                    <div class="">
                        <h5 class="s-title font-weight-bold">Featured Articles</h5>

                        <div class="home-articles my-4">
                            <ul class="list-unstyled">


                                @foreach($featuredArticles as $article)

                                    <li class="article">
                                        <a href="{{ url("/article/$article->article_id/".str_slug($article->title,'_')) }}" class="a-title"><span class="fa fa-edit"></span> {{ $article->title }}</a>
                                        <div class="meta-data">
                                            <p class="">Last Updated on {{ $article->updated_at }}</p>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="">
                        <h5 class="font-weight-bold s-title">Latest Articles</h5>

                        <div class="home-articles my-4">
                            <ul class="list-unstyled">

                                @foreach($articles as $article)


                                    <li class="article">
                                        <a href="{{ url("/article/$article->article_id/".str_slug($article->title,'_')) }}" class="a-title"><span class="fa fa-edit"></span> {{ $article->title }}</a>
                                        <div class="meta-data">
                                            <p class="">Last Updated on {{ $article->updated_at }}</p>
                                        </div>
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                   <div class="my-3">
                       <div class="card">
                           <div class="card-body">
                               <h5 class="card-title">Translate</h5>
                               <div id="google_translate_element"></div>
                               <script type="text/javascript">
                                   function googleTranslateElementInit() {
                                       new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
                                   }
                               </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

                           </div>
                       </div>
                   </div>

                    <div class="sign-in">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Log In</h5>
                                <form method="post" action="{{ route('login') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" name="username" id="username" value="{{ old('username') }}" required class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" autofocus>
                                        @if ($errors->has('username'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('username') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" id="password" value="{{ old('password') }}"  class="form-control  {{ $errors->has('password') ? 'is-invalid' : '' }}" required>
                                        @if ($errors->has('password'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('password') }}
                                            </div>
                                        @endif

                                    </div>

                                    <button type="submit" class="btn btn-primary">Login</button>
                                </form>
                            </div>
                        </div>
                        <div class="card my-3 quick-links">
                            <div class="card-body">
                                <h5 class="card-title">Quick Links</h5>
                                <div class="row">
                                    <div class="col-6">
                                        <a href="{{url('/')}}"><span class="fa fa-link"></span> Home</a>
                                    </div>
                                    <div class="col-6">
                                        <a href="{{ url('/contact-us') }}"><span class="fa fa-link"></span> Contact Us</a>
                                    </div>
                                    <div class="col-6">
                                        <a href="#"><span class="fa fa-link"></span> Cultureversity</a>
                                    </div>
                                    <div class="col-6">
                                        <a href="http://mag.culturepediaonline.com"><span class="fa fa-link"></span> Culturepedia Magazine</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<footer class="text-center">
    <div class="links">
        <p> <a class="nav-link" href="{{ url('/contact-us') }}">Contact Us</a>  <a href="{{ url('privacy_policy') }}">Privacy Policy</a> | <a href="{{ url('terms_of_use') }}">Terms of Use</a> </p>
    </div>
    <div class="copyright">
        <p>&copy; {{ date('Y') }} <a href="{{ url('/') }}">Culturepedia </a> | Powered by <a href="http://springlight.ng">Springlight Technology</a> </p>
    </div>
</footer>
@section('scripts')
    <script type="text/javascript" src="{{  asset('assets/js/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/typeahead.bundle.js') }}"></script>
@show
<script type="text/javascript" src=" {{ asset('assets/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript">
    (()=>{
        $(document).ready(function(){
            $(".owl-carousel").owlCarousel({
                items:1,
                autoplay:true,
                animateOut: 'fadeOut',
                autoplayTimeout:3000,
                rewind:true
            });
        });
    })();
</script>
</body>
</html>



