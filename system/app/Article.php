<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $primaryKey = "article_id";

    public function videos(){
        return $this->hasMany('App\Video','article_id');
    }

    public function pictures(){
        return $this->hasMany('App\Picture','article_id');
    }


    public function getUpdatedAtAttribute($timestamp) {
        $date = new \DateTime($timestamp);
            $date = $date->format('F d, Y');
        return $date;
    }

    public function  getCreatedAtAttribute($timestamp){
        $date = new \DateTime($timestamp);
        $date = $date->format('F d, Y');
        return $date;
    }
}
