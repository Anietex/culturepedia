(() => {
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            items: 1,
            autoplay: true,
            animateOut: 'fadeOut',
            autoplayTimeout: 3000,
            rewind: true
        });
    });
})();

$('a').smoothScroll({
    easing: 'swing',
    speed: 600
});

$(window).scroll(fixedSidebar);

function fixedSidebar() {
    let sidebar = $("#sidebar");
    let article = $("#article");
    let sidebarTop = sidebar.get(0).getBoundingClientRect().top;
    let articleTop = article.get(0).getBoundingClientRect().top;
    console.log(articleTop);
    if (articleTop < 1) {
        sidebar.addClass('fixed');
        article.addClass('shift-left');
    } else {
        sidebar.removeClass('fixed');
        article.removeClass('shift-left');
    }
}
