<nav class="navbar navbar-expand-lg navbar-dark bg-light">

    <div class="container">
        <a class="navbar-brand" href="{{url('/')}}">
            <img src="{{ asset('assets/images/logo.png') }}" width="90px" class="mr-2" alt="Culturepedia">
            </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse  navbar-collapse" id="navbarTogglerDemo02">



            <ul class="navbar-nav ml-auto mt-2 mt-lg-0 ">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/') }}">Home</a></li>
                <li class="nav-item ">
                    <a class="nav-link" href="{{ url('/articles') }}">All Articles</a></li>
                <li class="nav-item ">
                    <a class="nav-link" href="{{ url('/article/random') }}">Random Article</a></li>


                <li class="nav-item ">
                    <a class="nav-link" href="http://versity.culturepediaonline.com">Cultureversity</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="http://mag.culturepediaonline.com">Culturepedia Magazine</a>
                </li>
                @if(Auth::guest())
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ route('login') }}">Log in</a></li>
                    <li class="nav-item "><a class="nav-link" href="{{ route('register') }}">Register</a></li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/user') }}">My Account</a></li>
                    <li class="nav-item"><a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Log Out</a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                @endif

            </ul>
        </div>
    </div>
</nav>