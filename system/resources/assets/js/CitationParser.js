export class CitationParser {

    /**
     * Parses an HTML element that contains information for
     * a web citation
     *
     *
     * @param elm
     * @returns {{type: string, url: string, title: string, firstName: string, lastName: string, dateRetrieved: string}}
     */
    static parseWeb(elm){
        let citation = {
            type:'web',
            url:'',
            title:'',
            firstName:'',
            lastName:'',
            dateRetrieved:''
        };
        let authorElm = elm.querySelector('.author-name');

        if(authorElm){
            let authorText = authorElm.innerText.replace(/[,.]/g,'').trim();
            let name = authorText.split(' ');

            if(name.length>1){
                citation.lastName = name[0].trim();
                citation.firstName = name[1];
            }else {
                citation.firstName = authorText
            }
        }



        let title = elm.querySelector('.title a');

        if(title){
            citation.title = title.innerText.replace(/[.]/g,'').trim();
            citation.url = title.href;
        }


        let date = document.querySelector('.date-retrieved');
        if(date){
            let dr = date.innerText.replace(/[.]/g,'').trim().split(' ');
            if(dr.length>1){
                citation.dateRetrieved = dr.slice(1,4).join(' ');
            }
        }

        // console.log(citation);

        return citation;
    }


    /**
     *Parses an HTML element that contains information for
     * a book citation
     * @param elm
     * @returns {{type: string, title: string, firstName: string, lastName: string, publisher: string, publisherLocation: string, pages: string}}
     */
    static parseBook(elm){

        let citation = {
            type:'book',
            title:'',
            firstName:'',
            lastName:'',
            publisher:'',
            publisherLocation:'',
            publicationYear:'',
            pages:''
        };


        let authorElm = elm.querySelector('.author-name');

        if(authorElm){
            let authorText = authorElm.innerText.trim().replace(',','');
            let name = authorText.split(' ');
            if(name.length>1){ // check if there are both first and last name
                citation.lastName = name[0].trim().replace(',','');
                citation.firstName = name[1].replace(',','');
            }else {
                citation.firstName = authorText
            }
        }



        let titleElm = elm.querySelector('.title i');

        if(titleElm){
            citation.title = titleElm.innerText.trim().replace(',','');
        }


        let pubElm = elm.querySelector('.publisher');
        if(pubElm){
            citation.publisher = pubElm.innerText.trim().replace(',','')
        }


        let pubLocElm = elm.querySelector('.publisher-location');
        if(pubLocElm){
            citation.publisherLocation = pubLocElm.innerText.trim().replace(':','');
        }


        let pubYearElm = elm.querySelector('.publication-year');
        if(pubYearElm){
            citation.publicationYear = pubYearElm.innerText.trim().replace('.','');
        }

        let pageElm = elm.querySelector('.pages .page-nums');

        if(pageElm){
            citation.pages = pageElm.innerText.trim().replace('.','');
        }





        return citation;


    }

    /**
     * Parses an HTML element that contains information for
     *  news citation
     *
     * @param elm
     * @returns {{type: string, url: string, title: string, firstName: string, lastName: string, publicationName: string, accessDate: string, sourceDate: string}}
     */

    static parseNews(elm){
        let citation = {
            type:'news',
            url:'',
            title:'',
            firstName:'',
            lastName:'',
            publicationName:'',
            accessDate:'',
            // sourceDate:''
        };





        let authorElm = elm.querySelector('.author-name');

        if(authorElm){
            let authorText = authorElm.innerText.trim();
            let name = authorText.replace(',','').split(' ');
            if(name.length>1){ // check if there are both first and last name
                citation.lastName = name[0].replace(',','').trim();
                citation.firstName = name[1];
            }else {
                citation.firstName = authorText
            }
        }


        let title = elm.querySelector('.title a');



        if(title){
            citation.title = title.innerText.replace(/[,]/,'');
            citation.url = title.href;
        }else {
            title = elm.querySelector('.title');
            citation.title = title.innerText.replace(/[,]/,'').trim();
        }




        let sdElm = elm.querySelector('.date-retrieved');

        if(sdElm){

            citation.accessDate = sdElm.innerText.trim().split(' ').slice(1,4).join(' ');
        }


        let pubElm = elm.querySelector('.publisher');
        if(pubElm){
            citation.publicationName = pubElm.innerText.trim().replace(',','')
        }




        return citation;


    }


    /**
     * Parses an HTML element that contains information for
     * a journal citation
     * @param elm
     * @returns {{type: string, url: string, title: string, firstName: string, lastName: string, sourceDate: string, volume: string, pages: string}}
     */

    static parseJournal(elm){
        let citation = {
            type:'journal',
            url:'',
            title:'',
            firstName:'',
            lastName:'',
            sourceDate:'',
            volume:'',
            pages:''
        };


        let authorElm = elm.querySelector('.author-name');

        if(authorElm){
            let authorText = authorElm.innerText.replace(/[,"]/g,'').trim();
            let name = authorText.split(' ');

            if(name.length>1){
                citation.lastName = name[0].replace(',','').trim();
                citation.firstName = name[1];
            }else {
                citation.firstName = authorText
            }
        }


        let title = elm.querySelector('.title a');

        if(title){
            citation.title = title.innerText.replace(/[,"]/g,'').trim();
            citation.url = title.href;
        }else {
            title = elm.querySelector('.title');
            citation.title = title.innerText.replace(/[,"]/g,'').trim();
        }


        let pageElm = elm.querySelector('.pages .page-nums');

        if(pageElm){
            citation.pages = pageElm.innerText.replace(/,$/g,'').trim();
        }


        let sdElm = elm.querySelector('.source-date');

        if(sdElm){
            citation.sourceDate = sdElm.innerText.trim();
        }


        let volElm = elm.querySelector('.volume');

        if(volElm){
            citation.volume = volElm.innerText.replace(/[,"]/g,'').trim();
        }



        let journal = elm.querySelector('.journal-title');
        if(journal){
            citation.journal = journal.innerText.replace(/[,"]/g,'').trim();
        }

        // console.log(citation);
        return citation;

    }
}