<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleMedia extends Model
{
    protected $table = "articles_media";
    protected $primaryKey = "media_id";

    const UPDATED_AT = null;
    const CREATED_AT = null;

}
