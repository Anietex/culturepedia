 export class MediaHandler{

    constructor(){
        this.index =1;
        this.vidIndex = 0;
        this.picIndex = 0;
        this.addRemoveListener();
    }


    addEventListener(){

        this.pictureBtn.click(event=>{
            this.showPictureDialog(event);
        });

        this.pictureInput.change(event=>{
            this.handlePictures(event);
        });


        this.videoBtn.click(event=>{
            this.showVideoDialog(event);
        })

        this.videoInput.change(event=>{
            this.handleVideos(event);
        })






    }

    getElements(){
        this.pictureBtn = $("#picture-btn");
        this.pictureInput = $("#picture-input");
        this.videoBtn = $("#video-btn");
        this.videoInput = $("#video-input");

        this.uploadBtn = $(".upload-btn");
        this.uploadInput =$('#media-1')
    }
    showPictureDialog(){
        this.pictureInput.get(0).click();
    }

    showVideoDialog(){
        this.videoInput.get(0).click();
    }



    showFileDialog(){
        this.uploadInput.get(0).click();
    }

    addRemoveListener(){
        $(".close-btn").click(function (e) {
            $(this).parent().remove();
        });
    }


    handlePictures(event){
        let files = event.target.files;
        let _this = this;
        for(const file of files){
            let reader = new FileReader();
            reader.onload = event =>{
                let element = this.createImageThumbnail(event.target.result);
                $("#picture-files").prepend(element);
                this.addRemoveListener();
                this.attachPicToInput(files);
                // this.attachFileToInput(files)
            };
            reader.readAsDataURL(file)
        }
    }





    handleVideos(event)
    {
        let files = event.target.files;
        for(const file of files){
            let reader = new FileReader();
            reader.onload = event =>{
                let element = this.createVideoThumbnail(event.target.result);
                $("#video-files").prepend(element);
                this.addRemoveListener();
                this.attachVideoToInput(files);
                //this.attachFileToInput(files)
            };
            reader.readAsDataURL(file)
        }
    }

    handleFiles(event)
    {
        let files = event.target.files;
        let _this = this;
        for(const file of files){
            let reader = new FileReader();
            reader.onload = event =>{
                let element = this.createImageThumbnail(event.target.result);
                $("#media-files").prepend(element);
                this.addRemoveListener();
                this.attachFileToInput(files)
            };
            reader.readAsDataURL(file)
        }

    }



    createImageThumbnail(src){
        return `<div class="uploaded-media">
                   <span class="fa fa-close close-btn" ></span>
                    <img src="${src}" class="img-fluid">
                     <input type="text" name="pictures_description[]" class="img-description" placeholder="Picture description">
                    <input type="file" name="pictures[]" class="form-control media-input"  id="picture-${++this.picIndex}" accept="video/*,image/*">
                </div>`;
    }

    createVideoThumbnail(src){
        return `<div class="uploaded-media">
                   <span class="fa fa-close close-btn" ></span>
                    <video src="${src}" class="img-fluid"></video>
                     <input type="text" name="videos_description[]" class="img-description" placeholder="Video description">
                    <input type="file" name="videos[]" class="form-control media-input"  id="video-${++this.vidIndex}" accept="video/*,image/*">
                </div>`;
    }


    attachVideoToInput(files){
        $(`#video-${this.vidIndex}`).get(0).files = files;
    }


    attachPicToInput(files){
        $(`#picture-${this.picIndex}`).get(0).files = files;
    }



    attachFileToInput(files){
        $(`#media-${this.index}`).get(0).files = files;
    }

}