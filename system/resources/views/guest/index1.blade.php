<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">

    <title>Culturepedia | Welcome</title>
</head>
<body>
<header>
    <nav class="container">
        <div class="navbar">
            <div class="logo d-flex">
                <h1>Culture<span class="colored">pedia</span></h1>
            </div>
            <div class="menu-icon d-sm-none" id="dropdown-toggle">
                <span class="fa fa-bars"></span>
            </div>
            <div class="links" id="dropdown">
                <ul class="list-unstyled">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li><a href="{{ url('/articles') }}">All Articles</a></li>
                    <li><a href="{{ url('/article/featured') }}">Featured Article</a></li>
                    <li><a href="{{ url('/article/random') }}">Random Article</a></li>
                    @if(Auth::guest())
                        <li><a href="{{ route('login') }}">Log in</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    @else
                        <li><a href="{{ url('/user') }}">My Account</a></li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Log Out</a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
</header>

<main class="main">
    <section class="home-banner">
    <div class="overlay">
        <div class="container">
            <div class="wrapper text-center">
                <h1>Welcome to <span class="text-uppercase title">Culturepedia</span></h1>
                <div class="brief">
                    <p>Thought happens when you hear peace so authoratively that whatsoever you are easing is your attraction.
                        Things, yogis, and true suns will always protect them. Lobster combines greatly with crusted blueberries.
                    </p>
                </div>
                <div class="search">
                   <form action="{{ url('/search') }}" method="get">

                       <div id="article-search">
                           <input type="search" name="query" class="search-box search" placeholder="Search for different cultures facts ...">
                       </div>


                       <button>Search</button>
                   </form>
                </div>
            </div>
        </div>
    </div>

    <div class="slider">
        <div class="owl-carousel">
            <div class="slide1 slide"></div>
            <div class="slide2 slide"></div>
            <div class="slide3 slide"></div>
            <div class="slide4 slide"></div>
            <div class="slide5 slide"></div>
        </div>
    </div>
    </section>
    <section class="home-articles">
        <div class="container">
            <div class="articles my-3">
                <div class="inner-wrapper">
                    @foreach($articles as $article)

                        <div class="article">
                            <div class="heading">
                                <h1 class="text-capitalize"><a href="{{ url("/article/$article->article_id/".str_slug($article->title,'_')) }}">{{ $article->title }}</a></h1>
                                <div class="meta">
                                    <p  class="text-uppercase font-weight-bold">Last Updated on {{ $article->updated_at }}</p>
                                </div>
                            </div>
                            <div class="excerpt"  id="excerpt">
                                <p >{!!  $article->tribe !!}</p>
                            </div>

                            <div class="read-btn mt-4">
                                <a href="{{ url("/article/$article->article_id/".str_slug($article->title,'_')) }}" class="btn btn-outline-primary">Read More</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>


</main>
<footer class="text-center">
    <div class="links">
        <p><a href="{{ url('privacy_policy') }}">Privacy Policy</a> | <a href="{{ url('terms_of_use') }}">Terms of Use</a> </p>
    </div>
    <div class="copyright">
        <p>&copy; {{ date('Y') }} <a href="{{ url('/') }}">Culturepedia </a> | Powered by <a href="http://springlight.ng">Springlight Technologies</a> </p>
    </div>
</footer>
<script type="text/javascript" src="{{  asset('assets/js/jquery-3.3.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/typeahead.bundle.js') }}"></script>
<script type="text/javascript" src="{{  asset('assets/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript">
        (()=>{
            $(document).ready(function(){
                $(".owl-carousel").owlCarousel({
                    items:1,
                    autoplay:true,
                    animateOut: 'fadeOut',
                    autoplayTimeout:3000,
                    rewind:true
                });
            });
        })();
    </script>
</body>
</html>
