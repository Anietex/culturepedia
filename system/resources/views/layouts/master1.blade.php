<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @section('styles')
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
    @show
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">

    <title>Culturepedia  |  @yield('title')</title>
</head>
<body>
<header>
    <nav class="container">
        <div class="navbar">
            <div class="logo d-flex">
               <h1>Culture<span class="colored">pedia</span></h1>
            </div>
            <div class="menu-icon d-sm-none" id="dropdown-toggle">
                <span class="fa fa-bars"></span>
            </div>
            <div class="links" id="dropdown">
                <ul class="list-unstyled">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li><a href="{{ url('/articles') }}">All Articles</a></li>
                    <li><a href="{{ url('/article/featured') }}">Featured Article</a></li>
                    <li><a href="{{ url('/article/random') }}">Random Article</a></li>
                    @if(Auth::guest())
                    <li><a href="{{ route('login') }}">Log in</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                    @else
                        <li><a href="{{ url('/user') }}">My Account</a></li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">Log Out</a></li>
                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
</header>
<section class="subheader">
   <div class="container">
      <div class="sub-wrapper d-flex flex-column-reverse flex-sm-row flex-lg-row justify-content-sm-between" >
          <div class="text">
              <p> @yield('section-title')</p>
          </div>
          <div class="search">
              <div class="seearch-wrapper">
                  <form action="{{ url('/search') }}" method="get" id="search-form">
                      <div id="article-search">
                          <input type="search" name="query" class="search" placeholder="Search articles ...">
                      </div>
                  </form>
              </div>
          </div>

      </div>
   </div>
</section>
<main class="main">
    <section class="content">
        @yield('content')
    </section>
</main>
<footer class="text-center">
   <div class="links">
       <p><a href="{{ url('privacy_policy') }}">Privacy Policy</a> | <a href="{{ url('terms_of_use') }}">Terms of Use</a> </p>
   </div>
    <div class="copyright">
        <p>&copy; {{ date('Y') }} <a href="{{ url('/') }}">Culturepedia </a> | Powered by <a href="http://springlight.ng">Springlight Technologies</a> </p>
    </div>
</footer>
@section('scripts')
<script type="text/javascript" src="{{  asset('assets/js/jquery-3.3.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/typeahead.bundle.js') }}"></script>
@show
<script type="text/javascript" src=" {{ asset('assets/js/custom.js') }}"></script>
</body>
</html>