<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
   use ResetsPasswords;

   protected $redirectTo = 'admin/dashboard';


   public function __construct()
   {
       $this->middleware('guest');
   }


}
