@extends('admin.master')
@section('styles')
    @parent
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/editor.css') }}">
@endsection
@section('title','Edit Article')
@section('header-title')
    <h1><span class="fa fa-pencil"></span> Edit Article <small>Make changes</small></h1>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Article</li>
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            Edit Article
        </div>
        <div class="card-body">
            <div class="article-form">
                <div class="wrapper">
                    @if ($errors->any())
                        <div class="alert alert-danger  alert-dismissible fade show" role="alert">
                            @foreach ($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @if(session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <p>{{ session('success') }}</p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @if(session('error'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <p>{{ session('error') }}</p>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <form action="{{ url('admin/article/'.$article->article_id) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="title">Article title</label>
                            <input type="text" name="title" id="title" class="form-control input-lg" placeholder="Article title" value="{{ $article->title }}">
                        </div>
                        <label for="article"><span  id="heading"></span> </label>
                        <div class="form-group" id="text-pane">
                            <div id="editor"></div>
                        </div>

                        <div class="upload-pictures upload-pane" id="picture-pane">
                            <label>Upload Pictures </label>
                            <small>Maximum file size of 500KB</small>
                            <div class="media-files pictures d-flex flex-wrap" id="picture-files">

                                @foreach($pictures as $picture)
                                    <div class="uploaded-media">
                                        <span class="fa fa-close close-btn" ></span>
                                        <img src="{{ asset('storage/media/pictures/'.$picture->file_name)}}" class="img-fluid">
                                        <input type="text" name="old_pic_description[]" class="img-description" placeholder="Media description" value="{{ $picture->description }}">
                                        <input type="hidden" name="old_pictures[]" class="form-control media-input"   value="{{ $picture->picture_id }}" >
                                    </div>
                                @endforeach
                                <div class="upload-btn">
                                    <div class="add-media add-media-btn">
                                        <button type="button" class="fa fa-plus fa-2x" data-input="#media-1" id="picture-btn"></button>
                                    </div>
                                    <input type="file"  class="form-control media-input"  id="picture-input" accept="image/*">
                                </div>
                            </div>
                        </div>


                        <div class="upload-videos upload-pane" id="video-pane">

                            <label>Upload short Videos</label>
                            <small>Maximum file size of 3.5MB</small>
                            <div class="media-files pictures d-flex flex-wrap" id="video-files">

                                @foreach($videos as $video)
                                    <div class="uploaded-media">
                                        <span class="fa fa-close close-btn" ></span>
                                        <video src="{{  asset('storage/media/videos/'.$video->file_name)}}" controls class="img-fluid"></video>
                                        <input type="text" name="old_vid_description[]" class="img-description" placeholder="Video description">
                                        <input type="hidden" name="old_videos[]" class="form-control media-input" value="{{ $video->video_id }}">
                                    </div>
                                @endforeach

                                <div class="upload-btn">
                                    <div class="add-media add-media-btn">
                                        <button type="button" class="fa fa-plus fa-2x" id="video-btn"></button>
                                    </div>
                                    <input type="file"  name="" class="form-control media-input"  id="video-input" accept="video/*">
                                </div>
                            </div>
                        </div>


                        <div class="inputs">
                            <input type="hidden"  name="tribe" value="{{ $article->tribe }}">
                            <input type="hidden" name="region" value="{{ $article->region }}">
                            <input type="hidden" name="country" value="{{ $article->country }}">
                            <input type="hidden" name="language" value="{{ $article->language }}">
                            <input type="hidden" name="geo-description" value="{{ $article->geo_description }}">
                            <input type="hidden" name="history" value="{{ $article->history }}">
                            <input type="hidden" name="literature" value="{{ $article->literature }}">
                            <input type="hidden" name="tradition" value="{{ $article->tradition }}">
                            <input type="hidden" name="leadership" value="{{ $article->leadership }}">
                            <input type="hidden" name="social-institution" value="{{ $article->social_institution }}">
                            <input type="hidden" name="religion" value="{{ $article->religion }}">
                            <input type="hidden" name="festivals" value="{{  $article->festivals }}">
                            <input type="hidden" name="occupation" value="{{ $article->occupations }}">
                            <input type="hidden" name="technology" value="{{ $article->technology }}">
                            <input type="hidden" name="clothing" value="{{ $article->clothing }}">
                            <input type="hidden" name="food" value="{{ $article->food }}">
                            <input type="hidden" name="tourist-attraction" value="{{ $article->tourist_attraction}}">
                            <input type="hidden" name="other-traditions" value="{{ $article->other_traditions }}">
                            <input type="hidden" name="references" value="{{ $article->article_references }}">
                        </div>

                        <div class="action-buttons d-flex justify-content-between">
                            <button type="button" class="btn btn-primary" id="prev-btn">Previous</button>
                            <button type="button" class="btn btn-primary nxt-btn" id="next-btn" >Next</button>
                            <button type="submit" class="btn btn-primary" id="post">Update</button>
                        </div>

                        <div class="d-none" id="citations">
                            <ol id="references">
                                {!! $article->article_references !!}
                            </ol>
                        </div>


                        <div class="d-none" id="fake-editor">
                            <div class="d-none" id="tribe"> {!! $article->tribe !!} </div>
                            <div class="d-none" id="region"> {!! $article->region !!}</div>
                            <div class="d-none" id="country">{!! $article->country!!}</div>
                            <div class="d-none" id="language">{!! $article->language!!}</div>
                            <div class="d-none" id="geo-description">{!! $article->geo_description !!}</div>
                            <div class="d-none" id="history"> {!! $article->history !!}</div>
                            <div class="d-none" id="literature">{!! $article->literature !!}</div>
                            <div class="d-none" id="tradition"> {!! $article->tradition !!}</div>
                            <div class="d-none" id="leadership"> {!! $article->leadership!!}</div>
                            <div class="d-none" id="social-institution">{{ $article->social_institution }}</div>
                            <div class="d-none" id="religion"> {!! $article->religion!!}</div>
                            <div class="d-none" id="festivals">{!!$article->festivals !!}</div>
                            <div class="d-none" id="occupation">{!! $article->occupations !!}</div>
                            <div class="d-none" id="technology"> {!! $article->technology!!}</div>
                            <div class="d-none" id="clothing">{!!$article->clothing!!}</div>
                            <div class="d-none" id="food">{!! $article->food !!}</div>
                            <div class="d-none" id="tourist-attraction">{!! $article->tourist_attraction !!}</div>
                            <div class="d-none" id="other-traditions">{!!  $article->other_traditions !!}</div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.26.0/polyfill.js"></script>
    {{--<script type="text/javascript" src=" {{ asset('assets/js/admin-article.js') }}" ></script>--}}
    {{--<script type="text/javascript" src="{{ asset('assets/js/editor.bundle.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('assets/js/admin-editor.bundle.js') }}"></script>
@endsection