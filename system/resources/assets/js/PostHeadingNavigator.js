import {CitationCreator} from "./CitationCreator";

export class   PostHeadingNavigator{

    constructor(editor){
        this.editor = editor;
        this.headings = this.getPostHeadings();
        this.active = 0;
        this.nextBtn =null;
        this.prevBtn =null;
        this.textArea =null;
        this.title =null;
        this.helpTex =null;

        this.updateObject();

        this.editor.linkAddedListener = ()=>{

            this.headings[this.active].textBoxValue = this.textArea.val();
            this.linkUpdated();
            this.updateFakeEditor();
            this.updateInputs();
            $('input[name="references"]').val($('#references').html());


        };

        this.editor.onUpdated = ()=>{
            this.headings[this.active].textBoxValue = this.textArea.val();
            this.updateInputs();
        };


    }


    getElements(){
        this.nextBtn = $("#next-btn");
        this.prevBtn =$("#prev-btn");
        this.textArea=$("#editor-text");
        this.title = $("#heading");
        this.helpTex =$("#help-text");
        this.picturePane = $('#picture-pane');
        this.videoPane = $('#video-pane');
        this.uploadPane = $("#upload-pane");
        this.textPane = $("#text-pane");
        this.mediaIput = $(".media-input");
        this.postBtn = $("#post");
    }


    extendTextarea(){
        this.textArea.val = (val)=>{
            if(val ===undefined)
                return this.textArea.get(0).innerHTML;
            this.textArea.get(0).innerHTML = val
        }


    }

    registerEvents(){
        this.nextBtn.click((event)=>{
            this.nextHeading();
        });

        this.prevBtn.click(event=>{
            this.previousHeading()
        });

        this.textArea.keyup(event=>{
            this.headings[this.active].textBoxValue = this.textArea.val();
            this.updateInputs();
            this.updateFakeEditor();
        });

        this.mediaIput.change(event=>{
            this.handleChoseFile();
        })

    }
    handleChoseFile(){
        if(this.mediaIput.val()!=''){
            this.uploadPane.append("")
        }
    }




    nextHeading(){
        if(this.active<this.headings.length){
            this.active+=1;
            this.updateElement();
            this.updateFakeEditor();
             this.editor.hidePopovers();
        }

    }

    previousHeading(){
        if(this.active>0){
            this.active-=1;
            this.editor.currentPane=  this.active;
            this.updateElement();
            this.updateFakeEditor();
            this.editor.hidePopovers();
        }

    }

    updateObject(){
        this.headings[0].textBoxValue = $('input[name="tribe"]').val();
        this.headings[1].textBoxValue = $('input[name="region"]').val();
        this. headings[2].textBoxValue = $('input[name="country"]').val();
        this.headings[3].textBoxValue = $('input[name="language"]').val();
        this.headings[4].textBoxValue = $('input[name="geo-description"]').val();
        this.headings[5].textBoxValue = $('input[name="history"]').val();
        this.headings[6].textBoxValue = $('input[name="literature"]').val();
        this. headings[7].textBoxValue = $('input[name="tradition"]').val();
        this. headings[8].textBoxValue = $('input[name="leadership"]').val();
        this. headings[9].textBoxValue = $('input[name="social-institution"]').val();
        this. headings[10].textBoxValue = $('input[name="religion"]').val();
        this. headings[11].textBoxValue = $('input[name="festivals"]').val();
        this.headings[12].textBoxValue = $('input[name="occupation"]').val();
        this. headings[13].textBoxValue = $('input[name="technology"]').val();
        this.headings[14].textBoxValue = $('input[name="clothing"]').val();
        this. headings[15].textBoxValue = $('input[name="food"]').val();
        this. headings[16].textBoxValue = $('input[name="tourist-attraction"]').val();
        this. headings[17].textBoxValue = $('input[name="other-traditions"]').val();
    }

    updateInputs(){

        switch (this.active) {
            case 0:
                $('input[name="tribe"]').val(this.headings[0].textBoxValue);
                break;
            case 1:
                $('input[name="region"]').val( this.headings[1].textBoxValue);
                break;
            case 2:
                $('input[name="country"]').val( this.headings[2].textBoxValue);
                break;
            case 3:
                $('input[name="language"]').val(this.headings[3].textBoxValue);
                break;
            case 4:
                $('input[name="geo-description"]').val(this.headings[4].textBoxValue);
                break;
            case 5:
                $('input[name="history"]').val(this.headings[5].textBoxValue);
                break;
            case 6:
                $('input[name="literature"]').val(this.headings[6].textBoxValue);
                break;
            case 7:
                $('input[name="tradition"]').val( this.headings[7].textBoxValue);
                break;
            case 8:
                $('input[name="leadership"]').val( this.headings[8].textBoxValue);
                break;
            case 9:
                $('input[name="social-institution"]').val(this.headings[9].textBoxValue);
                break;
            case 10:
                $('input[name="religion"]').val(this.headings[10].textBoxValue);
                break;
            case 11:
                $('input[name="festivals"]').val(this.headings[11].textBoxValue);
                break;
            case 12:
                $('input[name="occupation"]').val( this.headings[12].textBoxValue);
                break;
            case 13:
                $('input[name="technology"]').val(this.headings[13].textBoxValue);
                break;
            case 14:
                $('input[name="clothing"]').val(this.headings[14].textBoxValue);
                break;
            case 15:
                $('input[name="food"]').val( this.headings[15].textBoxValue );
                break;
            case 16:
                $('input[name="tourist-attraction"]').val(this.headings[16].textBoxValue);
                break;
            case 17:
                $('input[name="other-traditions"]').val( this.headings[17].textBoxValue );

        }

    }


    fakeObjectUpdate(){
        let links =  $('#fake-editor').get(0).querySelectorAll('.cite-link');
        this.headings[0].textBoxValue = $('#tribe').html();
        this.headings[1].textBoxValue = $('#region').html();
        this. headings[2].textBoxValue = $('#country').html();
        this.headings[3].textBoxValue = $('#language').html();
        this.headings[4].textBoxValue = $('#geo-description').html();
        this.headings[5].textBoxValue = $('#history').html();
        this.headings[6].textBoxValue = $('#literature').html();
        this. headings[7].textBoxValue = $('#tradition').html();
        this. headings[8].textBoxValue = $('#leadership').html();
        this. headings[9].textBoxValue = $('#social-institution').html();
        this. headings[10].textBoxValue = $('#religion').html();
        this. headings[11].textBoxValue = $('#festivals').html();
        this.headings[12].textBoxValue = $('#occupation').html();
        this. headings[13].textBoxValue = $('#technology').html();
        this.headings[14].textBoxValue = $('#clothing').html();
        this. headings[15].textBoxValue = $('#food').html();
        this. headings[16].textBoxValue = $('#tourist-attraction').html();
        this. headings[17].textBoxValue = $('#other-traditions').html();
    }


    linkUpdated(){
        let panes = Array.from(document.getElementById('fake-editor').children);

        panes.forEach((pane)=>{
            let fakeEditor = document.querySelector('#fake-editor');

            let ob = new MutationObserver(()=>{
                let links =  document.querySelector('#fake-editor').querySelectorAll('.cite-link');

                for(let i = 0; i<links.length; i++){
                    let id = links[i].id;
                     if(links[i].id.includes('fake'))
                        continue;
                    links[i].id = 'fake-'+links[i].id;

                }
                CitationCreator.onCitationUpdated(fakeEditor);
                ob.disconnect();
                this.fakeObjectUpdate();
                this.updateElement();
                this.editor.renameRefIDs();
            });

            ob.observe(pane,{ childList: true,subtree: true});
        });
    }


    updateFakeEditor(){

        switch (this.active){
            case 0:
                $('#tribe').html(this.headings[0].textBoxValue);
                break;
            case 1:
                $('#region').html(this.headings[1].textBoxValue);
                break;
            case 2:
                $('#country').html( this.headings[2].textBoxValue);
                break;
            case 3:
                $('#language').html(this.headings[3].textBoxValue);
                break;
            case 4:
                $('#geo-description').html(this.headings[4].textBoxValue);
                break;
            case 5:
                $('#history').html(this.headings[5].textBoxValue);
                break;
            case 6:
                $('#literature').html(this.headings[6].textBoxValue);
                break;
            case 7:
                $('#tradition').html( this.headings[7].textBoxValue);
                break;
            case 8:
                $('#leadership').html( this.headings[8].textBoxValue);
                break;
            case 9:
                $('#social-institution').html(this.headings[9].textBoxValue);
                break;
            case 10:
                $('#religion').html(this.headings[10].textBoxValue);
                break;
            case 11:
                $('#festivals').html(this.headings[11].textBoxValue);
                break;
            case 12:
                $('#occupation').html( this.headings[12].textBoxValue);
                break;
            case 13:
                $('#technology').html(this.headings[13].textBoxValue);
                break;
            case 14:
                $('#clothing').html(this.headings[14].textBoxValue);
                break;
            case 15:
                $('#food').html( this.headings[15].textBoxValue );
                break;
            case 16:
                $('#tourist-attraction').html(this.headings[16].textBoxValue);
                break;
            case 17:
                $('#other-traditions').html( this.headings[17].textBoxValue );


        }




    }



    updateElement(){
        if(this.active+1 >= this.headings.length){
            this.nextBtn.hide();
        }else{
            this.nextBtn.show();
        }

        if(this.active<=0){
            this.prevBtn.hide()
        }else {
            this.prevBtn.show();
        }

        if(this.active === this.headings.length-2) {
            this.title.text(this.headings[this.active].heading);
            this.helpTex.text(this.headings[this.active].description);
            this.textPane.hide();
            this.picturePane.show();
            this.videoPane.hide();
            this.postBtn.hide();
        }
        else if(this.active === this.headings.length-1){
            this.title.text(this.headings[this.active].heading);
            this.helpTex.text(this.headings[this.active].description);
            this.textPane.hide();
            this.picturePane.hide();
            this.videoPane.show();
            this.postBtn.show();
        }else{
            this.textPane.show();
            this.videoPane.hide();
            this.picturePane.hide();
            this.title.text(this.headings[this.active].heading);
            this.helpTex.text(this.headings[this.active].description);
            this.textArea.val(this.headings[this.active].textBoxValue);
            this.postBtn.hide();
            this.updateInputs();
        }

        this.editor.addLinkEventListener();




    }


    getPostHeadings(){

        return    [
            {
                heading:"Tribe/ Community",
                description:"On this heading you will write about the tribe/community that practice this culture",
                textBoxValue:""
            },
            {
                heading:"Region/ Province/ State",
                description:"On this heading you will write about the Region/Province/State that practice this culture",
                textBoxValue:""
            },
            {
                heading:"Country",
                description:"On this heading you will write about the Country in which this culture is prevalent",
                textBoxValue:""
            },
            {
                heading:"Language(s)",
                description:"On this heading you will write about the Language(s) popular in this culture",
                textBoxValue:""
            },
            {
                heading:"Landscape",
                description:"On this heading you will on  the Landscape of this culture",
                textBoxValue:""
            },
            {
                heading:"History and Mythology",
                description:"On this heading you will write about the History and Mythology of this culture",
                textBoxValue:""
            },
            {
                heading:"Literature",
                description:"On this heading you will write about the Literature of this culture",
                textBoxValue:""
            },
            {
                heading:"Music and Arts",
                description:"On this heading you will write about the culture\'s music and art",
                textBoxValue:""
            },
            {
                heading:"Politics and Leadership",
                description:"On this heading you will write about the Politics and Leadership of this culture",
                textBoxValue:""
            },
            {
                heading:"Social Institutions",
                description:"On this heading you will write about the Social Institutions of this culture",
                textBoxValue:""
            },
            {
                heading:"Religion and Rituals",
                description:"On this heading you will write about the Religion and Ritual perform by this culture",
                textBoxValue:""
            },
            {
                heading:"Festivals",
                description:"On this heading you will write about the festivals this culture",
                textBoxValue:""
            },
            {
                heading:"Occupations",
                description:"On this heading you will write about the  Occupations of this culture",
                textBoxValue:""
            },
            {
                heading:"Tools and Technologies",
                description:"On this heading you will write about the  Tools and Technologies of this culture",
                textBoxValue:""
            },
            {
                heading:"Clothing and Cosmetics",
                description:"On this heading you will write about the  Clothing and Cosmetics of this culture",
                textBoxValue:""
            },
            {
                heading:"Food and Food Processing",
                description:"On this heading you will write about the  Food and Food Processing  of this culture",
                textBoxValue:""
            },
            {
                heading:" Tourist Attractions",
                description:"On this heading you will write about the   Tourist Attractions  of this culture",
                textBoxValue:""
            },
            {
                heading:"Other traditions",
                description:"On this heading you will write about   other traditions  of this culture",
                textBoxValue:""
            },
            {
                heading:"Pictures",
                description:"Upload Picture about their festivals, food ,dressing etc.",
                files:[]
            },
            {
                heading:"Videos",
                description:"Upload Videos about their festivals, food ,dressing etc",
                files:[]
            },

        ];
    }
}