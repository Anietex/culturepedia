'use strict';

$('#dropdown-toggle').click(function () {
    $('#dropdown').slideToggle(200);
});

$('#excerpt a').each(function (i, e) {
    if (!$(e).hasClass('btn')) {
        $(e).removeAttr('href');
    }
});

$(window).scroll(fixedSidebar);

function fixedSidebar() {
    var sidebar = $("#sidebar");
    var article = $("#article");

    var sidebarTop = sidebar.get(0).getBoundingClientRect().top;
    var articleTop = article.get(0).getBoundingClientRect().top;
    //console.log(articleTop)
    if (articleTop < 1) {
        sidebar.addClass('fixed');
        article.addClass('shift-left');
    } else {
        sidebar.removeClass('fixed');
        article.removeClass('shift-left');
    }
}

$.get('/api/articles', function (data) {

    var articles = function articles() {
        var articles = data;

        return function findMatches(q, cb) {
            var matches = [];
            var substrRegex = new RegExp(q, 'i');

            $.each(articles, function (i, article) {
                if (substrRegex.test(article.title)) {
                    matches.push(article);
                }
            });

            cb(matches);
        };
    };

    $('#article-search .search').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        name: 'articles',
        source: articles(),
        display: function display(article) {
            return article.title;
        },
        templates: {
            suggestion: function suggestion(data) {
                return '<div class="s-item"><a href=\'' + data.link + '\'>' + data.title + '</a></div>';
            }
        }
    });
});

var substringMatcher = function substringMatcher(strs) {
    return function findMatches(q, cb) {
        var matches, substringRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        var substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function (i, str) {
            if (substrRegex.test(str)) {
                matches.push(str);
            }
        });

        cb(matches);
    };
};

var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
