@extends('admin.master')
@section('styles')
    @parent
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/datatables/datatables.css') }}">
@endsection
@section('title','All Articles')
@section('page-title','All Articles')


@section('header-title')
    <h1><span class="fa  fa-users "></span> Users<small> Manage Users</small></h1>
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
    <li class="breadcrumb-item active" aria-current="page">Users</li>
@endsection



@section('content')
        <div class="card">
            <div class="card-header">
              Users
            </div>
            <div class="card-body">
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Joined On</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $loop->index+1 }}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
@endsection
@section('scripts')
    @parent
    <script type="text/javascript" src="{{  asset('assets/datatables/datatables.js') }}"></script>
    <script>
        $(document).ready( function () {
            $('.table').DataTable({
                autoFill: true
            });
        } );
    </script>
@endsection