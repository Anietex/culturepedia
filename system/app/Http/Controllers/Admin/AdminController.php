<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Article;
use App\ContactModel;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function __construct(Request $request)
    {
        $adminArticles = Article::where('user_id',1)->count();
        $allArticles = Article::where('published',true)->count();
        $unapproved = Article::where('approved',false)->where('published',true)->count();
        $approved = Article::where('approved',true)->where('published',true)->count();
        $users = User::count();
        $messages = Message::where('read',false)->count();

        \View::share('adminArticles',$adminArticles);
        \View::share('allArticles',$allArticles);
        \View::share('unapproved',$unapproved);
        \View::share('approved',$approved);
        \View::share('usersCount',$users);
        \View::share('unreadMsg',$messages);
        \View::share('auth_user','');

    }

    public function dashboard(){
        $data['articles'] = Article::where('published',true)->where('published',true)->orderBy('article_id','desc')->take(5)->get();
        $data['users'] = User::orderBy('created_at','desc')->orderBy('created_at','desc')->take(5)->get();
        $data['published'] = Article::where('published',true)->where('approved',true)->count();
        $data['totalArticles'] = Article::where('published',true)->count();
        $data['totalUsers'] = User::count();
        return view('admin.dashboard',$data);
    }



    public function articles(){

        $articles  = Article::where('user_id',1)->orderBy('article_id','desc')->get();
        return view('admin.admin_articles',['articles'=>$articles]);
    }


    public function createArticle(){
        return view('admin.create_article');
    }

    public function editArticle(Article $article){
        $pictures = Article::find($article->article_id)->pictures;






        $videos  = Article::find($article->article_id)->videos;
        return \View::make('admin.edit_article',['article'=>$article,'videos'=>$videos,'pictures'=>$pictures]);
    }

    public function allArticles(){
        $articles = Article::where('published',true)->orderBy('article_id','desc')->get();

       return \View::make('admin.all_articles')->with(['articles'=>$articles]);


    }

    public function unapprovedArticles(){
        $articles = Article::where('published',true)->where('approved',false)->orderBy('article_id','desc')->get();
        return \View::make('admin.unapproved_article')->with(['articles'=>$articles]);
    }


    public function approvedArticles(){
        $articles = Article::where('published',true)->where('approved',true)->orderBy('article_id','desc')->get();
        return \View::make('admin.approved_article')->with(['articles'=>$articles]);
    }


    public function users(){
        $data['users'] = User::orderBy('created_at','desc')->orderBy('created_at','desc')->get();
        return view('admin.users')->with($data);
    }


    public function updateAccount(Request $request){
        $this->validate($request,['username'=>'required',
                                    'email'=>'required']);
        //var_dump(auth()->user()->user_id);
        $admin = Admin::find(auth()->user()->id);
        $admin->username = $request->input('username');
        $admin->email  = $request->input('email');
        $admin->save();
        auth()->login($admin);
        return redirect(url()->previous())->with('au_success','Account was successfully updated');
    }


    public function updatePassword(Request $request){
        $this->validate($request,['password'=>'required|confirmed',
                                    'old_password'=>'required']);
        $admin = Admin::find(auth()->user()->id);

        if(auth()->attempt(['username'=>$admin->username,'password'=>$request->old_password])){
            $admin->password = bcrypt($request->password);
            $admin->save();
            return redirect(url()->previous())->with('au_success','Password changed successfully');
        }else{
            return redirect(url()->previous())->with('password_error','Invalid old Password');
        }

    }

    public function messages(){
        $messages = Message::orderBy('created_at','desc')->get();
        return view('admin/messages')->with(['messages'=>$messages]);
    }

    public function getMessage(Message $message){
        $message->read = true;
        $message->save();

        return response()->json($message);
    }

    public function deleteMessage($id){
        Message::destroy($id);
        return redirect(url()->previous())->with('delete_success','Message was successfully deleted');
    }
}
