'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ArticleViewer = function () {
    function ArticleViewer() {
        _classCallCheck(this, ArticleViewer);

        this.element = this.createElement();
        this.linkView = this.getLinkView();
    }

    _createClass(ArticleViewer, [{
        key: 'getLinkView',
        value: function getLinkView() {
            var lv = document.createElement('div');
            lv.classList.add('link-view');
            lv.id = "link-view";

            var header = function header() {
                var wrapper = document.createElement('div');
                wrapper.classList.add("header");
                var title = document.createElement('div');
                title.classList.add('title');
                var p = document.createElement('p');
                p.innerHTML = '<span class="fa fa-link"></span> <span id="pop-title"></span>';
                title.appendChild(p);
                wrapper.appendChild(title);
                return wrapper;
            };
            var links = function links() {
                var links = document.createElement('div');
                links.classList.add('links');
                links.innerHTML = '<div class="link-item">\n                   <h4><a id="viewed-link" href=""></a></h4>\n                   <p id="def-text"></p>\n               </div>';
                return links;
            };
            lv.appendChild(header());
            lv.appendChild(links());
            return lv;
        }
    }, {
        key: 'createElement',
        value: function createElement() {
            var wrapper = this.getWrapper();
            var inner = this.innerWrapper();
            inner.appendChild(this.getLinkView());
            wrapper.appendChild(inner);
            return wrapper;
        }
    }, {
        key: 'innerWrapper',
        value: function innerWrapper() {
            var innerWrapper = document.createElement('div');
            innerWrapper.classList.add('baloon');
            return innerWrapper;
        }
    }, {
        key: 'getWrapper',
        value: function getWrapper() {
            var wrapper = document.createElement('div');
            wrapper.id = "balloon";
            wrapper.classList.add('balloon-wrapper');
            return wrapper;
        }
    }, {
        key: 'attachElement',
        value: function attachElement() {

            document.body.appendChild(this.element);
        }
    }, {
        key: 'showViewLink',
        value: function showViewLink(link, text, title, def, event) {
            if (document.body.contains(this.element)) {
                document.body.removeChild(this.element);
            }
            this.attachElement();
            var defText = document.getElementById('def-text');
            var vlink = document.getElementById('viewed-link');
            var popTitle = document.getElementById('pop-title');
            if (link === '') popTitle.innerText = "Definition";else popTitle.innerText = "Link";

            defText.innerText = def;
            if (!def) defText.innerText = title;
            vlink.innerText = text;
            vlink.title = title;
            vlink.href = link;
            this.element.style.display = "block";
            document.querySelector("#link-view").style.display = "block";

            this.positionElement(this.element, event.pageX, event.pageY);
        }
    }, {
        key: 'hidePopover',
        value: function hidePopover() {
            if (document.body.contains(this.element)) {
                document.body.removeChild(this.element);
            }
        }
    }, {
        key: 'positionElement',
        value: function positionElement(element, x, y) {
            element.style.left = x - 100 + "px";
            element.style.top = y + 38 + "px";
        }
    }]);

    return ArticleViewer;
}();

(function () {
    var viewer = new ArticleViewer();
    var defTexts = $('.def-text');
    var links = $('.article-link');
    defTexts.mouseenter(function (event) {
        var elm = $(event.target);
        viewer.showViewLink('', elm.text(), '', elm.data('def'), event);
    });
    defTexts.mouseleave(function () {
        viewer.hidePopover();
    });
    links.mouseenter(function (event) {
        var elm = $(event.target);
        viewer.showViewLink(elm.attr('href'), elm.text(), elm.attr('title'), elm.data('def'), event);
    });
    links.mouseleave(function () {
        viewer.hidePopover();
    });
})();
