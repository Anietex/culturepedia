class Commands {

    static bold() {
        document.execCommand('bold', false, null)
    }

    static italic() {
        document.execCommand('italic', false, null)
    }

    static orderedList() {
        document.execCommand('insertOrderedList', false, null);
    }

    static unorderedList() {
        document.execCommand('insertUnorderedList', false, null)
    }

    static undo() {
        document.execCommand('undo', false, null);
    }

    static redo() {
        document.execCommand('redo', false, null)
    }

    static createLink(link, text, title, def) {
        if (def === '' && link === '#')
            return;
        if (link === '#' && def !== '') {
            document.execCommand('insertHTML', false, `<span class="def-text" data-def="${ def }">${text}</span>`)
        } else {
            document.execCommand('insertHTML', false, `<a href="${link}" title="${title}" class="article-link" target="_blank" data-def="${ def }">${text}</a>`)
        }
    }
}

class Editor {

    constructor(container){
        this.container = container;
    }



    getElements(){
        this.undoBtn = document.getElementById('undo');
        this.redoBtn = document.getElementById('redo');
        this.boldBtn = document.getElementById("bold");
        this.italicBtn = document.getElementById("italic");
        this.linkBtn = document.getElementById('link');
        this.listUlBtn = document.getElementById('unlist');
        this.listOlBtn = document.getElementById('orlist');
        this.indentBtn = document.getElementById('in-indent');
        this.outdentBtn = document.getElementById('red-indent');
        this.balloon = document.getElementById('balloon');
        this.editor = document.getElementById("editor-text");
        this.mX=0;
        this.mY =0;
        this.popOver = new EditorPopover();
    }

    init(){
        this.createEditor();
        this.getElements();
        this.editor.contentEditable = true;
        this.registerEvents();
        this.addLinkEventListener();

    }

    handleAddLink(){

        let sel;
        if(window.getSelection())
            sel = window.getSelection();
        else{
            sel = document.selection
        }

        let cor = getCaretPosition();
        this.mX = cor.x;
        this.mY = cor.y;
       // console.log(cor)
       //  let range = '';
        let textNode;
        let start;
        let end;
        let _this =this;

        if(this.mX !==0 || this.mY !==0){
            if(sel.anchorNode!==null){
                let selected =sel.toString();
                if(sel.toString()===''){

                    let pos = sel.focusOffset;
                    let text = sel.focusNode.textContent;
                    textNode  = sel.focusNode;
                    if(pos!==text.length) {
                        if (!/\s/.test(text.charAt(pos))) {
                            let index = text.indexOf(' ', pos);
                            pos = index === -1 ? text.length : index;
                        }
                    }

                    let preText = text.substr(0, pos);
                    range = sel.getRangeAt(0);

                    let words = preText.split(' ');
                    let lastWord = words[words.length - 1];
                    end = pos;
                    start = pos-lastWord.length;
                    range.setStart(textNode,start);
                    range.setEnd(textNode,end);
                    selected = sel.toString();
                }else{
                    textNode  = sel.focusNode;
                    start = sel.anchorOffset;
                    end = sel.toString().length+start;
                }

                this.popOver.showEditLink(selected,this.mX,this.mY,(reply)=>{

                    selectText();
                    let title = reply.external_url_title;
                    let url = reply.external_url_link;
                    let definition = reply.definition;
                    title = title===undefined||title===null || title === ''?'':title;
                    url   = url===undefined||url===null || url === ''?'#':url;
                    definition = definition===undefined||definition===null || definition === ''?'':definition;
                    Commands.createLink(url,selected,title,definition);
                    _this.addLinkEventListener();
                    _this.linkAddedListener()
                });

            }
        }

        function selectText() {
            let range = document.createRange();
            sel.removeAllRanges();

            range.selectNode(_this.editor);
            sel.addRange(range);
            range.setStart(textNode,start);
            range.setEnd(textNode,end);
        }
        function getCaretPosition() {
            let x = 0;
            let y = 0;
            let sel = window.getSelection();
            if(sel.rangeCount) {
                var range = sel.getRangeAt(0).cloneRange();
                if(range.getClientRects()) {
                    range.collapse(true);
                    var rect = range.getClientRects()[0];
                    if(rect) {
                        y = rect.top+$(window).scrollTop();
                        x = rect.left;
                    }
                }
            }
            return {
                x: x,
                y: y
            };
        }
    }

    registerEvents(){
        this.boldBtn.addEventListener('click',Commands.bold,false);
        this.italicBtn.addEventListener('click',Commands.italic,false);
        this.listOlBtn.addEventListener('click',Commands.orderedList);
        this.listUlBtn.addEventListener('click',Commands.unorderedList,false);
        this.undoBtn.addEventListener('click',Commands.undo,false);
        this.redoBtn.addEventListener('click',Commands.redo,false);
        this.editor.addEventListener('mousedown',(event)=>{this.hideBalloon(event);},true);
        this.editor.addEventListener('keyup',()=>{this.hideBalloon()});
        this.linkBtn.addEventListener('click',(event)=>{this.handleAddLink()});
        this.addLinkEventListener();
    }

    showLinkInfoPopover(event){
        let anchor = event.target;
        let link = anchor.href;
        let text = anchor.innerText;
        let title = anchor.title;

         this.popOver.showViewLink(link,text,title,anchor.dataset.def,event);

    }


    hideBalloon(event){
       this.popOver.hidePopover();
    }

    addLinkEventListener(event){
        this.editorLinks = this.editor.querySelectorAll('a');
       let defTexts = this.editor.querySelectorAll('.def-text');

       for (const def of defTexts){

           def.addEventListener('mousedown',(event)=>{

               let fakeEvent = {
                   target:{
                       href:'',
                       innerText:def.innerText,
                       title:'',
                       dataset:def.dataset,
                   },
                   pageX: event.pageX,
                   pageY: event.pageY
               };
               this.showLinkInfoPopover(fakeEvent)});
       }

       for(const link of  this.editorLinks){
           if(link instanceof HTMLAnchorElement){
               link.addEventListener('mousedown',event=>{this.showLinkInfoPopover(event)})
           }
       }
    }

    createEditor(){
        let container = document.querySelector(this.container);


        
        function createActionBar() {
            return `
                <div class="action-bar">
            <button type="button" id="undo" title="Undo"><span class="fa fa-reply"></span></button>
            <button type="button" id="redo" title="Redo"><span class="fa fa-share"></span></button>
            <button type="button" id="bold" title="Bold Ctrl+B"> <span class="fa fa-bold" ></span></button>
            <button type="button"  id="italic" title="Italic"><span class="fa fa-italic" ></span></button>
            <button type="button" id="link" title="Link"><span class="fa fa-link" ></span></button>
            <button type="button" id="unlist"><span class="fa fa-list-ul" ></span></button>
            <button type="button"  id="orlist"><span class="fa fa-list-ol"></span></button>
            <!--<button type="button" id="in-indent"><span class="fa fa-indent" ></span></button>-->
            <!--<button type="button" id="red-indent"><span class="fa fa-outdent" ></span></button>-->
        </div>`;
        }

        function createEditorArea() {
            return `<div class="text-wrapper"><div class="text-area" id="editor-text"><span></span></div></div>`
        }

        let editorWrapper = document.createElement('div');
        let editor = document.createElement('div');
        editorWrapper.classList.add('editor-wrapper');
        editor.classList.add('editor');
        editor.innerHTML=createActionBar()+createEditorArea();

        editorWrapper.appendChild(editor);
        container.appendChild(editorWrapper);

    }

    setArticleJSON(articleJSON){
        this.popOver.setArticleJSON(articleJSON);
    }


}


class PopoverCreator{

    constructor(){

        this.element = this.createElement();
        this.linkView = this.getLinkView();
        this.linkEdit = this.getLinkEdit();
    }
    createElement(){
        let wrapper = this.getWrapper();
        let inner = this.innerWrapper();
        inner.appendChild(this.getLinkView());
        inner.appendChild(this.getLinkEdit());
        wrapper.appendChild(inner);
        return wrapper;
    }

    innerWrapper(){
        let innerWrapper = document.createElement('div');
        innerWrapper.classList.add('baloon');
        return innerWrapper;
    }


    getWrapper(){
        let wrapper = document.createElement('div');
        wrapper.id = "balloon";
        wrapper.classList.add('balloon-wrapper');
        return wrapper;

    }
    getLinkView(){
        let lv = document.createElement('div');
        lv.classList.add('link-view');
        lv.id = "link-view";

        let header = ()=>{
            let wrapper = document.createElement('div');
            wrapper.classList.add("header");
            let title = document.createElement('div');
            title.classList.add('title');
            let p= document.createElement('p');
            p.innerHTML=`<span class="fa fa-link"></span> Link`;
            title.appendChild(p);
            wrapper.appendChild(title);
           // let buttons = document.createElement('div');
           // buttons.classList.add('buttons');
           // buttons.innerHTML = `<button class="" id="close-btn"><span class="fa fa-ban"></span></button><button id="edit-btn">Edit</button>`;
           // wrapper.appendChild(buttons);
            return wrapper;
        }
        let links = ()=>{
            let links = document.createElement('div');
            links.classList.add('links');
            links.innerHTML=`<div class="link-item">
                   <h4><a id="viewed-link" href="#" target="_blank"></a></h4>
                   <p id="def-text"></p>
               </div>`;
            return links;
        }
        lv.appendChild(header());
        lv.appendChild(links());

        return lv;

    }

    getLinkEdit(){
        let le =  document.createElement('div')
        le.classList.add('link-edit');
        le.id="link-edit";

        let getHeader =()=>{
            let header =document.createElement('div');
            header.classList.add('header');
            header.innerHTML = `<div class="left">
                    <button id="cancel-btn">Cancel</button>
                </div>
                <div class="center">
                    <p>Link</p>
                </div>
                <div class="right">
                    <button class="" id="done-btn">Done</button>
                </div>`;
            return header;
        }
        let body = ()=>{
            let bd = document.createElement('div');
            let tabWrap =  document.createElement('div');
            bd.appendChild(tabWrap);
           tabWrap.classList.add('tabs-wrapper');
            bd.classList.add('body');
            let buttons = ()=>{
                let btns = document.createElement('div');
                btns.classList.add('tabs-btn');
                btns.innerHTML = `<button class="active" id="search-btn">Search Pages</button>
                        <button class="" id="external-btn">External Links</button>
                        <button class="" id="def-btn">Definition</button>`;
                return btns;
            }
            let tabs = ()=>{
                let tabs = document.createElement('div');
                tabs.classList.add('tabs');

                let searchTab = ()=>{
                    let st = document.createElement('div');
                    st.classList.add('tab');
                    st.id='search-tab';

                    st.innerHTML = `<div class="search-box">
                                <input type="search" id="search-box">
                            </div>
                            <div class="search-result" id="search-result">
                
                               
                            </div>
                                 `;
                    return st;
                }
                let extTab = ()=>{
                    let ext = document.createElement('div');
                    ext.classList.add('tab');
                    ext.id = "external-tab";
                    ext.innerHTML = `
                        <div class="link-box">
                                 <label>Title</label>
                                <input type="url" id="ext-title">
                                
                                <label>Url</label>
                                <input type="url" id="ext-url">
                            </div>
                    `;
                    return ext;
                }
                let defTab = ()=>{
                    let def = document.createElement('div');
                    def.classList.add('tab');
                    def.id='definition-tab';
                    def.innerHTML = `<div class="text-area">
                                <label>Enter the word definition</label>
                                <textarea rows="5" id="definition"></textarea>
                            </div>`;

                    return def;

                }
                tabs.appendChild(searchTab());
                tabs.appendChild(extTab());
                tabs.appendChild(defTab());
                return tabs;

            }
            tabWrap.appendChild(buttons());
            tabWrap.appendChild(tabs());
            return bd;

        }
        le.appendChild(getHeader());
        le.appendChild(body());

        return le;

    }

    attachElement(){
        document.body.appendChild(this.element);
    }

    showViewLink(link,text,title,def){
        if(document.body.contains(this.element)){
            document.body.removeChild(this.element)
        }
       this.attachElement();
       let defText = document.getElementById('def-text');
       let vlink = document.getElementById('viewed-link');

       defText.innerText = def;
       if(!def)
           defText.innerText = title;
       vlink.innerText=text;
       vlink.title =title;
       vlink.href = link;
       this.element.style.display ="block";
        document.querySelector("#link-view").style.display ="block";
        document.querySelector("#link-edit").style.display ="none";
    }


    showEditLink(){
        if(document.body.contains(this.element)){
            document.body.removeChild(this.element)
        }
        this.attachElement();
        this.element.style.display ="block";
        document.querySelector("#link-view").style.display ="none";
        document.querySelector("#link-edit").style.display ="block";
    }

    hidePopover(){
        if(document.body.contains(this.element)){
            document.body.removeChild(this.element)
        }
    }






}


class EditorPopover{



    constructor(){

       this.pop = new PopoverCreator();

    }

    showViewLink(link,text,title,def,event){
        this.pop.showViewLink(link,text,title,def,);
        this.getElements();
        this.registerEvents();
        let x,y;
        x = event.pageX;
        y = event.pageY;
        this.positionElement(this.pop.element,x,y);
    }

    showEditLink(text,x,y,cb){
        this.pop.showEditLink();
        this.getElements();
        this.registerEvents();
        this.reset();
        document.getElementById('search-box').value =text;
        this.addlinks(text,cb);
        this.positionElement(this.pop.element,x,y);
        let _this = this;
        this.doneBtn.onclick = ()=>{
            let external_url_title = document.querySelector('#ext-title').value;
            let external_url_link = document.querySelector("#ext-url").value;
            let definition =  document.querySelector("#definition").value;
            let reply = {
                external_url_title,
                external_url_link,
                definition
            };
            cb(reply);
            _this.hidePopover();
        }

    }


    addlinks(text,cb){
        if(text) {
            let resultPane = document.getElementById('search-result');
            const articleJson = this.articleJSON;

                let filtered = articleJson.filter(article => {
                    let re = new RegExp(text, 'i');
                    return re.test(article.title)
                });
                resultPane.innerHTML = '';
                filtered.forEach(item => {
                    resultPane.innerHTML += `<p class="result"><span class="fa fa-link"></span> <a href="${item.link}" target="_blank" title="${item.title}" class="link-result">${item.title}</a> </p>`
                });
                this.addLinkSelectEvent(cb);
            }

    }

    addLinkSelectEvent(cb){
        let links =  document.querySelectorAll('.link-result');
        for(const link of links){
             link.onclick = event =>{
                 event.preventDefault();
                 let elm = event.target;
                 let external_url_title = elm.title;
                 let external_url_link = elm.href;
                 let definition =  '';
                 let reply = {
                     external_url_title,
                     external_url_link,
                     definition
                 };
                 cb(reply);
                 this.hidePopover();
             }
        }
    }

    hidePopover(){
        this.pop.hidePopover();
    }

    getElements(){
        this.searchBtn = document.querySelector('#search-btn');
        this.externalBtn = document.querySelector('#external-btn');
        this.defBtn = document.querySelector("#def-btn");
        this.tabs = document.querySelectorAll('.tab');
        this.tabBtns = document.querySelectorAll(".tabs-btn button");
        this.searchTab = document.querySelector('#search-tab');
        this.externalTab = document.querySelector('#external-tab');
        this.defTab = document.querySelector('#definition-tab');
        this.viewLink =document.querySelector('#link-view');
        this.editLink = document.querySelector('#link-edit');
        this.editBtn = document.querySelector("#edit-btn");
        this.cancelBtn = document.querySelector("#cancel-btn");
        this.doneBtn = document.querySelector("#done-btn");
        this.closeBtn = document.querySelector("#close-btn");

    }

    positionElement(element,x,y){

        element.style.left = x-133+"px";
        element.style.top = y+40+"px";
    }

    registerEvents(){
        this.externalBtn.addEventListener('click',(event)=>{this.showExternal(event)},false);
        this.searchBtn.addEventListener('click',(event)=>{this.showSearch(event)},false);
        this.defBtn.addEventListener('click',(event)=>{this.showDef(event)},false);
     //   this.editBtn.addEventListener('click',()=>{this.showEditPane()});
        this.cancelBtn.addEventListener('click',()=>{this.hidePopover()});
     //   this.closeBtn.addEventListener('click',()=>{this.hidePopover()});
    }
    showEditPane(){
        let vlink = document.getElementById('viewed-link');
        let searchbox = document.getElementById('search-box');

        searchbox.value = vlink.innerText;
        this.viewLink.style.display="none";
        this.editLink.style.display ="block";
    }

    showExternal(event){
        let target = event.target;
        this.removeActionClass();
        this.hideTabs();
        target.classList.add('active');
        this.externalTab.style.display = "block";
    }

    showSearch(event){
        let target = event.target;
        this.removeActionClass();
        this.hideTabs();
        target.classList.add('active');
        this.searchTab.style.display = "block";
    }

    showDef(event){
        let target = event.target;
        this.removeActionClass();
        this.hideTabs();
        target.classList.add('active');
        this.defTab.style.display = "block";
    }

    hideTabs(){
        for(const tab of this.tabs){
            tab.style.display = "none"
        }
    }

    removeActionClass(){
        for(const btn of this.tabBtns){
            btn.classList.remove('active');
        }
    }

    setArticleJSON(articleJSON){
        this.articleJSON = articleJSON;
    }

    reset(){
        this.showSearch({target:this.searchBtn});
        document.querySelector('#ext-title').value = '';
        document.querySelector("#ext-url").value ='';
        document.querySelector("#definition").value ='';
    }

}


window.onload = ()=>{
    let edit = new Editor('#editor');
    edit.init();
    let nav = new PostHeadingNavigator(edit);
    nav.getElements();
    nav.extendTextarea();
    nav.registerEvents();
    nav.updateElement();
$.get('/api/articles',function (data) {
    edit.setArticleJSON(data);
});
};







