<?php

namespace App;
use Illuminate\Foundation\Auth\User  as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Admin extends Authenticatable
{
    protected $guard = 'admin';

    protected $fillable = ['username','email','password'];


}
