class MediaHandler{

    constructor(){
        this.index =1;
        this.vidIndex = 0;
        this.picIndex = 0;
       this.addRemoveListener();
    }


    addEventListener(){

        this.pictureBtn.click(event=>{
            this.showPictureDialog(event);
        });

        this.pictureInput.change(event=>{
            this.handlePictures(event);
        });


        this.videoBtn.click(event=>{
            this.showVideoDialog(event);
        })

        this.videoInput.change(event=>{
          this.handleVideos(event);
        })






    }

    getElements(){
        this.pictureBtn = $("#picture-btn");
        this.pictureInput = $("#picture-input");
        this.videoBtn = $("#video-btn");
        this.videoInput = $("#video-input");

        this.uploadBtn = $(".upload-btn");
        this.uploadInput =$('#media-1')
    }
    showPictureDialog(){
        this.pictureInput.get(0).click();
    }

    showVideoDialog(){
        this.videoInput.get(0).click();
    }



    showFileDialog(){
        this.uploadInput.get(0).click();
    }

    addRemoveListener(){
        $(".close-btn").click(function (e) {
            $(this).parent().remove();
        });
    }


    handlePictures(event){
        let files = event.target.files;
        let _this = this;
        for(const file of files){
            let reader = new FileReader();
            reader.onload = event =>{
                let element = this.createImageThumbnail(event.target.result);
                $("#picture-files").prepend(element);
                this.addRemoveListener();
                this.attachPicToInput(files);
               // this.attachFileToInput(files)
            };
            reader.readAsDataURL(file)
        }
    }





    handleVideos(event)
    {
        let files = event.target.files;
        for(const file of files){
            let reader = new FileReader();
            reader.onload = event =>{
                let element = this.createVideoThumbnail(event.target.result);
                $("#video-files").prepend(element);
                this.addRemoveListener();
                this.attachVideoToInput(files);
               //this.attachFileToInput(files)
            };
            reader.readAsDataURL(file)
        }
    }

    handleFiles(event)
    {
       let files = event.target.files;
       let _this = this;
      for(const file of files){
          let reader = new FileReader();
          reader.onload = event =>{
              let element = this.createImageThumbnail(event.target.result);
              $("#media-files").prepend(element);
              this.addRemoveListener();
              this.attachFileToInput(files)
          };
          reader.readAsDataURL(file)
      }

    }



    createImageThumbnail(src){
        return `<div class="uploaded-media">
                   <span class="fa fa-close close-btn" ></span>
                    <img src="${src}" class="img-fluid">
                     <input type="text" name="pictures_description[]" class="img-description" placeholder="Picture description">
                    <input type="file" name="pictures[]" class="form-control media-input"  id="picture-${++this.picIndex}" accept="video/*,image/*">
                </div>`;
    }

    createVideoThumbnail(src){
        return `<div class="uploaded-media">
                   <span class="fa fa-close close-btn" ></span>
                    <video src="${src}" class="img-fluid"></video>
                     <input type="text" name="videos_description[]" class="img-description" placeholder="Video description">
                    <input type="file" name="videos[]" class="form-control media-input"  id="video-${++this.vidIndex}" accept="video/*,image/*">
                </div>`;
    }


    attachVideoToInput(files){
        $(`#video-${this.vidIndex}`).get(0).files = files;
    }


    attachPicToInput(files){
        $(`#picture-${this.picIndex}`).get(0).files = files;
    }



    attachFileToInput(files){
        $(`#media-${this.index}`).get(0).files = files;
    }

}

let media = new MediaHandler();

media.getElements();
media.addEventListener();
media.addRemoveListener();


 export class   PostHeadingNavigator{

    constructor(editor){
        this.editor = editor;
        this.headings = this.getPostHeadings();
        this.active =0;
        this.nextBtn =null;
        this.prevBtn =null;
        this.textArea =null;
        this.title =null;
        this.helpTex =null;
        this.updateObject();
        this.editor.linkAddedListener = ()=>{

           this.headings[this.active].textBoxValue = this.textArea.val();
           this.updateInputs();

        }


    }


    getElements(){
        this.nextBtn = $("#next-btn");
        this.prevBtn =$("#prev-btn");
        this.textArea=$("#editor-text");
        this.title = $("#heading");
        this.helpTex =$("#help-text");
        this.picturePane = $('#picture-pane');
        this.videoPane = $('#video-pane');
        this.uploadPane = $("#upload-pane");
        this.textPane = $("#text-pane");
        this.mediaIput = $(".media-input");
        this.postBtn = $("#post");

    }


    extendTextarea(){
        this.textArea.val = (val)=>{
            if(val ===undefined)
                return this.textArea.get(0).innerHTML;
           this.textArea.get(0).innerHTML = val
        }


    }

    registerEvents(){
        this.nextBtn.click((event)=>{
            this.nextHeading();
        });

        this.prevBtn.click(event=>{
            this.previousHeading()
        });

        this.textArea.keyup(event=>{
            this.headings[this.active].textBoxValue = this.textArea.val();
            this.updateInputs();
        });

        this.mediaIput.change(event=>{
            this.handleChoseFile();
        })

    }
    handleChoseFile(){
        if(this.mediaIput.val()!=''){
            this.uploadPane.append("")
        }
    }




    nextHeading(){
        if(this.active<this.headings.length){
            this.active+=1;
            this.updateElement();
            this.editor.hideBalloon();
        }

    }

    previousHeading(){
        if(this.active>0){
            this.active-=1;
            this.updateElement();
            this.editor.hideBalloon();
        }

    }

    updateObject(){

        this.headings[0].textBoxValue = $('input[name="tribe"]').val();
        this.headings[1].textBoxValue = $('input[name="region"]').val();
        this. headings[2].textBoxValue = $('input[name="country"]').val();
        this.headings[3].textBoxValue = $('input[name="language"]').val();
        this.headings[4].textBoxValue = $('input[name="geo-description"]').val();
        this.headings[5].textBoxValue = $('input[name="history"]').val();
        this.headings[6].textBoxValue = $('input[name="literature"]').val();
        this. headings[7].textBoxValue = $('input[name="tradition"]').val();
        this. headings[8].textBoxValue = $('input[name="leadership"]').val();
        this. headings[9].textBoxValue = $('input[name="social-institution"]').val();
        this. headings[10].textBoxValue = $('input[name="religion"]').val();
        this. headings[11].textBoxValue = $('input[name="festivals"]').val();
        this.headings[12].textBoxValue = $('input[name="occupation"]').val();
        this. headings[13].textBoxValue = $('input[name="technology"]').val();
        this.headings[14].textBoxValue = $('input[name="clothing"]').val();
        this. headings[15].textBoxValue = $('input[name="food"]').val();
        this. headings[16].textBoxValue = $('input[name="tourist-attraction"]').val();
        this. headings[17].textBoxValue = $('input[name="other-traditions"]').val();

    }

    updateInputs(){

        switch (this.active) {
            case 0:
                $('input[name="tribe"]').val(this.headings[0].textBoxValue);
                break;
            case 1:
                $('input[name="region"]').val( this.headings[1].textBoxValue);
                break;
            case 2:
                $('input[name="country"]').val( this.headings[2].textBoxValue);
                break;
            case 3:
                $('input[name="language"]').val(this.headings[3].textBoxValue);
                break;
            case 4:
                $('input[name="geo-description"]').val(this.headings[4].textBoxValue);
                break;
            case 5:
                $('input[name="history"]').val(this.headings[5].textBoxValue);
                break;
            case 6:
                $('input[name="literature"]').val(this.headings[6].textBoxValue);
                break;
            case 7:
                $('input[name="tradition"]').val( this.headings[7].textBoxValue);
                break;
            case 8:
                $('input[name="leadership"]').val( this.headings[8].textBoxValue);
                break;
            case 9:
                $('input[name="social-institution"]').val(this.headings[9].textBoxValue);
                break;
            case 10:
                $('input[name="religion"]').val(this.headings[10].textBoxValue);
                break;
            case 11:
                $('input[name="festivals"]').val(this.headings[11].textBoxValue);
                break;
            case 12:
                $('input[name="occupation"]').val( this.headings[12].textBoxValue);
                break;
            case 13:
                $('input[name="technology"]').val(this.headings[13].textBoxValue);
                break;
            case 14:
                $('input[name="clothing"]').val(this.headings[14].textBoxValue);
                break;
            case 15:
                $('input[name="food"]').val( this.headings[15].textBoxValue );
                break;
            case 16:
                $('input[name="tourist-attraction"]').val(this.headings[16].textBoxValue);
                break;
            case 17:
                $('input[name="other-traditions"]').val( this.headings[17].textBoxValue );


        }

    }


    updateElement(){
        if(this.active+1 >= this.headings.length){
            this.nextBtn.hide();
        }else{
            this.nextBtn.show();
        }

        if(this.active<=0){
            this.prevBtn.hide()
        }else {
            this.prevBtn.show();
        }

        if(this.active === this.headings.length-2) {

            this.title.text(this.headings[this.active].heading);
            this.helpTex.text(this.headings[this.active].description);
            this.textPane.hide();
            this.picturePane.show();
            this.videoPane.hide();
            this.postBtn.hide();
        }
        else if(this.active === this.headings.length-1){
            this.title.text(this.headings[this.active].heading);
            this.helpTex.text(this.headings[this.active].description);
            this.textPane.hide();
            this.picturePane.hide();
            this.videoPane.show();
            this.postBtn.show();
        }else{
            this.textPane.show();
            this.videoPane.hide();
            this.picturePane.hide();
            this.title.text(this.headings[this.active].heading);
            this.helpTex.text(this.headings[this.active].description);
            this.textArea.val(this.headings[this.active].textBoxValue) ;
            this.postBtn.hide()
            this.updateInputs();
        }

        this.editor.addLinkEventListener();




    }


    getPostHeadings(){
        return    [
            {
                heading:"Tribe/ Community",
                description:"On this heading you will write about the tribe/community that practice this culture",
                textBoxValue:""
            },
            {
                heading:"Region/ Province/ State",
                description:"On this heading you will write about the Region/Province/State that practice this culture",
                textBoxValue:""
            },
            {
                heading:"Country",
                description:"On this heading you will write about the Country in which this culture is prevalent",
                textBoxValue:""
            },
            {
                heading:"Language(s)",
                description:"On this heading you will write about the Language(s) popular in this culture",
                textBoxValue:""
            },
            {
                heading:"Landscape",
                description:"On this heading you will on  the Landscape of this culture",
                textBoxValue:""
            },
            {
                heading:"History and Mythology",
                description:"On this heading you will write about the History and Mythology of this culture",
                textBoxValue:""
            },
            {
                heading:"Literature",
                description:"On this heading you will write about the Literature of this culture",
                textBoxValue:""
            },
            {
                heading:"Music and Arts",
                description:"On this heading you will write about the culture\'s music and art",
                textBoxValue:""
            },
            {
                heading:"Politics and Leadership",
                description:"On this heading you will write about the Politics and Leadership of this culture",
                textBoxValue:""
            },
            {
                heading:"Social Institutions",
                description:"On this heading you will write about the Social Institutions of this culture",
                textBoxValue:""
            },
            {
                heading:"Religion and Rituals",
                description:"On this heading you will write about the Religion and Ritual perform by this culture",
                textBoxValue:""
            },
            {
                heading:"Festivals",
                description:"On this heading you will write about the festivals this culture",
                textBoxValue:""
            },
            {
                heading:"Occupations",
                description:"On this heading you will write about the  Occupations of this culture",
                textBoxValue:""
            },
            {
                heading:"Tools and Technologies",
                description:"On this heading you will write about the  Tools and Technologies of this culture",
                textBoxValue:""
            },
            {
                heading:"Clothing and Cosmetics",
                description:"On this heading you will write about the  Clothing and Cosmetics of this culture",
                textBoxValue:""
            },
            {
                heading:"Food and Food Processing",
                description:"On this heading you will write about the  Food and Food Processing  of this culture",
                textBoxValue:""
            },
            {
                heading:" Tourist Attractions",
                description:"On this heading you will write about the   Tourist Attractions  of this culture",
                textBoxValue:""
            },
            {
                heading:"Other traditions",
                description:"On this heading you will write about   other traditions  of this culture",
                textBoxValue:""
            },
            {
                heading:"Pictures",
                description:"Upload Picture about their festivals, food ,dressing etc.",
                files:[]
            },
            {
                heading:"Videos",
                description:"Upload Videos about their festivals, food ,dressing etc",
                files:[]
            },

        ];
    }
}






