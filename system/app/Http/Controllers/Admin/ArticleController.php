<?php

namespace App\Http\Controllers\Admin;


use App\Http\Requests\StoreArticle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ArticleController as ArticleBase;
use App\Article;

class ArticleController extends ArticleBase
{

    public function store(StoreArticle $request){



        $article = new Article();
        $article->user_id = 1;
        $article->title = $request->input('title');
        $article->approved = true;
        $article->published = true; 
        $article->tribe  = $request->input('tribe');
        $article->region =$request->input('region');
        $article->country = $request->input('country');
        $article->language = $request->input('language');
        $article->geo_description = $request->input('geo-description');
        $article->history = $request->input('history');
        $article->literature = $request->input('literature');
        $article->tradition = $request->input('tradition');
        $article->leadership = $request->input('leadership');
        $article->social_institution = $request->input('social-institution');
        $article->religion = $request->input('religion');
        $article->festivals = $request->input('festivals');
        $article->occupations = $request->input('occupation');
        $article->technology = $request->input('technology');
        $article->clothing = $request->input('clothing');
        $article->food = $request->input('food');
        $article->tourist_attraction = $request->input('tourist-attraction');
        $article->other_traditions = $request->input('other-traditions');
        $article->article_references = $request->input('references');
        $article->published = true;
        $article->approved = true;
        $article->article_references = $request->input('references');



        if($article->save()){
            if($request->hasFile('pictures'))
                $this->uploadPictures($request->file('pictures'),$request->input('pictures_description'),$article->article_id);
            if($request->hasFile('videos'))
                $this->uploadVideos($request->file('videos') ,$request->input('videos_description'),$article->article_id);
            return redirect(url()->previous())->with('success',"Article was successfully published");
        }else{
            return redirect(url()->previous())->with('error',"Article was not   published due to some system error.");
        }



    }
    public function approve($id){
        $article =  Article::find($id);
        $article->approved = true;
        if($article->save())
            return redirect(url()->previous())->with('approval_success','Article was approved successfully');
        return redirect(url()->previous())->with('approval_error',"Article was not approved due to system error");
    }

    public function disapprove($id){
        $article =  Article::find($id);
        $article->approved = false;
        if($article->save())
            return redirect(url()->previous())->with('disapproval_success','Article was disapproved successfully');
        return redirect(url()->previous())->with('disapproval_error',"Article was not disapproved due to system error");
    }

}
