<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('articles',function (Blueprint $table){
          $table->increments("article_id");
          $table->string("title")->uniquie();
          $table->mediumText("tribe");
          $table->mediumText("region");
          $table->mediumText("country");
          $table->mediumText("language");
          $table->mediumText("geo_description");
          $table->mediumText("history");
          $table->mediumText("literature");
          $table->mediumText("tradition");
          $table->mediumText("leadership");
          $table->mediumText("social_institution");
          $table->mediumText("religion");
          $table->mediumText("occupations");
          $table->mediumText("technology");
          $table->mediumText("clothing");
          $table->mediumText("food");
          $table->mediumText("tourist_attraction");
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles');
    }
}
